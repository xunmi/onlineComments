package com.xunmi.comments.util;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * POI文件生成工具
 */
public class POIUtil {

	public static void teacherTemplate() {
		// 第一步，创建一个webbook，对应一个Excel文件
		HSSFWorkbook wb = new HSSFWorkbook();
		// 第二步，在webbook中添加一个sheet，对应excel文件中的sheet
		HSSFSheet sheet = wb.createSheet("教师批量添加模板！");
		// 第三步，在sheet添加表头第0行，注意老版本poi对excel的行数列有限制short
		HSSFRow row = sheet.createRow(0);
		// 第四步，创建单元格，并设置表头，设置表头居中
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 创建居中格式

		HSSFCell cell = row.createCell(0);
		cell.setCellValue("用户名");
		cell.setCellStyle(style);
		cell = row.createCell(1);
		cell.setCellValue("密码");
		cell.setCellStyle(style);
		cell = row.createCell(2);
		cell.setCellValue("性别");
		cell.setCellStyle(style);
		cell = row.createCell(3);
		cell.setCellValue("昵称");
		cell.setCellStyle(style);

		String path = POIUtil.class.getClassLoader().getResource("").getPath();
		System.out.println(path);
		String rootPath = path.substring(0, path.indexOf("/build/"));
		try {
			FileOutputStream fopt = new FileOutputStream(rootPath
					+ "/upload/TeacherTemplate.xls");
			wb.write(fopt);
			fopt.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
