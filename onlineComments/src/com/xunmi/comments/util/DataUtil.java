package com.xunmi.comments.util;

import java.security.MessageDigest;

/**
 * 数据处理工具类
 */
public class DataUtil {

	/**
	 * 使用MD5进行加密处理
	 */
	public static String md5(String src) {

		try {
			StringBuffer buffer = new StringBuffer();

			char[] chars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
					'A', 'B', 'C', 'D', 'E', 'F', };

			byte[] bytes = src.getBytes();
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] targ = md.digest(bytes);

			for (byte b : targ) {
				// 二进制高四位转换为16进制
				buffer.append(chars[(b >> 4) & 0x0F]);
				// 低四位转换为16进制
				buffer.append(chars[b & 0x0F]);
			}

			return buffer.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
