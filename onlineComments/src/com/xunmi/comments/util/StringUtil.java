package com.xunmi.comments.util;

public class StringUtil {

	/**
	 * 截取字符串
	 */
	public static String cutOutString(String str, int begin, int end) {
		return str.substring(begin, end);
	}

	/**
	 * 依据majorYears生成元年到末年字符串，逗号隔开 例：2015,2016,2017,2018
	 */
	public static String timeGroupString(int majorYears, int startTime) {
		String createTime = "";
		for (int i = startTime; i < startTime + majorYears; i++) {
			createTime = createTime + i + ",";
		}
		return createTime.substring(0, createTime.length() - 1);
	}

	/**
	 * 将数组变换成字符串,使用","号分割
	 */
	public static String arr2Str(Object[] arr) {
		String temp = "";
		if (ValidateUtil.isValid(arr)) {
			for (Object s : arr) {
				temp = temp + s + ",";
			}
			return temp.substring(0, temp.length() - 1);
		}
		return temp;
	}
}
