package com.xunmi.comments.util;

import java.io.File;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xunmi.comments.service.RightService;

/**
 * 提取所有权限的工具类
 * 
 * @author JFW-PC
 * 
 */
public class ExtractAllRightsUtil {

	public static void main(String[] args) throws Exception {
		ApplicationContext ac = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		RightService rs = (RightService) ac.getBean("rightService");

		ClassLoader loader = ExtractAllRightsUtil.class.getClassLoader();
		URL url = loader.getResource("com/xunmi/comments/actions");
		File dir = new File(url.toURI());
		File[] files = dir.listFiles();
		String fileName = "";
		for (File f : files) {
			fileName = f.getName();
			if (fileName.endsWith(".class")
					&& !fileName.equals("BaseAction.class")) {
				processAction(fileName, rs);
			}
		}
	}

	/**
	 * 处理action类，捕获所有url地址，形成权限
	 */
	private static void processAction(String fileName, RightService rs) {
		try {
			String pkgName = "com.xunmi.comments.actions";
			String simpleClassName = fileName.substring(0,
					fileName.indexOf(".class"));
			String className = pkgName + "." + simpleClassName;

			// 得到具体类
			Class clazz = Class.forName(className);
			Method[] methods = clazz.getDeclaredMethods();
			Class retType = null;
			String mname = null;
			Class[] paramType = null;
			for (Method m : methods) {
				retType = m.getReturnType();// 返回值类型
				mname = m.getName();// 方法名称
				paramType = m.getParameterTypes();// 参数类型
				String url = null;
				if (retType == String.class && !ValidateUtil.isValid(paramType)
						&& Modifier.isPublic(m.getModifiers())) {
					if (mname.equals("execute")) {
						url = "/" + simpleClassName;
					} else {
						url = "/" + simpleClassName + "_" + mname;
					}
					System.out.println(url);
					rs.appendRightByURL(url);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
