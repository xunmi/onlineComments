package com.xunmi.comments.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlUtil {
	private static final String regEx_html = "<[^>]+>|&nbsp;"; // 定义HTML标签的正则表达式
	
	public static String delHTMLTag(String htmlStr) {
		Pattern p_style = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
        Matcher m_style = p_style.matcher(htmlStr);
        htmlStr = m_style.replaceAll("");
		return htmlStr.trim();
	}
}
