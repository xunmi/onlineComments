package com.xunmi.comments.service;

import java.util.List;

import com.xunmi.comments.model.Answer;
import com.xunmi.comments.model.Course;
import com.xunmi.comments.model.Page;
import com.xunmi.comments.model.Question;
import com.xunmi.comments.model.Test;
import com.xunmi.comments.model.User;

public interface CourseService {

	/**
	 * 新建课程
	 */
	public void newCourse(Course course, User user);

	/**
	 * 我（教师）的课程列表
	 */
	public List<Course> findMycourses(User user);

	/**
	 * 我的已分配课程列表
	 */
	public List<Course> findMyallotedcourses(User user);

	/**
	 * 获取要更新的课程
	 */
	public Course getCourse(Integer cid);

	/**
	 * 更新课程信息
	 */
	public void updateCourse(Course model);

	/**
	 * 我的课程下的实验列表
	 */
	public List<Test> findMytests(Integer cid);

	/**
	 * 添加实验
	 */
	public void newTest(Test model, Integer cid);

	/**
	 * 获得要设计的实验
	 */
	public Test getTest(Integer tid);

	/**
	 * 获得要设计的实验，同时携带所有孩子
	 */
	public Test getTestWithChildren(Integer tid);

	/**
	 * 保存/更新页面
	 */
	public void saveOrUpdatePage(Page model);

	/**
	 * 获取要设计的页面
	 */
	public Page getPage(Integer pid);

	/**
	 * 获取要设计的页面，同时携带所有的孩子
	 */
	public Page getPageWithChridren(Integer pid);

	/**
	 * 保存/更新问题
	 */
	public void saveOrUpdateQuestion(Question model);

	/**
	 * 获取要设计的问题
	 */
	public Question getQuestion(Integer qid);

	/**
	 * AJAX更新实验名称
	 */
	public void updateTest(Test model);

	/**
	 * AJAX更新页面名称
	 */
	public void updatePage(Page model);

	/**
	 * 删除问题
	 */
	public void deleteQuestion(Integer qid);

	/**
	 * 删除页面，同时删除页面下的问题
	 */
	public void deletePage(Integer pid);

	/**
	 * 删除实验，同时删除页面和问题
	 */
	public void deleteTest(Integer tid);

	/**
	 * 删除课程
	 */
	public void deleteCourse(Integer cid);

	/**
	 * 通过tid查询
	 */
	public List<Course> findBytidcourses(Integer tid);

	/**
	 * 通过cid查询
	 */
	public List<Course> findBycidcourses(Integer cid);

	/**
	 * 通过tid和cid查询
	 */
	public List<Course> findBytidAndcidcourses(Integer tid, Integer cid);

	/**
	 * 学生课程列表
	 */
	public List<Course> findStudentcourses(User user);

	/**
	 * 删除courseparent
	 */
	public void deleteCourseParent(Integer cpid);

	/**
	 * 通过cpid查找course列表
	 */
	public List<Course> findBycpidCourses(Integer cpid);

	/**
	 * 更新或保存实验
	 */
	public void saveOrUpdateTest(Test t);

	/**
	 * 查询已提交实验报告
	 * 
	 * @author hjh
	 */
	public List<User> findSubmitUsers(Integer tid);

	/**
	 * 通过tid查找answer
	 * 
	 * @author hjh
	 */
	public Answer findAnswerByTid(Integer tid, Integer uid);

	/**
	 * 获取学生答案
	 * 
	 * @author hjh
	 */
	public List<Answer> getUserAnswers(Integer uid);
	
	/**
	 * 分页查询
	 * @param cid
	 * @param currPage
	 * @param showNumber
	 * @return
	 */
	public List<Test> findMytestsPaging(Integer cid, Integer currPage,
			Integer showNumber);

	/**
	 * 添加批注
	 * 
	 * @author JFW-PC
	 */
	public void addSolutionCommets(Answer answer);

}
