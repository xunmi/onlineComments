package com.xunmi.comments.service;

import java.util.List;

import com.xunmi.comments.model.Term;

public interface TermService {

	/**
	 * 查找所有学期
	 */
	public List<Term> findAllterms();

}
