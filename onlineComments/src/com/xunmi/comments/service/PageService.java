package com.xunmi.comments.service;

import java.util.List;

import com.xunmi.comments.model.Answer;
import com.xunmi.comments.model.Page;

public interface PageService {

	/**
	 * 根据tid查找pages
	 */
	public List<Page> findPagesWithChildren(Integer tid);
	
	/**
	 * 获取page
	 */
	public Page getPage(Integer currPid);

	/**
	 * 查询指定页面的下一页
	 */
	public Page getNextPage(Page targPage);

	/**
	 * 查询指定页面的上一页
	 */
	public Page getPrePage(Page targPage);
	
	/**
	 * 判断指定页面是否是所在调查尾页
	 */
	public boolean isLastPage(Page targPage);

	/**
	 * 批量保存答案
	 */
	public void saveAnswers(List<Answer> list);

	
}
