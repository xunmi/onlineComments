package com.xunmi.comments.service;

import java.util.List;

import com.xunmi.comments.model.CourseParent;
import com.xunmi.comments.model.User;

public interface CourseParentService {

	/**
	 * 新建课程
	 */
	public void newCourseParent(CourseParent model, User user);

	/**
	 * 我的课程列表
	 */
	public List<CourseParent> findMycourseparents(User user);

	/**
	 * 获得CourseParent
	 */
	public CourseParent getCourseParent(Integer id);
	
	/**
	 * 查找所有课程
	 */
	public List<CourseParent> findAllcourseparents();

	/**
	 * 删除课程（保留已分配的course）
	 
	public void deleteCourseParent(Integer cpid);*/

	/**
	 * 更新课程信息
	 */
	public void updateCourseParent(CourseParent model);
}
