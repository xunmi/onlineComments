package com.xunmi.comments.service;

import java.util.List;
import java.util.Set;

import com.xunmi.comments.model.security.Role;

/**
 * RoleService
 */
public interface RoleService extends BaseService<Role> {

	/**
	 * 保存或者添加角色
	 */
	void saveOrUpdateRole(Role model, Integer[] ownRightIds);

	/**
	 * 查询不在指定范围的角色
	 */
	List<Role> findRolesNotInRange(Set<Role> roles);

	/**
	 * 查询在制定单位的角色
	 */
	List<Role> findRolesInRange(Integer[] ownRoleIds);

}
