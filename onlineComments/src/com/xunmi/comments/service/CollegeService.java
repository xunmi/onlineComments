package com.xunmi.comments.service;

import java.util.List;

import com.xunmi.comments.model.College;

/**
 * CollegeService
 */
public interface CollegeService extends BaseService<College> {

	/**
	 * 按照id查询college
	 */
	College getCollege(Integer cid);

	/**
	 * 查询所有的college
	 */
	List<College> findAllCollege();

	/**
	 * 更新学院
	 */
	void updateCollege(College model);

	/**
	 * 删除学院
	 */
	void deleteCollege(Integer cid);

	/**
	 * 查找学院列表
	 * 
	 * @author hjh
	 */
	public List<College> findAllcolleges();

}
