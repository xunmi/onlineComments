package com.xunmi.comments.service.impl;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.xunmi.comments.dao.BaseDao;
import com.xunmi.comments.model.CourseParent;
import com.xunmi.comments.model.User;
import com.xunmi.comments.service.CourseParentService;
import com.xunmi.comments.service.CourseService;

@Service("courseParentService")
public class CourseParentServiceImpl implements CourseParentService {

	@Resource(name="courseParentDao")
	private BaseDao<CourseParent> courseParentDao;
	
	@Resource
	private CourseService courseService;
	
	/**
	 * 新建课程
	 */
	public void newCourseParent(CourseParent model, User user) {
		//设置关联关系
		model.setUser(user);
		courseParentDao.saveEntity(model);
	}
	
	/**
	 * 我的课程列表
	 */
	public List<CourseParent> findMycourseparents(User user){
		String hql = "from CourseParent c where c.user.id = ?";
		return courseParentDao.findEntityByHQL(hql,user.getId());
	}
	
	/**
	 * 获得CourseParent
	 */
	public CourseParent getCourseParent(Integer id) {
		return courseParentDao.getEntity(id);
	}
	
	/**
	 * 查找所有课程
	 */
	public List<CourseParent> findAllcourseparents(){
		String hql = "from CourseParent";
		return courseParentDao.findEntityByHQL(hql);
	}
	
	/**
	 * 删除课程（保留已分配的course）
	 
	public void deleteCourseParent(Integer cpid) {
		courseParentDao.deleteEntity(courseParentDao.getEntity(cpid));
	}*/
	
	/**
	 * 更新课程信息
	 */
	public void updateCourseParent(CourseParent model) {
		courseParentDao.updateEntity(model);
	}
}
