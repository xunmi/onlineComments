package com.xunmi.comments.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.xunmi.comments.dao.BaseDao;
import com.xunmi.comments.model.College;
import com.xunmi.comments.model.Major;
import com.xunmi.comments.service.MajorService;

/**
 * 专业实现类
 */
@Service("majorService")
public class MajorServiceImpl implements MajorService {

	@Resource(name = "collegeDao")
	private BaseDao<College> collegeDao;

	@Resource(name = "majorDao")
	private BaseDao<Major> majorDao;

	/**
	 * 查询所有专业
	 */
	public List<College> findAllColleges() {
		String hql = "from College";
		return this.collegeDao.findEntityByHQL(hql);
	}

	/**
	 * 按照学院id查询所有专业
	 */
	public List<Major> findAllMajorsByCid(Integer cid) {
		String hql = "from Major m where m.college.id = ?";
		return this.majorDao.findEntityByHQL(hql, cid);
	}

	/**
	 * 添加专业
	 */
	public void addMajor(Major model, Integer cid) {
		College c = new College();
		c.setId(cid);
		// 设置关联关系
		model.setCollege(c);
		majorDao.saveEntity(model);
	}

	/**
	 * 按照id查询专业id
	 */
	public Major findMajorByMid(Integer mid) {
		return majorDao.getEntity(mid);
	}

	/**
	 * 更新专业
	 */
	public void updateMajor(Major model) {
		majorDao.updateEntity(model);
	}

	/**
	 * 删除专业
	 */
	public void deleteMajor(Integer mid) {
		String hql = "delete from Major m where m.id = ?";
		majorDao.batchEntityByHQL(hql, mid);
	}

	/**
	 * 按照学院id查询学院
	 */
	public College findCollege(Integer cid) {
		return collegeDao.getEntity(cid);
	}
	
	/**
	 * 根据学院查找系
	 * @author hjh
	 */
	public List<Major> findMajor(College c){
		String hql = "from Major m where m.college.id = ?";
		return majorDao.findEntityByHQL(hql, c.getId());
	}
	
	/**
	 * 根据id查找系
	 * @author hjh
	 */
	public Major getCollege(Integer cid) {
		return majorDao.getEntity(cid);
	}

}
