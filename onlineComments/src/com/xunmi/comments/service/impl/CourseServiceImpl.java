package com.xunmi.comments.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.xunmi.comments.dao.BaseDao;
import com.xunmi.comments.model.Answer;
import com.xunmi.comments.model.Course;
import com.xunmi.comments.model.CourseParent;
import com.xunmi.comments.model.Page;
import com.xunmi.comments.model.Question;
import com.xunmi.comments.model.Test;
import com.xunmi.comments.model.User;
import com.xunmi.comments.service.CourseService;

@Service("courseService")
public class CourseServiceImpl implements CourseService {

	@Resource(name = "userDao")
	private BaseDao<User> userDao;

	@Resource(name = "courseDao")
	private BaseDao<Course> courseDao;

	@Resource(name = "testDao")
	private BaseDao<Test> testDao;

	@Resource(name = "pageDao")
	private BaseDao<Page> pageDao;

	@Resource(name = "questionDao")
	private BaseDao<Question> questionDao;

	@Resource(name = "answerDao")
	private BaseDao<Answer> answerDao;

	@Resource(name = "courseParentDao")
	private BaseDao<CourseParent> courseParentDao;

	/**
	 * 新建课程
	 */
	public void newCourse(Course course, User user) {
		// 设置关联关系
		course.setUser(user);

		courseDao.saveEntity(course);
	}

	/**
	 * 我（教师）的课程集合
	 */
	public List<Course> findMycourses(User user) {
		String hql = "from Course c where c.t_id = ?";
		return courseDao.findEntityByHQL(hql, user.getId());
	}

	/**
	 * 我的已分配课程集合
	 */
	public List<Course> findMyallotedcourses(User user) {
		String hql = "from Course c where c.user.id = ?";
		return courseDao.findEntityByHQL(hql, user.getId());
	}

	/**
	 * 获取要更新的课程
	 */
	public Course getCourse(Integer cid) {
		return courseDao.getEntity(cid);
	}

	/**
	 * 更新课程信息
	 */
	public void updateCourse(Course model) {
		courseDao.updateEntity(model);

	}

	/**
	 * 通过tid查询
	 */
	public List<Course> findBytidcourses(Integer tid) {
		String hql = "from Course c where c.t_id = ?";
		return courseDao.findEntityByHQL(hql, tid);
	}

	/**
	 * 通过cid查询
	 */
	public List<Course> findBycidcourses(Integer cid) {
		String hql = "from Course c where c.courseParent.id = ?";
		return courseDao.findEntityByHQL(hql, cid);
	}

	/**
	 * 通过tid和cid查询
	 */
	public List<Course> findBytidAndcidcourses(Integer tid, Integer cid) {
		String hql = "from Course c where c.t_id = ? and c.courseParent.id = ?";
		return courseDao.findEntityByHQL(hql, tid, cid);
	}

	/**
	 * 我的课程下的实验列表
	 */
	public List<Test> findMytests(Integer cid) {
		String hql = "from Test t where t.course.id = ?";
		return testDao.findEntityByHQL(hql, cid);
	}

	/**
	 * 添加实验
	 */
	public void newTest(Test model, Integer cid) {
		// 设置关联关系
		model.setCourse(courseDao.getEntity(cid));
		testDao.saveEntity(model);
	}

	/**
	 * 获得要设计的实验
	 */
	public Test getTest(Integer tid) {
		Test t = testDao.getEntity(tid);
		t.getUsers().size();
		return t;
	}

	/**
	 * 获得要设计的实验，同时携带所有孩子
	 */
	public Test getTestWithChildren(Integer tid) {
		Test t = this.getTest(tid);
		// 强行初始化pages和questions集合
		for (Page page : t.getPages()) {
			page.getQuestions().size();
		}
		return t;
	}

	/**
	 * 保存/更新页面
	 */
	public void saveOrUpdatePage(Page model) {
		pageDao.saveOrUpdateEntity(model);
	}

	/**
	 * 获得要设计的页面
	 */
	public Page getPage(Integer pid) {
		return pageDao.getEntity(pid);
	}

	/**
	 * 获取要设计的页面，同时携带所有的孩子
	 */
	public Page getPageWithChridren(Integer pid) {
		Page p = this.getPage(pid);
		// 强行初始化questions集合
		p.getQuestions().size();
		return p;
	}

	/**
	 * 保存/更新问题
	 */
	public void saveOrUpdateQuestion(Question model) {
		questionDao.saveOrUpdateEntity(model);
	}

	/**
	 * 获取要设计的问题
	 */
	public Question getQuestion(Integer qid) {
		return questionDao.getEntity(qid);
	}

	/**
	 * 通过cpid查找course列表
	 */
	public List<Course> findBycpidCourses(Integer cpid) {
		String hql = "from Course c where c.courseParent.id = ?";
		return courseDao.findEntityByHQL(hql, cpid);
	}

	/**
	 * AJAX更新实验名字
	 */
	public void updateTest(Test model) {
		testDao.updateEntity(model);
	}

	/**
	 * AJAX更新页面名称
	 */
	public void updatePage(Page model) {
		pageDao.updateEntity(model);
	}

	/**
	 * 删除问题
	 */
	public void deleteQuestion(Integer qid) {
		String hql = "delete from Answer a where a.question.id = ?";
		answerDao.batchEntityByHQL(hql, qid);
		hql = "delete from Question q where q.id = ?";
		questionDao.batchEntityByHQL(hql, qid);
	}

	/**
	 * 删除页面，同时删除页面下的问题
	 */
	public void deletePage(Integer pid) {
		// 删除答案
		String hql = "delete from Answer a where a.question.id in (select q.id from Question q where q.page.id = ?)";
		answerDao.batchEntityByHQL(hql, pid);
		// 删除问题
		hql = "delete from Question q where q.page.id = ?";
		questionDao.batchEntityByHQL(hql, pid);
		// 删除页面
		hql = "delete from Page p where p.id = ? ";
		pageDao.batchEntityByHQL(hql, pid);
	}

	/**
	 * 删除实验，同时删除页面和问题
	 */
	public void deleteTest(Integer tid) {
		String hql = "delete from Answer a where a.question.id in (select q.id from Question q where q.page.id in (select p.id from Page p where p.test.id = ?))";
		answerDao.batchEntityByHQL(hql, tid);
		// 删除问题
		hql = "delete from Question q where q.page.id in (select p.id from Page p where p.test.id = ?)";
		questionDao.batchEntityByHQL(hql, tid);
		// 删除页面
		hql = "delete from Page p where p.test.id = ? ";
		pageDao.batchEntityByHQL(hql, tid);
		// 删除实验
		hql = "delete from Test t where t.id = ?";
		courseDao.batchEntityByHQL(hql, tid);
	}

	/**
	 * 删除课程，同时删除实验和页面和问题
	 */
	public void deleteCourse(Integer cid) {
		// 删除答案
		String hql = "delete from Answer a where a.question.id in (select q.id from Question q where q.page.id in (select p.id from Page p where p.test.id in (select t.id from Test t where t.course.id = ?)))";
		answerDao.batchEntityByHQL(hql, cid);
		// 删除问题
		hql = "delete from Question q where q.page.id in (select p.id from Page p where p.test.id in (select t.id from Test t where t.course.id = ?))";
		questionDao.batchEntityByHQL(hql, cid);
		// 删除页面
		hql = "delete from Page p where p.test.id in (select t.id from Test t where t.course.id = ?)";
		pageDao.batchEntityByHQL(hql, cid);
		// 删除实验
		hql = "delete from Test t where t.course.id = ?";
		testDao.batchEntityByHQL(hql, cid);
		// 删除课程
		hql = "delete from Course c where c.id = ?";
		courseDao.batchEntityByHQL(hql, cid);
	}

	/**
	 * 删除courseparent
	 */
	public void deleteCourseParent(Integer cpid) {
		// 删除答案
		String hql = "delete from Answer a where a.question.id in (select q.id from Question q where q.page.id in (select p.id from Page p where p.test.id in (select t.id from Test t where t.course.id in (select c.id from Course c where c.courseParent.id = ?))))";
		answerDao.batchEntityByHQL(hql, cpid);
		// 删除问题
		hql = "delete from Question q where q.page.id in (select p.id from Page p where p.test.id in (select t.id from Test t where t.course.id in (select c.id from Course c where c.courseParent.id = ?)))";
		questionDao.batchEntityByHQL(hql, cpid);
		// 删除页面
		hql = "delete from Page p where p.test.id in (select t.id from Test t where t.course.id in (select c.id from Course c where c.courseParent.id = ?))";
		pageDao.batchEntityByHQL(hql, cpid);
		// 删除实验
		hql = "delete from Test t where t.course.id in (select c.id from Course c where c.courseParent.id = ?)";
		testDao.batchEntityByHQL(hql, cpid);
		// 删除课程
		hql = "delete from Course c where c.courseParent.id = ?";
		courseDao.batchEntityByHQL(hql, cpid);
		// 删除courseparent
		courseParentDao.deleteEntity(courseParentDao.getEntity(cpid));
	}

	/**
	 * 学生课程列表
	 */
	public List<Course> findStudentcourses(User user) {
		String hql = "from Course c inner join fetch c.classrooms cr where cr.id = ?";
		return courseDao.findEntityByHQL(hql, user.getClassroom().getId());
	}

	/**
	 * 更新或保存实验
	 */
	public void saveOrUpdateTest(Test t) {
		testDao.saveOrUpdateEntity(t);
	}

	/**
	 * 查询已提交实验报告
	 * 
	 * @author hjh
	 */
	public List<User> findSubmitUsers(Integer tid) {
		String hql = "from User u inner join fetch u.tests t where t.id = ?";
		return userDao.findEntityByHQL(hql, tid);
	}

	/**
	 * 获得提交时间
	 * 
	 * @author hjh
	 */
	public Answer findAnswerByTid(Integer tid, Integer uid) {
		String hql = "from Answer a where a.t_id = ? and a.user.id = ?";
		return answerDao.findEntityByHQL(hql, tid, uid).get(0);
	}

	/**
	 * 获取学生答案
	 * 
	 * @author hjh
	 */
	public List<Answer> getUserAnswers(Integer uid) {
		String hql = "from Answer a where a.user.id = ?";
		return answerDao.findEntityByHQL(hql, uid);
	}
	
	/**
	 * 分页查询
	 * @param cid
	 * @param showNumber
	 * @param currPage
	 * @return
	 */
	public List<Test> findMytestsPaging(Integer cid, Integer currPage,
			Integer showNumber) {
		String hql = "from Test t where t.course.id = ?";
		return testDao.findEntityByHQLPaging(hql,currPage,showNumber,cid);
	}

	/**
	 * 添加批注
	 * 
	 * @author JFW-PC
	 */
	public void addSolutionCommets(Answer answer) {
		answerDao.updateEntity(answer);
	}
}
