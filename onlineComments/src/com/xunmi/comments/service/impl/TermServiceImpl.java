package com.xunmi.comments.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.xunmi.comments.dao.BaseDao;
import com.xunmi.comments.model.Term;
import com.xunmi.comments.service.TermService;

@Service("termService")
public class TermServiceImpl implements TermService {

	@Resource(name="termDao")
	private BaseDao<Term> termDao;
	
	/**
	 * 查找所有学期
	 */
	public List<Term> findAllterms(){
		String hql = "from Term";
		return termDao.findEntityByHQL(hql);
	}
}
