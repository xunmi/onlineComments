package com.xunmi.comments.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.xunmi.comments.dao.BaseDao;
import com.xunmi.comments.model.College;
import com.xunmi.comments.service.CollegeService;

@Service("collegeService")
public class CollegeServiceImpl extends BaseServiceImpl<College> implements
		CollegeService {

	/**
	 * 重写该方法，目的是为了覆盖超类中该方法的注解，指明注入指定的dao对象，否则spring 无法确定注入哪个dao(有四个满足条件的dao)
	 */
	@Resource(name = "collegeDao")
	public void setBaseDao(BaseDao<College> baseDao) {
		super.setBaseDao(baseDao);
	}

	/**
	 * 按照id查询college
	 */
	public College getCollege(Integer cid) {
		return this.getEntity(cid);
	}

	/**
	 * 查询所有的college
	 */
	public List<College> findAllCollege() {
		String hql = "from College";
		return this.findEntityByHQL(hql);
	}

	/**
	 * 更新学院
	 */
	public void updateCollege(College model) {
		this.updateEntity(model);
	}

	/**
	 * 删除学院
	 */
	public void deleteCollege(Integer cid) {
		String hql = "delete from College c where c.id = ?";
		this.batchEntityByHQL(hql, cid);
	}

	/**
	 * 查找学院列表
	 * 
	 * @author hjh
	 */
	public List<College> findAllcolleges() {
		String hql = "from College c";
		return this.findEntityByHQL(hql);
	}

}
