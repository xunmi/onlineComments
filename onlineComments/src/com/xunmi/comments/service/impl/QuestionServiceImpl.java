package com.xunmi.comments.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.xunmi.comments.dao.BaseDao;
import com.xunmi.comments.model.Question;
import com.xunmi.comments.service.QuestionService;

@Service("questionService")
public class QuestionServiceImpl implements QuestionService {

	@Resource(name="questionDao")
	private BaseDao<Question> questionDao;
	
	/**
	 * ��ȡquestion
	 */
	public Question getQuestion(Integer qid) {
		return questionDao.getEntity(qid);
	}
}
