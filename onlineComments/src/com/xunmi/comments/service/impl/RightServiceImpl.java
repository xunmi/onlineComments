package com.xunmi.comments.service.impl;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.xunmi.comments.dao.BaseDao;
import com.xunmi.comments.model.security.Right;
import com.xunmi.comments.service.RightService;
import com.xunmi.comments.util.StringUtil;
import com.xunmi.comments.util.ValidateUtil;

@Service("rightService")
public class RightServiceImpl extends BaseServiceImpl<Right> implements
		RightService {

	/**
	 * 重写该方法，目的是为了覆盖超类中该方法的注解，指明注入指定的dao对象，否则spring 无法确定注入哪个dao(有四个满足条件的dao)
	 */
	@Resource(name = "rightDao")
	public void setBaseDao(BaseDao<Right> baseDao) {
		super.setBaseDao(baseDao);
	}

	/**
	 * 保存或者更新权限
	 */
	public void saveOrUpdateRight(Right r) {
		// 插入操作
		int pos = 0;
		long code = 1L;
		if (r.getId() == null) {
			// 方案一，效率太低
			// String hql = "from Right r order by r.rightPos,r.rightCode desc";
			// List<Right> rights = this.findEntityByHQL(hql);
			// if (!ValidateUtil.isValid(rights)) {
			// pos = 0;
			// code = 1L;
			// } else {
			// // 得到最上面的right
			// Right top = rights.get(0);
			// int topPos = top.getRightPos();
			// long topCode = top.getRightCode();
			// // 判断权限码是否达到最大值
			// if (topCode >= (1L << 60)) {
			// pos = topPos + 1;
			// code = 1;
			// } else {
			// pos = topPos;
			// code = topCode << 1;
			// }
			// }

			// 方案二
			String hql = "select max(r.rightPos),max(r.rightCode) from Right r "
					+ "where r.rightPos = (select max(rr.rightPos) from Right rr)";
			Object[] arr = (Object[]) this.uniqueResult(hql);
			Integer topPos = (Integer) arr[0];
			Long topCode = (Long) arr[1];
			// 没有权限
			if (topPos == null) {
				pos = 0;
				code = 1L;
			} else {
				// 权限码是否达到最大值
				if (topCode >= (1L << 60)) {
					pos = topPos + 1;
					code = 1L;
				} else {
					pos = topPos;
					code = topCode << 1;
				}
			}
			r.setRightCode(code);
			r.setRightPos(pos);
		}
		this.saveOrUpdateEntity(r);
	}

	/**
	 * 按照url追加权限
	 */
	public void appendRightByURL(String url) {
		String hql = "select count(*) from Right r where r.rightUrl = ?";
		Long count = (Long) this.uniqueResult(hql, url);
		// 判断是否已有权限
		if (count == 0) {
			Right r = new Right();
			r.setRightUrl(url);
			this.saveOrUpdateRight(r);
		}
	}

	/**
	 * 批量更新权限
	 */
	public void batchUpdateRights(List<Right> allRights) {
		String hql = "update Right r set r.rightName = ?,r.common = ? where r.id = ?";
		if (ValidateUtil.isValid(allRights)) {
			for (Right r : allRights) {
				this.batchEntityByHQL(hql, r.getRightName(), r.isCommon(),
						r.getId());
			}
		}
	}

	/**
	 * 查询在指定范围内的权限
	 */
	public List<Right> findAllRightsInRange(Integer[] ownRightIds) {
		if (ValidateUtil.isValid(ownRightIds)) {
			String hql = "from Right r where r.id in ("
					+ StringUtil.arr2Str(ownRightIds) + ")";
			return this.findEntityByHQL(hql);
		}
		return null;
	}

	/**
	 * 查找不在指定范围的权限
	 */
	public List<Right> findRightsNotInRange(Set<Right> rights) {
		if (!ValidateUtil.isValid(rights)) {
			return this.findAllEntities();
		} else {
			String hql = "from Right r where r.id not in("
					+ extractRightIds(rights) + ")";
			return this.findEntityByHQL(hql);
		}
	}

	/**
	 * 抽取所有Right的id，形成字符串,","号分割
	 */
	private String extractRightIds(Set<Right> rights) {
		String temp = "";
		if (ValidateUtil.isValid(rights)) {
			for (Right r : rights) {
				temp = temp + r.getId() + ",";
			}
			return temp.substring(0, temp.length() - 1);
		}
		return temp;
	}

	/**
	 * 查询最大权限位
	 */
	public int getMaxRightPos() {
		String hql = "select max(r.rightPos) from Right r";
		Integer pos = (Integer) this.uniqueResult(hql);
		return pos == null ? 0 : pos;
	}
}
