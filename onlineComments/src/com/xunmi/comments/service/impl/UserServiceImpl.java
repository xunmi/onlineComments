package com.xunmi.comments.service.impl;

import java.util.HashSet;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.xunmi.comments.dao.BaseDao;
import com.xunmi.comments.model.Classroom;
import com.xunmi.comments.model.College;
import com.xunmi.comments.model.User;
import com.xunmi.comments.model.security.Role;
import com.xunmi.comments.service.RoleService;
import com.xunmi.comments.service.UserService;
import com.xunmi.comments.util.ValidateUtil;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Resource
	private RoleService roleService;

	/**
	 * 重写该方法，目的是为了覆盖超类中该方法的注解，指明注入指定的dao对象，否则spring 无法确定注入哪个dao(有四个满足条件的dao)
	 */
	@Resource(name = "userDao")
	private BaseDao<User> userDao;

	@Resource(name = "collegeDao")
	private BaseDao<College> collegeDao;

	@Resource(name = "classroomDao")
	private BaseDao<Classroom> classroomDao;

	/**
	 * 校验登录信息
	 */
	public User validateLoginInfo(String username, String md5) {
		String hql = "from User u where u.username = ? and u.password = ?";
		List<User> list = userDao.findEntityByHQL(hql, username, md5);
		return ValidateUtil.isValid(list) ? list.get(0) : null;
	}

	/**
	 * 通过excel文件批量插入用户
	 */
	public void batchEntityByFile(List<User> userList) {
		int num = userList.size();
		for (int i = 0; i < num; i++) {
			userDao.saveEntity(userList.get(i));
		}
	}

	/**
	 * 查询所有的学院
	 */
	public List<College> findAllColleges() {
		String hql = "from College";
		return collegeDao.findEntityByHQL(hql);
	}

	/**
	 * 按照用户类型和班级id查询用户
	 */
	public List<User> findPersonByRoleIdAndClassId(char type, Integer classId) {
		String hql = "from User u where u.type = ? and u.classroom.id = ?";
		return userDao.findEntityByHQL(hql, type, classId);
	}

	/**
	 * 添加用户，依据type和classId添加用户
	 */
	public void addUserByTypeAndClassId(User model) {
		userDao.saveEntity(model);
	}

	/**
	 * 按照用户id查询用户
	 */
	public User findUserByUid(Integer uid) {
		return userDao.getEntity(uid);
	}

	/**
	 * 更新用户
	 */
	public void updateUser(User model) {
		userDao.updateEntity(model);
	}

	/**
	 * 删除用户
	 */
	public void deleteUser(Integer uid) {
		User u = userDao.getEntity(uid);
		userDao.deleteEntity(u);
	}

	/**
	 * 通过班级id查询班级
	 */
	public Classroom findClassroomByCid(Integer classId) {
		return classroomDao.getEntity(classId);
	}

	/**
	 * 查询所有教师
	 */
	public List<User> findAllTeacher() {
		String hql = "from User u where u.type = 1";
		return userDao.findEntityByHQL(hql);
	}

	/**
	 * 根据t_id查找user
	 * 
	 * @author hjh
	 */
	public User findUser(Integer t_id) {
		return userDao.getEntity(t_id);
	}

	/**
	 * 查找教师选择列表
	 * 
	 * @author hjh
	 */
	public List<User> findUser(College c) {
		String hql = "from User u where u.college.id = ?";
		return userDao.findEntityByHQL(hql, c.getId());
	}

	/**
	 * 查找所有user
	 * 
	 * @author hjh
	 */
	public List<User> findAllusers() {
		String hql = "from User";
		return userDao.findEntityByHQL(hql);
	}

	/**
	 * 查询所有未分配学院的教师
	 */
	public List<User> findAllUndisTeachers() {
		String hql = "from User where type = '1' and college.id = null";
		return userDao.findEntityByHQL(hql);
	}

	/**
	 * 教师分配学院
	 */
	public void distributeTeacher(User model) {
		userDao.updateEntity(model);
	}

	/**
	 * 更新用户授权（只能更新角色设置，用户信息不改变）
	 */
	public void updateAuthorize(User model, Integer[] ownRoleIds) {
		// 查询新对象，不可对原有对象操纵
		User newUser = this.findUserByUid(model.getId());
		if (!ValidateUtil.isValid(ownRoleIds)) {
			newUser.getRoles().clear();
		} else {
			List<Role> roles = roleService.findRolesInRange(ownRoleIds);
			newUser.setRoles(new HashSet<Role>(roles));
		}
	}

	/**
	 * 清除用户授权
	 */
	public void clearAuthorize(Integer userId) {
		this.findUserByUid(userId).getRoles().clear();
	}
}
