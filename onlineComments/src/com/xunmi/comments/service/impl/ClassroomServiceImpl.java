package com.xunmi.comments.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.xunmi.comments.dao.BaseDao;
import com.xunmi.comments.model.Classroom;
import com.xunmi.comments.model.College;
import com.xunmi.comments.model.Major;
import com.xunmi.comments.service.ClassroomService;

/**
 * ClassroomServiceImpl
 */
@Service("classroomService")
public class ClassroomServiceImpl implements ClassroomService {

	@Resource(name = "collegeDao")
	private BaseDao<College> collegeDao;

	@Resource(name = "classroomDao")
	private BaseDao<Classroom> classroomDao;

	@Resource(name = "majorDao")
	private BaseDao<Major> majorDao;

	/**
	 * 查询所有学院
	 */
	public List<College> findAllCollege() {
		String hql = "from College";
		return collegeDao.findEntityByHQL(hql);
	}

	/**
	 * 按照专业id查找对应的课程
	 */
	public List<Classroom> findClassroomByMid(Integer mid) {
		String hql = "from Classroom c where c.major.id = ?";
		return classroomDao.findEntityByHQL(hql, mid);
	}

	/**
	 * 处理添加班级
	 */
	public void addClassroom(Classroom model, Integer mid) {
		Major m = new Major();
		m.setId(mid);
		// 设置关联关系
		model.setMajor(m);
		classroomDao.saveEntity(model);
	}

	/**
	 * 按照班级id查询班级
	 */
	public Classroom findClassroomByCid(Integer cid) {
		return classroomDao.getEntity(cid);
	}

	/**
	 * 更新班级信息
	 */
	public void updateClassroom(Classroom model) {
		classroomDao.updateEntity(model);
	}

	/**
	 * 按照班级id删除班级
	 */
	public void deleteClassroom(Integer cid) {
		String hql = "delete from Classroom c where c.id = ?";
		classroomDao.batchEntityByHQL(hql, cid);
	}

	/**
	 * 按照专业id查询专业
	 */
	public Major findMajorByCid(Integer mid) {
		return majorDao.getEntity(mid);
	}

	/**
	 * 按照专业id,学期查询对应的课程
	 */
	public List<Classroom> findClassroomByMidAndSemester(Integer mid,
			String semester) {
		String hql = "from Classroom c where c.major.id = ? and createtime like ?";
		// 查找匹配的字符串
		semester = "%" + semester + "%";
		return classroomDao.findEntityByHQL(hql, mid, semester);
	}

	/**
	 * 依据专业id查询学制年数
	 */
	public Integer findMajorYearsByMid(Integer mid) {
		Major m = majorDao.getEntity(mid);
		return m.getMajorYears();
	}

	/**
	 * 获取classroom
	 * 
	 * @author hjh
	 */
	public Classroom getClassroom(Integer id) {
		return classroomDao.getEntity(id);
	}

	/**
	 * 根据major查找classroom
	 * 
	 * @author hjh
	 */
	public List<Classroom> findClassroom(Major m) {
		String hql = "from Classroom cr where cr.major.id = ?";
		return classroomDao.findEntityByHQL(hql, m.getId());
	}

}
