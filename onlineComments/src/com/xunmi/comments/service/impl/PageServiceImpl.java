package com.xunmi.comments.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.xunmi.comments.dao.BaseDao;
import com.xunmi.comments.model.Answer;
import com.xunmi.comments.model.Page;
import com.xunmi.comments.service.PageService;

@Service("pageService")
public class PageServiceImpl implements PageService {
	
	@Resource(name="pageDao")
	private BaseDao<Page> pageDao;
	@Resource(name="answerDao")
	private BaseDao<Answer> answerDao;
	
	/**
	 * 根据tid查找pages,连同questions子节点
	 */
	public List<Page> findPagesWithChildren(Integer tid) {
		String hql = "from Page p where p.test.id = ? order by p.orderno asc";
		List<Page> pages = pageDao.findEntityByHQL(hql, tid);
		for(Page p : pages) {
			p.getQuestions().size();
		}
		return pages;
	}
	
	/**
	 * 获取page
	 */
	public Page getPage(Integer currPid) {
		return pageDao.getEntity(currPid);
	}
	
	/**
	 * 查询指定页面的下一页
	 */
	public Page getNextPage(Page targPage) {
		String hql = "from Page p where p.test.id = ? and p.orderno > ? order by p.orderno asc" ;
		List<Page> list = pageDao.findEntityByHQL(hql, targPage.getTest().getId(),targPage.getOrderno());
		list.get(0).getQuestions().size();
		return list.get(0);
	}
	
	/**
	 * 查询指定页面的上一页
	 */
	public Page getPrePage(Page targPage) {
		String hql = "from Page p where p.test.id = ? and p.orderno < ? order by p.orderno desc" ;
		List<Page> list = pageDao.findEntityByHQL(hql, targPage.getTest().getId(),targPage.getOrderno());
		list.get(0).getQuestions().size();
		return list.get(0);
	}
	
	/**
	 * 判断指定页面是否是所在调查尾页
	 */
	public boolean isLastPage(Page targPage) {
		String hql = "select count(*) from Page p where p.survey.id = ? and p.orderno > ? " ;
		Long count = (Long) pageDao.uniqueResult(hql, targPage.getTest().getId(),targPage.getOrderno());
		return count == 0;
	}
	
	/**
	 * 批量保存答案
	 */
	public void saveAnswers(List<Answer> list) {
		Date date = new Date();
		for(Answer a : list){
			a.setSubmitTime(date);
			answerDao.saveEntity(a);
		}
	}
}
