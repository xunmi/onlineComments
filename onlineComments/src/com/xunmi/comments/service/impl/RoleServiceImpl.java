package com.xunmi.comments.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.xunmi.comments.dao.BaseDao;
import com.xunmi.comments.model.security.Right;
import com.xunmi.comments.model.security.Role;
import com.xunmi.comments.service.RightService;
import com.xunmi.comments.service.RoleService;
import com.xunmi.comments.util.StringUtil;
import com.xunmi.comments.util.ValidateUtil;

@Service("roleService")
public class RoleServiceImpl extends BaseServiceImpl<Role> implements
		RoleService {

	@Resource
	private RightService rightService;

	/**
	 * 重写该方法，目的是为了覆盖超类中该方法的注解，指明注入指定的dao对象，否则spring 无法确定注入哪个dao(有四个满足条件的dao)
	 */
	@Resource(name = "roleDao")
	public void setBaseDao(BaseDao<Role> baseDao) {
		super.setBaseDao(baseDao);
	}

	/**
	 * 保存或者添加角色
	 */
	public void saveOrUpdateRole(Role model, Integer[] ownRightIds) {
		// 没有给角色授予任何权限
		if (!ValidateUtil.isValid(ownRightIds)) {
			model.getRights().clear();
		} else {
			List<Right> rights = rightService.findAllRightsInRange(ownRightIds);
			model.setRights(new HashSet<Right>(rights));
		}
		this.saveOrUpdateEntity(model);
	}

	/**
	 * 查询不在指定范围的角色
	 */
	public List<Role> findRolesNotInRange(Set<Role> roles) {
		if (!ValidateUtil.isValid(roles)) {
			return this.findAllEntities();
		} else {
			String hql = "from Role r where r.id not in ("
					+ extractRoleIds(roles) + ")";
			return this.findEntityByHQL(hql);
		}
	}

	/**
	 * 抽取所有Right的id，形成字符串,","号分割
	 */
	private String extractRoleIds(Set<Role> roles) {
		String temp = "";
		if (ValidateUtil.isValid(roles)) {
			for (Role r : roles) {
				temp = temp + r.getId() + ",";
			}
			return temp.substring(0, temp.length() - 1);
		}
		return temp;
	}

	/**
	 * 查询在指定范围的角色
	 */
	public List<Role> findRolesInRange(Integer[] ownRoleIds) {
		if (ValidateUtil.isValid(ownRoleIds)) {
			String hql = "from Role r where r.id in ("
					+ StringUtil.arr2Str(ownRoleIds) + ")";
			return this.findEntityByHQL(hql);
		}
		return null;
	}
}
