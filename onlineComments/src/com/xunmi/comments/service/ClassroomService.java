package com.xunmi.comments.service;

import java.util.List;

import com.xunmi.comments.model.Classroom;
import com.xunmi.comments.model.College;
import com.xunmi.comments.model.Major;

public interface ClassroomService {

	/**
	 * 查询所有学院
	 */
	List<College> findAllCollege();

	/**
	 * 按照专业id查找对应的课程
	 */
	List<Classroom> findClassroomByMid(Integer mid);

	/**
	 * 添加班级
	 */
	void addClassroom(Classroom model, Integer mid);

	/**
	 * 按照班级id查询班级
	 */
	Classroom findClassroomByCid(Integer cid);

	/**
	 * 更新班级信息
	 */
	void updateClassroom(Classroom model);

	/**
	 * 按照班级id删除班级
	 */
	void deleteClassroom(Integer cid);

	/**
	 * 按照专业id查询专业
	 */
	Major findMajorByCid(Integer mid);

	/**
	 * 按照专业id,学期查询对应的课程
	 */
	List<Classroom> findClassroomByMidAndSemester(Integer mid, String semester);

	/**
	 * 依据专业id查询学制年数
	 */
	Integer findMajorYearsByMid(Integer mid);

	/**
	 * 获取classroom
	 * 
	 * @author hjh
	 */
	public Classroom getClassroom(Integer id);

	/**
	 * 根据major查找classroom
	 * 
	 * @author hjh
	 */
	public List<Classroom> findClassroom(Major m);

}
