package com.xunmi.comments.service;

import java.util.List;

import com.xunmi.comments.model.Classroom;
import com.xunmi.comments.model.College;
import com.xunmi.comments.model.User;

/**
 * UserService
 */
public interface UserService {

	/**
	 * 校验登录信息
	 */
	public User validateLoginInfo(String username, String md5);

	/**
	 * 通过excel文件批量插入用户
	 */
	public void batchEntityByFile(List<User> userList);

	/**
	 * 查询所有的学院
	 */
	public List<College> findAllColleges();

	/**
	 * 按照用户类型和班级id查询用户
	 */
	public List<User> findPersonByRoleIdAndClassId(char type, Integer classId);

	/**
	 * 添加用户，依据type和classId添加用户
	 */
	public void addUserByTypeAndClassId(User model);

	/**
	 * 按照用户id查询用户
	 */
	public User findUserByUid(Integer uid);

	/**
	 * 更新用户
	 */
	public void updateUser(User model);

	/**
	 * 删除用户
	 */
	public void deleteUser(Integer uid);

	/**
	 * 通过班级id查询班级
	 */
	public Classroom findClassroomByCid(Integer classId);

	/**
	 * 查询所有老师
	 */
	public List<User> findAllTeacher();

	/**
	 * 查找教师选择列表
	 * 
	 * @author hjh
	 */
	public List<User> findUser(College c);

	/**
	 * 根据t_id查找user
	 * 
	 * @author hjh
	 */
	public User findUser(Integer t_id);

	/**
	 * 查找所有user
	 * 
	 * @author hjh
	 */
	public List<User> findAllusers();

	/**
	 * 查询所有未分配学院的教师
	 */
	public List<User> findAllUndisTeachers();

	/**
	 * 教师分配学院
	 */
	public void distributeTeacher(User model);

	/**
	 * 更新用户授权（只能更新角色设置，用户信息不改变）
	 */
	public void updateAuthorize(User model, Integer[] ownRoleIds);

	/**
	 * 清除用户授权角色
	 */
	public void clearAuthorize(Integer userId);

}
