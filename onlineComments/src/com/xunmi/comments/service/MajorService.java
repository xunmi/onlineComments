package com.xunmi.comments.service;

import java.util.List;

import com.xunmi.comments.model.College;
import com.xunmi.comments.model.Major;

public interface MajorService {

	/**
	 * 查询所有学院
	 */
	List<College> findAllColleges();

	/**
	 * 按照学院id查询所有专业
	 */
	List<Major> findAllMajorsByCid(Integer cid);

	/**
	 * 添加抓也
	 */
	void addMajor(Major model, Integer cid);

	/**
	 * 按照id查询专业
	 */
	Major findMajorByMid(Integer mid);

	/**
	 * 更新专业
	 */
	void updateMajor(Major model);

	/**
	 * 删除专业
	 */
	void deleteMajor(Integer mid);

	/**
	 * 按照学院id查询学院
	 */
	College findCollege(Integer cid);

	/**
	 * 根据学院查找系
	 * 
	 * @author hjh
	 */
	public List<Major> findMajor(College c);

	/**
	 * 根据id查找系
	 * 
	 * @author hjh
	 */
	public Major getCollege(Integer cid);

}
