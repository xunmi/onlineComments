package com.xunmi.comments.actions;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.xunmi.comments.model.College;
import com.xunmi.comments.model.Course;
import com.xunmi.comments.model.CourseParent;
import com.xunmi.comments.model.User;
import com.xunmi.comments.service.ClassroomService;
import com.xunmi.comments.service.CollegeService;
import com.xunmi.comments.service.CourseParentService;
import com.xunmi.comments.service.CourseService;
import com.xunmi.comments.service.UserService;
import com.xunmi.comments.aware.UserAware;

@Controller
@Scope("prototype")
public class CourseAction extends BaseAction<Course> implements UserAware{

	private static final long serialVersionUID = -2373128774489031486L;
	
	private User user;

	//注入User对象
	public void setUser(User user) {
		this.user = user;
	}

	//注入courseService
	@Resource
	private CourseService courseService;
	//注入userService
	@Resource
	private UserService userService;
	//注入courseParentService
	@Resource
	private CourseParentService courseParentService;
	//注入collegeService
	@Resource
	private CollegeService collegeService;
	//注入classroomService
	@Resource
	private ClassroomService classroomService;
	

	//课程集合
	private List<Course> myCourses;
	//学院集合
	private List<College> colleges;
	//Admin课程集合
	private List<CourseParent> courseParents;
	public List<Course> getMyCourses() {
		return myCourses;
	}
	public void setMyCourses(List<Course> myCourses) {
		this.myCourses = myCourses;
	}
	public List<College> getColleges() {
		return colleges;
	}
	public void setColleges(List<College> colleges) {
		this.colleges = colleges;
	}
	public List<CourseParent> getCourseParents() {
		return courseParents;
	}
	public void setCourseParents(List<CourseParent> courseParents) {
		this.courseParents = courseParents;
	}

	//接收cid参数(course)
	private Integer cid;
	//接收cpid
	private Integer cpid;
	//接收tid
	private Integer tid;
	//接收crid
	private Integer crid;

	public Integer getCid() {
		return cid;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	public Integer getCpid() {
		return cpid;
	}

	public void setCpid(Integer cpid) {
		this.cpid = cpid;
	}

	public Integer getTid() {
		return tid;
	}

	public void setTid(Integer tid) {
		this.tid = tid;
	}
	public Integer getCrid() {
		return crid;
	}
	public void setCrid(Integer crid) {
		this.crid = crid;
	}
	
	/**
	 * 到达添加课程的页面
	 
	public String toAddCoursePage() {
		return "addCoursePage" ;
	}*/
	
	/**
	 * 新建课程
	 
	public String newCourse() {
		courseService.newCourse(this.model,user);
		return "myCoursesAction" ;
	}*/
	
	/**
	 * 到达我（教师）的课程的页面
	 */
	public String myCourses() {
		this.myCourses = courseService.findMycourses(user);
		return "myCourseListPage" ;
	}
	
	/**
	 * 到达我（教师）的课程的页面批阅
	 */
	public String myCoursesEndorse() {
		this.myCourses = courseService.findMycourses(user);
		return "myCourseListEndorsePage" ;
	}
	
	/**
	 * 到达我（学生）的课程的页面
	 */
	public String studentCourses() {
		this.myCourses = courseService.findStudentcourses(user);
		for(Course c : myCourses) {
			c.setUser(userService.findUser(c.getT_id()));;
		}
		return "studentCourseListPage" ;
	}
	
	
	/**
	 * 到达更新课程的页面
	 
	public String toUpdateCourse() {
		this.terms = termService.findAllterms();
		this.colleges = collegeService.findAllcolleges();
		this.users = userService.findAllusers();
		this.model = courseService.getCourse(cid);
		return "updateCoursePage";
	}*/
	
	/**
	 * 更新课程信息
	 
	public String doUpdateCourse() {
		this.cid = model.getId();
		//保持关联关系
		model.setUser(user);
		courseService.updateCourse(model);
		return "myAllotedCourseAction";
	}*/
	
	/**
	 * 删除课程
	 */
	public String doDeleteCourse() {
		//删除中间表关系
		Course course = courseService.getCourse(cid);
		course.setClassrooms(null);
		courseService.updateCourse(course);
		
		courseService.deleteCourse(cid);
		return "myAllotedCourseAction";
	}

	/**
	 * 到达已分配课程的页面
	 */
	public String toMyAllotedCoursePage() {
		this.colleges = collegeService.findAllcolleges();
		this.courseParents = courseParentService.findAllcourseparents();
		this.myCourses =  courseService.findMyallotedcourses(user);
		for(Course c : myCourses) {
			c.setUser(userService.findUser(c.getT_id()));
		}
		
		return "myAllotedCoursePage";
	}
	
	/**
	 * do分配课程
	 */
	public String doAlloteCourse() {
		CourseParent cp = courseParentService.getCourseParent(cpid);
		this.model.setCode(cp.getCode());
		this.model.setDescription(cp.getDescription());
		this.model.setName(cp.getName());
		this.model.setT_id(tid);
		this.model.setCourseParent(cp);
		this.model.getClassrooms().add(classroomService.getClassroom(crid));
		courseService.newCourse(this.model,user);
		return "myAllotedCourseAction";
	}
	
	/**
	 * 查询课程分配
	 */
	public String doInquireCourse() {
		this.colleges = collegeService.findAllcolleges();
		this.courseParents = courseParentService.findAllcourseparents();
		this.myCourses =  courseService.findMyallotedcourses(user);
		if(tid == 0 && cid == 0) {
			this.myCourses =  courseService.findMyallotedcourses(user);
			for(Course c : myCourses) {
				c.setUser(userService.findUser(c.getT_id()));
			}
		}else if(tid != 0 && cid == 0) {
			this.myCourses = courseService.findBytidcourses(tid);
			for(Course c : myCourses) {
				c.setUser(userService.findUser(c.getT_id()));
			}
		}else if(tid == 0 && cid != 0) {
			this.myCourses = courseService.findBycidcourses(cid);
			for(Course c : myCourses) {
				c.setUser(userService.findUser(c.getT_id()));
			}
		}else if(tid != 0 && cid != 0) {
			this.myCourses = courseService.findBytidAndcidcourses(tid,cid);
			for(Course c : myCourses) {
				c.setUser(userService.findUser(c.getT_id()));
			}
		}
		return "myAllotedCoursePage";
	}
}
