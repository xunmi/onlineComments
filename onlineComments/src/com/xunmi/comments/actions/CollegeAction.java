package com.xunmi.comments.actions;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.xunmi.comments.model.College;
import com.xunmi.comments.service.CollegeService;

/**
 * 学院action
 */
@Controller
@Scope("prototype")
public class CollegeAction extends BaseAction<College> {

	private static final long serialVersionUID = -1204547801614577458L;

	// 学院集合
	private List<College> colleges;

	private Integer cid;

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public List<College> getColleges() {
		return colleges;
	}

	public void setColleges(List<College> colleges) {
		this.colleges = colleges;
	}

	// 注入collegeService
	@Resource
	private CollegeService collegeService;

	/**
	 * 学院管理页面
	 */
	public String collegeManage() {
		this.colleges = collegeService.findAllCollege();
		return "collegeManage";
	}

	/**
	 * 添加一个学院
	 */
	public String addCollege() {
		return "addCollege";
	}

	/**
	 * 处理添加的学院
	 */
	public String doAddCollege() {
		collegeService.saveEntity(model);
		return "doAddCollege";
	}

	/**
	 * 编辑学院
	 */
	public String editCollege() {
		this.model = collegeService.getCollege(cid);
		return "editCollege";
	}

	/**
	 * 更新学院
	 */
	public String updateCollege() {
		collegeService.updateCollege(model);
		return "updateCollege";
	}

	/**
	 * 删除学院
	 */
	public String deleteCollege() {
		collegeService.deleteCollege(cid);
		return "deleteCollege";
	}

}
