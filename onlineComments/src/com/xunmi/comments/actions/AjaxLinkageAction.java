package com.xunmi.comments.actions;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;
import com.xunmi.comments.model.Classroom;
import com.xunmi.comments.model.College;
import com.xunmi.comments.model.Major;
import com.xunmi.comments.model.User;
import com.xunmi.comments.service.ClassroomService;
import com.xunmi.comments.service.CollegeService;
import com.xunmi.comments.service.MajorService;
import com.xunmi.comments.service.UserService;

@Controller
@Scope("prototype")
public class AjaxLinkageAction extends ActionSupport {

	private static final long serialVersionUID = -7607719515862198592L;
	
	//接收js传过来的值，可能是
	private Integer cid;
	private List<User> users;
	private List<Classroom> classrooms;
	private List<Major> majors;
	private String resultTree;
	
	public Integer getCid() {
		return cid;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
	public String getResultTree() {
		return resultTree;
	}
	public void setResultTree(String resultTree) {
		this.resultTree = resultTree;
	}
	public List<Classroom> getClassrooms() {
		return classrooms;
	}
	public void setClassrooms(List<Classroom> classrooms) {
		this.classrooms = classrooms;
	}
	public List<Major> getMajors() {
		return majors;
	}
	public void setMajors(List<Major> majors) {
		this.majors = majors;
	}


	//注入userService
	@Resource
	private UserService userService;
	//注入collegeService
	@Resource
	private CollegeService collegeService;
	//注入classroomService
	@Resource
	private ClassroomService classroomService;
	//注入majorService
	@Resource
	private MajorService majorService;
	
	/**
	 * 学院与教师联动
	 */
	public String collegeToTeacher(){
		College c = collegeService.getCollege(cid);
		this.users = userService.findUser(c);
		for(User u : users) {
			u.setCollege(null);
			u.setClassroom(null);
		}
		JSONArray jsonArray = JSONArray.fromObject(users);
		resultTree = jsonArray.toString();
		return "success";
	}
	
	/**
	 * 学院与系联动
	 */
	public String collegeToMajor(){
		College c = collegeService.getCollege(cid);
		this.majors = majorService.findMajor(c);
		for(Major m : majors) {
			m.setClassrooms(null);
			m.setCollege(null);
		}
		JSONArray jsonArray = JSONArray.fromObject(majors);
		resultTree = jsonArray.toString();
		return "success";
	}
	
	/**
	 * 系与班级联动
	 */
	public String majorToClassroom(){
		Major m = majorService.getCollege(cid);
		this.classrooms = classroomService.findClassroom(m);
		for(Classroom cr : classrooms) {
			cr.setCourses(null);
			cr.setMajor(null);
			cr.setUsers(null);
		}
		JSONArray jsonArray = JSONArray.fromObject(classrooms);
		resultTree = jsonArray.toString();
		return "success";
	}
	
	
}
