package com.xunmi.comments.actions;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.xunmi.comments.model.College;
import com.xunmi.comments.model.Major;
import com.xunmi.comments.service.MajorService;

/**
 * 专业action
 */
@Controller
@Scope("prototype")
public class MajorAction extends BaseAction<Major> {

	private static final long serialVersionUID = -8713621676465630378L;

	@Resource
	private MajorService majorService;

	// 学院集合
	private List<College> colleges;

	// 学院id
	private Integer cid;

	// 专业集合
	private List<Major> majors;

	// 专业id
	private Integer mid;

	public Integer getMid() {
		return mid;
	}

	public void setMid(Integer mid) {
		this.mid = mid;
	}

	public List<Major> getMajors() {
		return majors;
	}

	public void setMajors(List<Major> majors) {
		this.majors = majors;
	}

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public List<College> getColleges() {
		return colleges;
	}

	public void setColleges(List<College> colleges) {
		this.colleges = colleges;
	}

	/**
	 * 专业列表
	 */
	public String majorManage() {
		this.colleges = majorService.findAllColleges();
		return "majorManage";
	}

	/**
	 * 按照学院id所属专业
	 */
	public String checkMajor() {
		model.setCollege(majorService.findCollege(cid));
		this.majors = majorService.findAllMajorsByCid(cid);
		return "checkMajor";
	}

	/**
	 * 添加专业
	 */
	public String addMajor() {
		model.setCollege(majorService.findCollege(cid));
		return "addMajor";
	}

	/**
	 * 处理添加
	 */
	public String doAddMajor() {
		majorService.addMajor(model, cid);
		return "doAddMajor";
	}

	/**
	 * 编辑专业
	 */
	public String editMajor() {
		this.model = majorService.findMajorByMid(mid);
		model.setCollege(majorService.findCollege(cid));
		return "editMajor";
	}

	/**
	 * 更新专业
	 */
	public String updateMajor() {
		majorService.updateMajor(model);
		return "updateMajor";
	}

	/**
	 * 删除专业
	 */
	public String deleteMajor() {
		majorService.deleteMajor(mid);
		return "deleteMajor";
	}

}
