package com.xunmi.comments.actions;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.xunmi.comments.model.Page;
import com.xunmi.comments.model.Question;
import com.xunmi.comments.service.CourseService;

@Controller
@Scope("prototype")
public class QuestionAction extends BaseAction<Question> {

	private static final long serialVersionUID = 6618591863328185065L;
	
	private Integer cid;
	private Integer tid;
	private Integer pid;
	private Integer qid;
	public Integer getCid() {
		return cid;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	public Integer getTid() {
		return tid;
	}
	public void setTid(Integer tid) {
		this.tid = tid;
	}
	public Integer getPid() {
		return pid;
	}
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	public Integer getQid() {
		return qid;
	}
	public void setQid(Integer qid) {
		this.qid = qid;
	}
	
	@Resource
	private CourseService courseService;
	
	/**
	 * 到达选择题型页面
	 */
	public String toSelectQuestionType(){
		//保持关联关系
		this.model.setPage(courseService.getPage(pid));
		this.model.getPage().setTest(courseService.getTest(tid));
		this.model.getPage().getTest().setCourse(courseService.getCourse(cid));
		return "selectQuestionTypePage" ;
	}
	
	/**
	 * 根据选择的题型到达问题设计页面
	 */
	public String toDesignQuestion() {
		this.model.setPage(courseService.getPage(pid));
		this.model.getPage().setTest(courseService.getTest(tid));
		this.model.getPage().getTest().setCourse(courseService.getCourse(cid));
		return "" + model.getQuestionType();
	}
	
	/**
	 * 到达更新问题的页面
	 */
	public String toUpdateQuestion() {
		this.model = courseService.getQuestion(qid);
		this.model.setPage(courseService.getPage(pid));
		this.model.getPage().setTest(courseService.getTest(tid));
		this.model.getPage().getTest().setCourse(courseService.getCourse(cid));
		return "" + model.getQuestionType();
	}

	/**
	 * 保存/更新问题
	 */
	public String saveOrUpdateQuestion(){
		Page p = new Page();
		p.setId(pid);
		//维护关联关系
		//this.model = courseService.getQuestion(qid);
		model.setPage(p);
		courseService.saveOrUpdateQuestion(model);
		return "toDesignPageAction" ;
	}
	
	/**
	 * 删除问题
	 */
	public String doDeleteQuestion() {
		courseService.deleteQuestion(qid);
		return "toDesignPageAction";
	}
}
