package com.xunmi.comments.actions;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.xunmi.comments.model.Course;
import com.xunmi.comments.model.CourseParent;
import com.xunmi.comments.model.Term;
import com.xunmi.comments.model.User;
import com.xunmi.comments.service.CourseParentService;
import com.xunmi.comments.service.CourseService;
import com.xunmi.comments.service.TermService;
import com.xunmi.comments.aware.UserAware;

@Controller
@Scope("prototype")
public class CourseParentAction extends BaseAction<CourseParent> implements UserAware{

	private static final long serialVersionUID = 2601102641985044707L;

	//注入user
	private User user;

	public void setUser(User user) {
		this.user =user;
	}
	
	//注入courseService
	@Resource
	private CourseParentService courseParentService;
	//注入termService
	@Resource
	private TermService termService;
	//注入courseService
	@Resource
	private CourseService courseService;
	
	private Integer cpid;
	public Integer getCpid() {
		return cpid;
	}
	public void setCpid(Integer cpid) {
		this.cpid = cpid;
	}

	//课程集合
	private List<CourseParent> myCourseParents;
	//学期列表
	private List<Term> terms;
	public List<CourseParent> getMyCourseParents() {
		return myCourseParents;
	}
	public void setMyCourseParents(List<CourseParent> myCourseParents) {
		this.myCourseParents = myCourseParents;
	}
	public List<Term> getTerms() {
		return terms;
	}
	public void setTerms(List<Term> terms) {
		this.terms = terms;
	}
	
	/**
	 * 到达我的课程列表页面
	 */
	public String myCourseParents() {
		System.out.println(user.getId());
		this.myCourseParents = courseParentService.findMycourseparents(user);
		return "myCourseParentList";
	}
	
	/**
	 * 到达添加课程的页面
	 */
	public String toAddCourseParentPage() {
		//this.terms = termService.findAllterms();
		return "addCourseParentPage" ;
	}
	
	/**
	 * 新建课程
	 */
	public String newCourseParent() {
		courseParentService.newCourseParent(this.model,user);
		return "myCourseParentsAction" ;
	}
	
	/**
	 * 新建课程表单提交验证
	 */
	public void validateNewCourseParent() {
		if( this.model.getCode() == null || this.model.getCode().equals("") ) {
			addActionError("表单填写错误！");
		}
	}
	
	/**
	 * 删除课程
	 */
	public String doDeleteCourseParent() {
		List<Course> lists = courseService.findBycpidCourses(cpid);
		for(Course course : lists) {
			course.setClassrooms(null);
			courseService.updateCourse(course);
		}
		courseService.deleteCourseParent(cpid);
		return "myCourseParentsAction";
	}
	
	/**
	 * 到达更新课程的页面
	 */
	public String toUpdateCourseParentPage() {
		this.model = courseParentService.getCourseParent(cpid);
		return "updateCourseParentPage";
	}
	
	/**
	 * 更新课程
	 */
	public String doUpdateCourseParent() {
		//设置关联关系
		model.setUser(user);
		courseParentService.updateCourseParent(model);
		return "myCourseParentsAction";
	}
	
}
