package com.xunmi.comments.actions;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.xunmi.comments.model.Classroom;
import com.xunmi.comments.model.College;
import com.xunmi.comments.service.ClassroomService;
import com.xunmi.comments.util.StringUtil;

/**
 * 班级action
 */
@Controller
@Scope("prototype")
public class ClassroomAction extends BaseAction<Classroom> {

	private static final long serialVersionUID = 6405779038180006651L;

	@Resource
	private ClassroomService classroomService;

	// 班级集合
	private List<Classroom> classrooms;

	// 学院集合
	private List<College> colleges;

	// 专业id
	private Integer mid;

	// 班级id
	private Integer cid;

	// 学期
	private String semester;

	// 开班元年
	private int startTime;

	public int getStartTime() {
		return startTime;
	}

	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public Integer getMid() {
		return mid;
	}

	public void setMid(Integer mid) {
		this.mid = mid;
	}

	public List<College> getColleges() {
		return colleges;
	}

	public void setColleges(List<College> colleges) {
		this.colleges = colleges;
	}

	public List<Classroom> getClassrooms() {
		return classrooms;
	}

	public void setClassrooms(List<Classroom> classrooms) {
		this.classrooms = classrooms;
	}

	/**
	 * 班级列表
	 */
	public String classroomManage() {
		this.colleges = classroomService.findAllCollege();
		return "classroomManage";
	}

	/**
	 * 按照专业id,学期查询对应的课程
	 */
	public String findClassroomByMid() {
		this.model.setMajor(classroomService.findMajorByCid(mid));
		this.colleges = classroomService.findAllCollege();
		this.classrooms = classroomService.findClassroomByMidAndSemester(mid,
				StringUtil.cutOutString(semester, 0, 4));
		return "findClassroomByMid";
	}

	/**
	 * 到达添加班级页面
	 */
	public String addClassroom() {
		this.model.setMajor(classroomService.findMajorByCid(mid));
		return "addClassroom";
	}

	/**
	 * 处理添加班级
	 */
	public String doAddClassroom() {
		// 依据专业id查询学制年数
		int majorYears = classroomService.findMajorYearsByMid(mid);
		// 获得班级表中createtime字符串
		String createTime = StringUtil.timeGroupString(majorYears, startTime);
		System.out.println(createTime);
		model.setCreateTime(createTime);
		this.classroomService.addClassroom(model, mid);
		return "doAddClassroom";
	}

	/**
	 * 按照班级id查询班级
	 */
	public String editClassroom() {
		this.model = classroomService.findClassroomByCid(cid);
		model.setMajor(classroomService.findMajorByCid(mid));
		startTime = Integer.parseInt(StringUtil.cutOutString(
				model.getCreateTime(), 0, 4));
		return "editClassroom";
	}

	/**
	 * 更新班级
	 */
	public String updateClassroom() {
		classroomService.updateClassroom(model);
		return "updateClassroom";
	}

	/**
	 * 按照id删除班级
	 */
	public String deleteClassroom() {
		classroomService.deleteClassroom(cid);
		return "deleteClassroom";
	}

}
