package com.xunmi.comments.actions;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.xunmi.comments.model.College;
import com.xunmi.comments.model.User;
import com.xunmi.comments.service.UserService;
import com.xunmi.comments.util.DataUtil;

/**
 * 用户管理Action
 */
@Controller
@Scope("prototype")
public class UserAction extends BaseAction<User> {

	private static final long serialVersionUID = -2613763099301903732L;

	@Resource
	private UserService userService;

	// 学院集合
	private List<College> colleges;

	// 用户集合
	private List<User> users;

	// 未分配教师
	private List<User> undistributedTeachers;

	public List<User> getUndistributedTeachers() {
		return undistributedTeachers;
	}

	public void setUndistributedTeachers(List<User> undistributedTeachers) {
		this.undistributedTeachers = undistributedTeachers;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	// 班级Id
	private Integer classId;

	// 用户id
	private Integer uid;

	// 学院id
	private Integer cid;

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public Integer getClassId() {
		return classId;
	}

	public void setClassId(Integer classId) {
		this.classId = classId;
	}

	public List<College> getColleges() {
		return colleges;
	}

	public void setColleges(List<College> colleges) {
		this.colleges = colleges;
	}

	/**
	 * 到达用户管理界面
	 */
	public String loadUsers() {
		this.colleges = userService.findAllColleges();
		return "loadUsersPage";
	}

	/**
	 * 按照用户类型和班级id查询用户
	 */
	public String searchPerson() {
		model.setClassroom(userService.findClassroomByCid(classId));
		this.colleges = userService.findAllColleges();
		this.users = userService.findPersonByRoleIdAndClassId(model.getType(),
				classId);
		return "searchPerson";
	}

	/**
	 * 添加用户
	 */
	public String addUser() {
		model.setClassroom(userService.findClassroomByCid(classId));
		return "addUser";
	}

	/**
	 * 处理添加，依据type和classId添加用户
	 */
	public String doAddUser() {
		model.setPassword(DataUtil.md5(model.getPassword()));
		userService.addUserByTypeAndClassId(model);
		return "doAddUser";
	}

	/**
	 * 更新用户
	 */
	public String editUser() {
		this.model = userService.findUserByUid(uid);
		model.setClassroom(userService.findClassroomByCid(model.getClassroom()
				.getId()));
		return "editUser";
	}

	/**
	 * 更新用户
	 */
	public String updateUser() {
		classId = model.getClassroom().getId();
		userService.updateUser(model);
		return "updateUser";
	}

	/**
	 * 删除用户
	 */
	public String deleteUser() {
		this.model = userService.findUserByUid(uid);
		classId = model.getClassroom().getId();
		userService.deleteUser(uid);
		return "deleteUser";
	}

	/**
	 * 教师管理
	 */
	public String loadTeachers() {
		this.undistributedTeachers = userService.findAllUndisTeachers();
		this.users = userService.findAllTeacher();
		return "loadTeachers";
	}

	/**
	 * 分配教师到学院
	 */
	public String distributeTeacher() {
		this.colleges = userService.findAllColleges();
		this.model = userService.findUserByUid(uid);
		return "distributeTeacher";
	}

	public String doDistributeTeacher() {
		this.model = userService.findUserByUid(uid);
		College c = new College();
		c.setId(cid);
		model.setCollege(c);
		userService.distributeTeacher(model);
		return "doDistributeTeacher";
	}

}
