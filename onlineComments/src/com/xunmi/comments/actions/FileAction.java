package com.xunmi.comments.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.struts2.ServletActionContext;
import org.aspectj.util.FileUtil;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;
import com.xunmi.comments.model.Classroom;
import com.xunmi.comments.model.User;
import com.xunmi.comments.service.UserService;
import com.xunmi.comments.util.DataUtil;

/**
 * 文件上传/下载action
 */
@Controller
@Scope("prototype")
public class FileAction extends ActionSupport {

	private static final long serialVersionUID = 6434361103357463013L;

	// 接收上传的文件
	private File uploadFile;

	// 获得上传文件名
	private String uploadFileFileName;

	@Resource
	private UserService userService;

	// 下载文件名称
	private String fileName;

	private List<User> users;

	private Integer classId;
	private char type;

	public Integer getClassId() {
		return classId;
	}

	public void setClassId(Integer classId) {
		this.classId = classId;
	}

	public char getType() {
		return type;
	}

	public void setType(char type) {
		this.type = type;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getUploadFileFileName() {
		return uploadFileFileName;
	}

	public void setUploadFileFileName(String uploadFileFileName) {
		this.uploadFileFileName = uploadFileFileName;
	}

	public File getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(File uploadFile) {
		this.uploadFile = uploadFile;
	}

	/**
	 * 到达文件上传页面
	 */
	public String toUploadFilePage() {
		return "uploadFilePage";
	}

	/**
	 * 处理上传文件
	 */
	public String doUploadFile() {
		String directory = "/upload";
		String targetDirectory = ServletActionContext.getServletContext()
				.getRealPath(directory);
		// 生成上传的文件对象
		File target = new File(targetDirectory, uploadFileFileName);
		// 如果文件已存在，则删除源文件
		if (target.exists()) {
			target.delete();
		}
		// 复制file对象，实现上传
		try {
			FileUtil.copyFile(uploadFile, target);
			// System.out.println("文件上传成功了!");
			// System.out.println(uploadFileFileName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// loadUserInfo(uploadFileFileName);
		return SUCCESS;
	}

	/**
	 * 将teacher信息存到数据库
	 */
	public void loadTeacherInfo(String uploadFileFileName) {
		String directory = "/upload";
		String targetDirectory = ServletActionContext.getServletContext()
				.getRealPath(directory);
		File target = new File(targetDirectory, uploadFileFileName);

		List<User> userList = new ArrayList<User>();
		try {
			FileInputStream fi = new FileInputStream(target);
			Workbook wb = new HSSFWorkbook(fi);
			Sheet sheet = wb.getSheetAt(0);

			int rowNum = sheet.getLastRowNum() + 1;
			for (int i = 1; i < rowNum; i++) {
				User user = new User();
				Row row = sheet.getRow(i);
				int cellNum = row.getLastCellNum();
				for (int j = 0; j < cellNum; j++) {
					Cell cell = row.getCell(j);
					String cellValue = null;
					// 判断excel单元格的内容格式，并对其进行转换，以便插入数据库
					switch (cell.getCellType()) {
					case 0:
						cellValue = String.valueOf((int) cell
								.getNumericCellValue());
						break;
					case 1:
						cellValue = cell.getStringCellValue();
						break;
					// case 2:
					// cellValue = String.valueOf(cell.getDateCellValue());
					// break;
					case 3:
						cellValue = "";
						break;
					case 4:
						cellValue = String.valueOf(cell.getBooleanCellValue());
						break;
					case 5:
						cellValue = String.valueOf(cell.getErrorCellValue());
						break;
					}

					// 通过列数判断应对应插入的字段
					switch (j) {
					case 0:
						user.setUsername(cellValue);
						break;
					case 1:
						user.setPassword(DataUtil.md5(cellValue));
						break;
					case 2:
						user.setSex(cellValue.charAt(0));
						break;
					case 3:
						user.setNickname(cellValue);
						break;
					}
					user.setRegdate(new Date());
					user.setType('1');
				}
				userList.add(user);
			}
			// 保存到数据库中
			userService.batchEntityByFile(userList);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 将student信息存到数据库
	 */
	public void loadStudentInfo(String uploadFileFileName) {
		String directory = "/upload";
		String targetDirectory = ServletActionContext.getServletContext()
				.getRealPath(directory);
		File target = new File(targetDirectory, uploadFileFileName);

		List<User> userList = new ArrayList<User>();
		try {
			FileInputStream fi = new FileInputStream(target);
			Workbook wb = new HSSFWorkbook(fi);
			Sheet sheet = wb.getSheetAt(0);

			int rowNum = sheet.getLastRowNum() + 1;
			for (int i = 1; i < rowNum; i++) {
				User user = new User();
				Row row = sheet.getRow(i);
				int cellNum = row.getLastCellNum();
				for (int j = 0; j < cellNum; j++) {
					Cell cell = row.getCell(j);
					String cellValue = null;
					// 判断excel单元格的内容格式，并对其进行转换，以便插入数据库
					switch (cell.getCellType()) {
					case 0:
						cellValue = String.valueOf((int) cell
								.getNumericCellValue());
						break;
					case 1:
						cellValue = cell.getStringCellValue();
						break;
					// case 2:
					// cellValue = String.valueOf(cell.getDateCellValue());
					// break;
					case 3:
						cellValue = "";
						break;
					case 4:
						cellValue = String.valueOf(cell.getBooleanCellValue());
						break;
					case 5:
						cellValue = String.valueOf(cell.getErrorCellValue());
						break;
					}

					// 通过列数判断应对应插入的字段
					switch (j) {
					case 0:
						user.setUsername(cellValue);
						break;
					case 1:
						user.setPassword(DataUtil.md5(cellValue));
						break;
					case 2:
						user.setSex(cellValue.charAt(0));
						break;
					case 3:
						user.setNickname(cellValue);
						break;
					}
					user.setRegdate(new Date());
					user.setType('2');
					Classroom c = new Classroom();
					c.setId(classId);
					user.setClassroom(c);
				}
				userList.add(user);
			}
			// 保存到数据库中
			userService.batchEntityByFile(userList);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 文件下载
	 */
	public String fileDownload() {
		return "fileDownloadSuccess";
	}

	// 由struts.xml配置文件<param name="inputName">inputStream</param>自行调用
	public InputStream getInputStream() {
		return ServletActionContext.getServletContext().getResourceAsStream(
				"/upload/" + fileName);
	}

	/**
	 * 批量上传教师
	 */
	public String batchUploadTeachers() {
		this.doUploadFile();
		this.loadTeacherInfo(uploadFileFileName);
		this.users = userService.findAllTeacher();
		return "batchUploadTeachers";
	}

	public String batchUploadStudents() {
		this.doUploadFile();
		this.loadStudentInfo(uploadFileFileName);
		return "batchUploadStudents";
	}

	/**
	 * 下载用户模板
	 */
	public String downloadUserTemplate() {
		// 第一步，创建一个webbook，对应一个Excel文件
		HSSFWorkbook wb = new HSSFWorkbook();
		// 第二步，在webbook中添加一个sheet，对应excel文件中的sheet
		HSSFSheet sheet = wb.createSheet("用户批量添加模板");
		// 第三步，在sheet添加表头第0行，注意老版本poi对excel的行数列有限制short
		HSSFRow row = sheet.createRow(0);
		// 第四步，创建单元格，并设置表头，设置表头居中
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 创建居中格式

		HSSFCell cell = row.createCell(0);
		cell.setCellValue("用户名");
		cell.setCellStyle(style);
		cell = row.createCell(1);
		cell.setCellValue("密码");
		cell.setCellStyle(style);
		cell = row.createCell(2);
		cell.setCellValue("性别");
		cell.setCellStyle(style);
		cell = row.createCell(3);
		cell.setCellValue("昵称");
		cell.setCellStyle(style);

		String rootPath = ServletActionContext.getServletContext().getRealPath(
				"/upload");
		try {
			FileOutputStream fopt = new FileOutputStream(rootPath
					+ "/UserTemplate.xls");
			wb.write(fopt);
			fopt.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "fileDownloadSuccess";
	}

}
