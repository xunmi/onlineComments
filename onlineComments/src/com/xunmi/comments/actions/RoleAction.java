package com.xunmi.comments.actions;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.xunmi.comments.model.security.Right;
import com.xunmi.comments.model.security.Role;
import com.xunmi.comments.service.RightService;
import com.xunmi.comments.service.RoleService;

/**
 * roleAction
 */
@Controller
@Scope("prototype")
public class RoleAction extends BaseAction<Role> {

	private static final long serialVersionUID = 4594094748161082383L;

	@Resource
	private RightService rightService;

	@Resource
	private RoleService roleService;

	private List<Role> allRoles;

	private List<Right> noOwnRights;

	private Integer roleId;

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	// 角色拥有的权限id数组
	private Integer[] ownRightIds;

	public Integer[] getOwnRightIds() {
		return ownRightIds;
	}

	public void setOwnRightIds(Integer[] ownRightIds) {
		this.ownRightIds = ownRightIds;
	}

	public List<Role> getAllRoles() {
		return allRoles;
	}

	public void setAllRoles(List<Role> allRoles) {
		this.allRoles = allRoles;
	}

	public List<Right> getNoOwnRights() {
		return noOwnRights;
	}

	public void setNoOwnRights(List<Right> noOwnRights) {
		this.noOwnRights = noOwnRights;
	}

	/**
	 * 查询所有角色
	 */
	public String findAllRoles() {
		this.allRoles = roleService.findAllEntities();
		return "roleListPage";
	}

	/**
	 * 添加角色
	 */
	public String toAddRolePage() {
		this.noOwnRights = rightService.findAllEntities();
		return "addRolePage";
	}

	/**
	 * 保存或者更新角色
	 */
	public String saveOrUpdateRole() {
		roleService.saveOrUpdateRole(model, ownRightIds);
		return "findAllRolesAction";
	}

	/**
	 * 编辑角色
	 */
	public String editRole() {
		this.model = roleService.getEntity(roleId);
		this.noOwnRights = rightService.findRightsNotInRange(model.getRights());
		return "editRolePage";
	}

	public String deleteRole() {
		Role r = new Role();
		r.setId(roleId);
		roleService.deleteEntity(r);
		return "findAllRolesAction";
	}
}
