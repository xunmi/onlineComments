package com.xunmi.comments.actions;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.xunmi.comments.model.College;
import com.xunmi.comments.model.CourseParent;
import com.xunmi.comments.model.Term;
import com.xunmi.comments.service.CollegeService;
import com.xunmi.comments.service.CourseParentService;
import com.xunmi.comments.service.TermService;

@Controller
@Scope("prototype")
public class LinkageAction extends BaseAction<College>{

	/*** 
	 * 可能是因为拦截器的问题必须继承 BaseAction
	 */
	private static final long serialVersionUID = 7262124799410249328L;
	
	//注入collegeService
	@Resource
	private CollegeService collegeService;
	//注入courseParentService
	@Resource
	private CourseParentService courseParentService;
	//注入termService
	@Resource
	private TermService termService;
	
	//学院列表
	private List<College> colleges;
	//课程列表
	private List<CourseParent> courseparents;
	//学期列表
	private List<Term> terms;
	public List<College> getColleges() {
		return colleges;
	}
	public void setColleges(List<College> colleges) {
		this.colleges = colleges;
	}
	public List<CourseParent> getCourseparents() {
		return courseparents;
	}
	public void setCourseparents(List<CourseParent> courseparents) {
		this.courseparents = courseparents;
	}
	public List<Term> getTerms() {
		return terms;
	}
	public void setTerms(List<Term> terms) {
		this.terms = terms;
	}
	/**
	 * 到达课程分配的页面
	 */
	public String toAllotCoursePage() {
		this.colleges = collegeService.findAllcolleges();
		this.courseparents = courseParentService.findAllcourseparents();
		this.terms = termService.findAllterms();
		return "allotCoursePage";
	}

}
