package com.xunmi.comments.actions;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.xunmi.comments.model.security.Right;
import com.xunmi.comments.service.RightService;

/**
 * rightAction
 */
@Controller
@Scope("prototype")
public class RightAction extends BaseAction<Right> {

	private static final long serialVersionUID = 7705359303638793228L;

	@Resource
	private RightService rightService;

	private List<Right> allRights;

	private Integer rightId;

	public Integer getRightId() {
		return rightId;
	}

	public void setRightId(Integer rightId) {
		this.rightId = rightId;
	}

	public List<Right> getAllRights() {
		return allRights;
	}

	public void setAllRights(List<Right> allRights) {
		this.allRights = allRights;
	}

	/**
	 * 查询所有权限
	 */
	public String findAllRights() {
		this.allRights = rightService.findAllEntities();
		return "rightListPage";
	}

	/**
	 * 添加权限
	 */
	public String toAddRightPage() {
		return "addRightPage";
	}

	/**
	 * 插入或更新权限
	 */
	public String saveOrUpdateRight() {
		System.out.println(model.getRightPos());
		System.out.println(model.getRightUrl());
		rightService.saveOrUpdateRight(model);
		return "findAllRightsAction";
	}

	/**
	 * 更新权限
	 */
	public String editRight() {
		this.model = rightService.getEntity(rightId);
		System.out.println(model.getRightName());
		return "editRightPage";
	}

	/**
	 * 删除权限
	 */
	public String deleteRight() {
		Right r = new Right();
		r.setId(rightId);
		rightService.deleteEntity(r);
		return "findAllRightsAction";
	}

	/**
	 * 批量更新权限
	 */
	public String batchUpdateRights() {
		rightService.batchUpdateRights(allRights);
		return "findAllRightsAction";
	}

}
