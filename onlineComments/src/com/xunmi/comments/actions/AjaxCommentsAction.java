package com.xunmi.comments.actions;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;

@Controller
@Scope("prototype")
public class AjaxCommentsAction extends ActionSupport implements SessionAware {

	private static final long serialVersionUID = 92733615566029863L;

	private String result;

	private String info;

	private Map<String, Object> sessionMap;

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * 添加一个批注
	 */
	public String addComment() {
		// --------------
		// 分割字符串：字符串为******<answer_id></answer_id>*******<solutionComments></solutionComments>******
		// --------------
		String a[] = info.split("<solutionComments></solutionComments>");

		// 依据题目数量确定二维数组长度
		int keyNum = a.length;
		String keyValue[][] = new String[keyNum][2];

		// 将字符串切割后存到二维数组keyValue[key][value]
		for (int i = 0; i < a.length; i++) {
			String b[] = a[i].split("<answer_id></answer_id>");
			for (int j = 0; j < b.length; j++) {
				keyValue[i][j] = b[j];
				System.out.println(keyValue[i][j]);
			}
		}

		// -------------------------------------
		// 更新session数据
		for (int i = 0; i < keyValue.length; i++) {
			System.out.println(keyValue[i][0]);
			System.out.println(sessionMap.get(keyValue[i][0]));
			System.out.println("----------之前的session值-----------------------");
			sessionMap.put(keyValue[i][0], keyValue[i][1]);
			System.out.println("----------之后恩恩sesiioon值-----------------------");
			System.out.println(sessionMap.get(keyValue[i][0]));
		}
		return SUCCESS;
	}

	/**
	 * 获取session
	 */
	public void setSession(Map<String, Object> arg0) {
		this.sessionMap = arg0;
	}

}
