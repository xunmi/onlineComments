package com.xunmi.comments.actions;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;
import com.xunmi.comments.model.Page;
import com.xunmi.comments.model.Test;
import com.xunmi.comments.service.CourseService;

@Controller
@Scope("prototype")
public class AjaxUpdateNameAction extends ActionSupport {

	private static final long serialVersionUID = -7607719515862198592L;

	private Integer tid;
	private Integer pid;
	private String testTitle;
	private String pageName;
	private String result;

	public Integer getTid() {
		return tid;
	}

	public void setTid(Integer tid) {
		this.tid = tid;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getTestTitle() {
		return testTitle;
	}

	public void setTestTitle(String testTitle) {
		this.testTitle = testTitle;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	// 注入courseService
	@Resource
	private CourseService courseService;

	/**
	 * 修改实验名称
	 */
	public String updateTestName() throws Exception{
		testTitle = new String(testTitle.getBytes("ISO8859-1"),"utf-8");
		Test t = courseService.getTest(tid);
		t.setTitle(testTitle);
		courseService.updateTest(t);

		Map<String, String> map = new HashMap<String, String>();
		JSONObject obj = null;
		map.put("result", testTitle);
		obj = JSONObject.fromObject(map);
		result = obj.toString();
		return SUCCESS;
	}

	/**
	 * 修改页面名称
	 */
	public String updatePageName() throws Exception{
		pageName = new String(pageName.getBytes("ISO8859-1"),"utf-8");
		Page p = courseService.getPage(pid);
		p.setName(pageName);
		courseService.updatePage(p);

		Map<String, String> map = new HashMap<String, String>();
		JSONObject obj = null;
		map.put("result", pageName);
		obj = JSONObject.fromObject(map);
		result = obj.toString();
		return SUCCESS;
	}
}
