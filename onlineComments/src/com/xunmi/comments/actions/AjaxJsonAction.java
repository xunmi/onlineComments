package com.xunmi.comments.actions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;
import com.xunmi.comments.model.Classroom;
import com.xunmi.comments.model.Major;
import com.xunmi.comments.service.ClassroomService;
import com.xunmi.comments.service.MajorService;
import com.xunmi.comments.util.StringUtil;

/**
 * ajax+json的action
 */
@Controller
@Scope("prototype")
public class AjaxJsonAction extends ActionSupport {

	private static final long serialVersionUID = 548800200559389031L;

	@Resource
	private MajorService majorService;

	@Resource
	private ClassroomService classroomService;

	// 返回值
	private String result;

	// 学院id
	private Integer cid;

	// 专业id
	private Integer mid;

	// 学期
	private String semester;

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public Integer getMid() {
		return mid;
	}

	public void setMid(Integer mid) {
		this.mid = mid;
	}

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * 测试用例
	 */
	public String test() {

		Map<String, String> map = new HashMap<String, String>();
		JSONObject obj = null;

		map.put("test", "sd");
		obj = JSONObject.fromObject(map);
		result = obj.toString();

		return SUCCESS;
	}

	/**
	 * 按照学院id查询所有专业
	 */
	public String findAllMajorByCid() {
		List<Major> majors = majorService.findAllMajorsByCid(cid);

		Map<String, Object> map = new HashMap<String, Object>();
		JSONObject obj = null;

		Major m = null;
		for (int i = 0; i < majors.size(); i++) {
			m = majors.get(i);
			map.put(m.getId().toString(), m.getName());
		}

		obj = JSONObject.fromObject(map);
		result = obj.toString();

		return SUCCESS;
	}

	/**
	 * 按照专业id和学期查询所有班级
	 */
	public String findAllClassroomByMidAndSemester() {
		List<Classroom> classrooms = classroomService
				.findClassroomByMidAndSemester(mid,
						StringUtil.cutOutString(semester, 0, 4));

		Map<String, Object> map = new HashMap<String, Object>();
		JSONObject obj = null;

		// 重组数据，json将hiberbnate传回的List转换为json数组时易发生错误（尤其在表有关联时），建议自己重组数据信息
		Classroom c = null;
		for (int i = 0; i < classrooms.size(); i++) {
			c = classrooms.get(i);
			map.put(c.getId().toString(), c.getName());
		}

		obj = JSONObject.fromObject(map);
		result = obj.toString();
		return SUCCESS;
	}
}
