package com.xunmi.comments.actions;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.xunmi.comments.model.User;
import com.xunmi.comments.model.security.Role;
import com.xunmi.comments.service.RoleService;
import com.xunmi.comments.service.UserService;

/**
 * 用户授权action
 */
@Controller
@Scope("prototype")
public class UserAuthorizeAction extends BaseAction<User> {

	private static final long serialVersionUID = 6099491782424649147L;

	@Resource
	private UserService userService;

	@Resource
	private RoleService roleService;

	private List<User> allUsers;

	private Integer userId;

	// 未授权的角色
	private List<Role> noOwnRoles;

	private Integer[] ownRoleIds;

	public Integer[] getOwnRoleIds() {
		return ownRoleIds;
	}

	public void setOwnRoleIds(Integer[] ownRoleIds) {
		this.ownRoleIds = ownRoleIds;
	}

	public List<Role> getNoOwnRoles() {
		return noOwnRoles;
	}

	public void setNoOwnRoles(List<Role> noOwnRoles) {
		this.noOwnRoles = noOwnRoles;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public List<User> getAllUsers() {
		return allUsers;
	}

	public void setAllUsers(List<User> allUsers) {
		this.allUsers = allUsers;
	}

	/**
	 * 查询所有用户
	 */
	public String findAllUsers() {
		this.allUsers = userService.findAllusers();
		return "userAuthorizeListPage";
	}

	/**
	 * 编辑授权
	 */
	public String editAuthorize() {
		this.model = userService.findUserByUid(userId);
		this.noOwnRoles = roleService.findRolesNotInRange(model.getRoles());
		return "userAuthorizePage";
	}

	/**
	 * 更新授权
	 */
	public String updateAuthorize() {
		userService.updateAuthorize(model, ownRoleIds);
		return "findAllUsersAction";
	}

	public String clearAuthorize() {
		userService.clearAuthorize(userId);
		return "findAllUsersAction";
	}
}
