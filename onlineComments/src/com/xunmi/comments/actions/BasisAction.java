package com.xunmi.comments.actions;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 基础配置action
 */
@Controller
@Scope("prototype")
public class BasisAction extends ActionSupport {

	private static final long serialVersionUID = -7763397598591317598L;

	/**
	 * 访问CollegeAction中的collegeManage方法, 作为首页
	 */
	public String collegeManage() {
		return "collegeManage";
	}

}
