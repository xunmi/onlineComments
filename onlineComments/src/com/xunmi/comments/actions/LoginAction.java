package com.xunmi.comments.actions;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.xunmi.comments.model.User;
import com.xunmi.comments.service.RightService;
import com.xunmi.comments.service.UserService;
import com.xunmi.comments.util.DataUtil;

/**
 * 登录action
 */
@Controller
@Scope("prototype")
public class LoginAction extends BaseAction<User> implements SessionAware {

	private static final long serialVersionUID = -2677419278102299278L;
	// 接收session
	private Map<String, Object> sessionMap;

	@Resource
	private UserService userService;

	@Resource
	private RightService rightService;

	/**
	 * 到达登录页面
	 */
	public String toLoginPage() {
		return "loginPage";
	}

	/**
	 * 登录处理
	 */
	public String doLogin() {
		if (model.getType() == '1') {
			return "myCoursesAction";
		} else if (model.getType() == '2') {
			return "studentCoursesAction";
		} else {
			return "myCourseParentsAction";
		}
	}

	/**
	 * 校验登录信息
	 */
	public void validate() {
		User user = userService.validateLoginInfo(model.getUsername(),
				DataUtil.md5(model.getPassword()));
		if (user == null) {
			addActionError("用户名或者密码错误！");
		} else {
			// 初始化权限总和的数组
			int maxPos = rightService.getMaxRightPos();
			user.setRightSum(new long[maxPos + 1]);

			// 计算用户的权限总和
			user.calculateRightRightSum();
			// 存入session
			this.model = user;
			sessionMap.put("user", user);
		}
	}

	// 接收session
	public void setSession(Map<String, Object> session) {
		this.sessionMap = session;
	}
}
