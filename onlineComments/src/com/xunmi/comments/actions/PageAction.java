package com.xunmi.comments.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.struts2.interceptor.ParameterAware;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.xunmi.comments.model.Answer;
import com.xunmi.comments.model.Page;
import com.xunmi.comments.model.Question;
import com.xunmi.comments.model.Test;
import com.xunmi.comments.model.User;
import com.xunmi.comments.service.CourseService;
import com.xunmi.comments.service.PageService;
import com.xunmi.comments.service.QuestionService;
import com.xunmi.comments.aware.UserAware;
import com.xunmi.comments.util.HtmlUtil;

@Controller
@Scope("prototype")
public class PageAction extends BaseAction<Page> implements SessionAware,
		ParameterAware, UserAware {

	private static final long serialVersionUID = -5676573889619616339L;

	private User user;

	// 注入User对象
	public void setUser(User user) {
		this.user = user;
	}

	// 当前调查的key
	private static final String CURRENT_TEST = "current_test";
	// 所有参数的map
	private static final String ALL_PARAMS_MAP = "all_params_map";

	private Integer cid;

	private Integer tid;

	private Integer pid;

	// 获取学生的id
	private Integer uid;

	// 学生答题，当前页面id
	private Integer currPid;

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public Integer getTid() {
		return tid;
	}

	public void setTid(Integer tid) {
		this.tid = tid;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public Integer getCurrPid() {
		return currPid;
	}

	public void setCurrPid(Integer currPid) {
		this.currPid = currPid;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	// 答案列表
	private List<Answer> myAnswers;

	public List<Answer> getMyAnswers() {
		return myAnswers;
	}

	public void setMyAnswers(List<Answer> myAnswers) {
		this.myAnswers = myAnswers;
	}

	// 接受sessionMap
	private Map<String, Object> sessionMap;

	// 接受所有参数的map
	private Map<String, String[]> paramsMap;

	// 注入courseService
	@Resource
	private CourseService courseService;
	// 注入pageService
	@Resource
	private PageService pageService;
	// 注入questionService
	@Resource
	private QuestionService questionService;

	/**
	 * 到达添加page的页面
	 */
	public String toAddPage() {
		this.model.setTest(courseService.getTest(tid));
		this.model.getTest().setCourse(courseService.getCourse(cid));
		return "addPagePage";
	}

	/**
	 * 编辑/设计页面
	 */
	public String toDesignPage() {
		// 维护关联关系
		this.model = courseService.getPageWithChridren(pid);
		this.model.setTest(courseService.getTest(tid));
		this.model.getTest().setCourse(courseService.getCourse(cid));
		Set<Question> q = this.model.getQuestions();
		for (Question question : q) {
			question.setTitle(HtmlUtil.delHTMLTag(question.getTitle()));
		}
		this.model.setQuestions(q);
		return "designPagePage";
	}

	/**
	 * 添加页面
	 */
	public String doAddPage() {
		// 维护关联关系
		Test t = new Test();
		t.setId(tid);
		model.setTest(t);
		courseService.saveOrUpdatePage(model);
		return "designTestAction";
	}

	/**
	 * 删除页面，同时删除页面下的问题
	 */
	public String doDeletePage() {
		courseService.deletePage(pid);
		return "designTestAction";
	}

	/*---------------学生完成实验报告------------*/

	/**
	 * 到达学生答题页面（首页）
	 */
	public String toStudentPage() {
		this.currPid = 0;
		List<Page> pages = null;
		pages = pageService.findPagesWithChildren(tid);
		this.model = pages.get(0);
		this.model.setTest(courseService.getTest(tid));
		this.model.getTest().setCourse(courseService.getCourse(cid));

		// 存放test--> session
		sessionMap.put(CURRENT_TEST, this.model.getTest());
		// 将存放所有参数的大map -->session中(用户保存答案和回显).
		sessionMap.put(ALL_PARAMS_MAP,
				new HashMap<Integer, Map<String, String[]>>());

		return "studentPage";
	}

	/**
	 * 获得提交按钮的名称
	 */
	private String getSubmitName() {
		for (String key : paramsMap.keySet()) {
			if (key.startsWith("submit_")) {
				return key;
			}
		}
		return null;
	}

	/**
	 * 获取session存放所有参数的map
	 */
	@SuppressWarnings("unchecked")
	private Map<Integer, Map<String, String[]>> getAllParamsMap() {
		return (Map<Integer, Map<String, String[]>>) sessionMap
				.get(ALL_PARAMS_MAP);
	}

	/**
	 * 合并参数到session中
	 */
	private void mergeParamsIntoSession() {
		Map<Integer, Map<String, String[]>> allParamsMap = getAllParamsMap();
		allParamsMap.put(currPid, paramsMap);
	}

	/**
	 * 页面处理（学生）
	 */
	public String doStudentPage() {
		String submitName = getSubmitName();
		Page targPage = pageService.getPage(currPid);
		targPage.setTest(courseService.getTest(tid));
		// targPage.getTest().setCourse(courseService.getCourse(cid));
		// 上一页
		if (submitName.endsWith("pre")) {
			// System.out.println("-------pre---------");
			mergeParamsIntoSession();
			this.model = pageService.getPrePage(targPage);
			this.model.setTest(courseService.getTest(tid));
			this.model.getTest().setCourse(courseService.getCourse(cid));
		}
		// 下一页
		else if (submitName.endsWith("next")) {
			// System.out.println("-------next---------");
			mergeParamsIntoSession();
			this.model = pageService.getNextPage(targPage);
			this.model.setTest(courseService.getTest(tid));
			this.model.getTest().setCourse(courseService.getCourse(cid));
		}
		// 完成
		else if (submitName.endsWith("done")) {
			// System.out.println("-------done---------");
			mergeParamsIntoSession();
			// 中间表关系
			Test t = courseService.getTest(tid);
			t.getUsers().add(user);
			courseService.saveOrUpdateTest(t);
			// 答案入库
			pageService.saveAnswers(processAnswers());
			
			// 清除session
			clearSessionData();
			return "studentTestsAction";
		} else if (submitName.endsWith("exit")) {
			clearSessionData();
			return "studentTestsAction";
		}
		return "studentPage";
	}

	/**
	 * 处理答案
	 */
	private List<Answer> processAnswers() {
		// 所有答案的集合
		List<Answer> answers = new ArrayList<Answer>();
		Answer a = null;
		String key = null;
		String[] value = null;
		Map<Integer, Map<String, String[]>> allMap = getAllParamsMap();
		for (Map<String, String[]> map : allMap.values()) {
			for (Entry<String, String[]> entry : map.entrySet()) {
				key = entry.getKey();
				value = entry.getValue();
				// 处理所有a开头的参数
				if (key.startsWith("a")) {
					a = new Answer();
					a.setQuestion(questionService.getQuestion(getQid(key)));
					a.setSolution(value[0]);
					a.setUser(user);
					a.setT_id(tid);
					answers.add(a);
				}
			}
		}
		return answers;
	}

	/**
	 * 提取问题id
	 */
	private Integer getQid(String key) {
		return Integer.parseInt(key.substring(1));
	}

	/**
	 * 清除session数据
	 */
	private void clearSessionData() {
		// 移除当前答案
		List<Answer> answers = courseService.getUserAnswers(uid);
		for (int j = 0; j < answers.size(); j++) {
			sessionMap.remove(answers.get(j).getId());
		}

		// 移除页面配置session
		sessionMap.remove(CURRENT_TEST);
		sessionMap.remove(ALL_PARAMS_MAP);
	}

	/**
	 * 注入sessionMap
	 */
	public void setSession(Map<String, Object> session) {
		this.sessionMap = session;
	}

	/**
	 * 注入提交的所有参数的map
	 */
	public void setParameters(Map<String, String[]> parameters) {
		this.paramsMap = parameters;
	}

	/**
	 * 设置标记,用于答案回显,设置文本框回显
	 */
	public String setText(String name) {
		Map<String, String[]> map = getAllParamsMap().get(this.model.getId());
		String[] values = map.get(name);
		return values[0];
	}

	/**
	 * 到达批注学生答题页面（首页）
	 */
	public String toEndorseStudentPage() {
		this.currPid = 0;
		List<Page> pages = pageService.findPagesWithChildren(tid);
		this.model = pages.get(0);
		this.model.setTest(courseService.getTest(tid));
		this.model.getTest().setCourse(courseService.getCourse(cid));

		// 获取user答案
		this.myAnswers = courseService.getUserAnswers(uid);

		// 存放test--> session
		sessionMap.put(CURRENT_TEST, this.model.getTest());
		// 将存放所有参数的大map -->session中(用户保存答案和回显).
		sessionMap.put(ALL_PARAMS_MAP,
				new HashMap<Integer, Map<String, String[]>>());

		// ----------将答案存放到seesion，key为answer_id,value为答案
		List<Answer> answers = courseService.getUserAnswers(uid);
		for (int j = 0; j < answers.size(); j++) {
			if (sessionMap.get(answers.get(j).getId() + "") == null) {
				sessionMap.put(answers.get(j).getId() + "", answers.get(j)
						.getSolution());
			}
		}
		return "EndorseStudentPage";
	}

	/**
	 * 页面处理（教师）
	 */
	public String doTeacherPage() {
		String submitName = getSubmitName();
		Page targPage = pageService.getPage(currPid);
		targPage.setTest(courseService.getTest(tid));
		// 上一页
		if (submitName.endsWith("pre")) {
			// System.out.println("-------pre---------");
			//mergeParamsIntoSession();
			this.model = pageService.getPrePage(targPage);
			this.model.setTest(courseService.getTest(tid));
			this.model.getTest().setCourse(courseService.getCourse(cid));

			// 获取user答案
			this.myAnswers = courseService.getUserAnswers(uid);
					
		}
		// 下一页
		else if (submitName.endsWith("next")) {
			// System.out.println("-------next---------");
			//mergeParamsIntoSession();
			this.model = pageService.getNextPage(targPage);
			this.model.setTest(courseService.getTest(tid));
			this.model.getTest().setCourse(courseService.getCourse(cid));

			// 获取user答案
			this.myAnswers = courseService.getUserAnswers(uid);

		}
		// 完成
		else if (submitName.endsWith("done")) {
			// System.out.println("-------done---------");
			
			// 保存批注
			List<Answer> answers = courseService.getUserAnswers(uid);
			for (int j = 0; j < answers.size(); j++) {
				// 插入批注
				System.out.println(sessionMap.get(answers.get(j).getId()));
				answers.get(j).setSolutionComments((String) sessionMap.get(answers.get(j).getId()));
				courseService.addSolutionCommets(answers.get(j));
			}

			// 清除session
			clearSessionData();
			return "teacherTestsAction";
		}
		// 退出
		else if (submitName.endsWith("exit")) {
			clearSessionData();
			return "teacherTestsAction";
		}
		return "teacherPage";
	}
}
