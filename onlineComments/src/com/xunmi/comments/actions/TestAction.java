package com.xunmi.comments.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.xunmi.comments.model.Test;
import com.xunmi.comments.model.User;
import com.xunmi.comments.service.CourseService;

@Controller
@Scope("prototype")
public class TestAction extends BaseAction<Test>{

	private static final long serialVersionUID = -2990496926463182328L;
	
	private Integer cid;

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	private Integer tid;

	public Integer getTid() {
		return tid;
	}

	public void setTid(Integer tid) {
		this.tid = tid;
	}
	
	//学生提交实验报告的时间
	private List<Date> submitTime;

	public List<Date> getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(List<Date> submitTime) {
		this.submitTime = submitTime;
	}

	//注入courseService
	@Resource
	private CourseService courseService;
	
	//实验集合
	private List<Test> myTests;
	//提交实验的学生集合
	private List<User> myUsers;

	public List<Test> getMyTests() {
		return myTests;
	}

	public void setMyTests(List<Test> myTests) {
		this.myTests = myTests;
	}
	
	public List<User> getMyUsers() {
		return myUsers;
	}

	public void setMyUsers(List<User> myUsers) {
		this.myUsers = myUsers;
	}
	
	//分页用的总个数
	private Integer totalNumber;
	//分页用的每页个数
	private Integer showNumber;
	//分页用的总页数
	private Integer totalPage;
	//分页用的当前页
	private Integer currPage;

	public Integer getTotalNumber() {
		return totalNumber;
	}

	public void setTotalNumber(Integer totalNumber) {
		this.totalNumber = totalNumber;
	}

	public Integer getShowNumber() {
		return showNumber;
	}

	public void setShowNumber(Integer showNumber) {
		this.showNumber = showNumber;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public Integer getCurrPage() {
		return currPage;
	}

	public void setCurrPage(Integer currPage) {
		this.currPage = currPage;
	}

	/**
	 * 到达我的课程下的实验列表页面
	 */
	public String myTests() {
		this.model.setCourse(courseService.getCourse(cid));
		//上下页优化
		if(this.totalNumber == null) {
			this.myTests = courseService.findMytests(cid);
			this.totalNumber = myTests.size();
		}
		this.showNumber = 1;
		if(this.currPage == null) {
			this.currPage = 1;
		}
		this.totalPage = this.totalNumber%this.showNumber == 0 ? this.totalNumber/this.showNumber : this.totalNumber/this.showNumber+1;
		this.myTests = courseService.findMytestsPaging(cid,currPage,showNumber);
		return "myTestListPage";
	}
	
	/**
	 * 到达我的课程下的实验列表页面Endorse
	 */
	public String myTestsEndorse() {
		this.model.setCourse(courseService.getCourse(cid));
		//上下页优化
		if(this.totalNumber == null) {
			this.myTests = courseService.findMytests(cid);
			this.totalNumber = myTests.size();
		}
		this.showNumber = 1;
		if(this.currPage == null) {
			this.currPage = 1;
		}
		this.totalPage = this.totalNumber%this.showNumber == 0 ? this.totalNumber/this.showNumber : this.totalNumber/this.showNumber+1;
		this.myTests = courseService.findMytestsPaging(cid,currPage,showNumber);
		return "myTestListEndorsePage";
	}
	
	/**
	 * 到达（学生）课程下的实验列表页面
	 */
	public String studentTest() {
		this.model.setCourse(courseService.getCourse(cid));
		this.myTests = courseService.findMytests(cid);
		return "studentTextListPage";
	}
	
	/**
	 * 到达添加实验的页面
	 */
	public String toAddTestPage() {
		this.model.setCourse(courseService.getCourse(cid));
		return "addTestPage" ;
	}
	
	/**
	 * 添加实验
	 */
	public String newTest() {
		this.model.setCourse(courseService.getCourse(cid));
		courseService.newTest(this.model,cid);
		return "myTestsAction";
	}
	
	/**
	 * 到达设计实验页面
	 */
	public String toDesignTest() {
		this.model = courseService.getTestWithChildren(tid);
		this.model.setCourse(courseService.getCourse(cid));
		return "designTestPage";
	}
	
	/**
	 * 删除实验，同时删除页面和问题
	 */
	public String doDeleteTest() {
		courseService.deleteTest(tid);
		return "myTestsAction";
	}
	
	/**
	 * 进入已提交实验的用户列表
	 */
	public String toSubmitTestPage() {
		this.model = courseService.getTest(tid);
		this.model.setCourse(courseService.getCourse(cid));
		this.myUsers= courseService.findSubmitUsers(tid);
		this.submitTime = new ArrayList<Date>();
		for(User u : myUsers)
		{
			Date st = courseService.findAnswerByTid(tid,u.getId()).getSubmitTime();
			this.submitTime.add(st);
		}
		return "submitTestListPage";
	}
	
	
}
