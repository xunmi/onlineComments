package com.xunmi.comments.dao.impl;

import org.springframework.stereotype.Repository;

import com.xunmi.comments.model.Classroom;

@Repository("classroomDao")
public class ClassroomDaoImpl extends BaseDaoImpl<Classroom> {

}
