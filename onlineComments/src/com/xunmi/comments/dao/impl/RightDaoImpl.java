package com.xunmi.comments.dao.impl;

import org.springframework.stereotype.Repository;

import com.xunmi.comments.model.security.Right;

@Repository("rightDao")
public class RightDaoImpl extends BaseDaoImpl<Right> {

}
