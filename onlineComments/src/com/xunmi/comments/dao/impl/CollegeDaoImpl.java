package com.xunmi.comments.dao.impl;

import org.springframework.stereotype.Repository;

import com.xunmi.comments.model.College;

@Repository("collegeDao")
public class CollegeDaoImpl extends BaseDaoImpl<College> {

}
