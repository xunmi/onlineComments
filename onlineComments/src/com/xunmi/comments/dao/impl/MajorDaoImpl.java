package com.xunmi.comments.dao.impl;

import org.springframework.stereotype.Repository;

import com.xunmi.comments.model.Major;

@Repository("majorDao")
public class MajorDaoImpl extends BaseDaoImpl<Major> {

}
