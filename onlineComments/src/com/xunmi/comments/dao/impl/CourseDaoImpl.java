package com.xunmi.comments.dao.impl;

import org.springframework.stereotype.Repository;

import com.xunmi.comments.model.Course;

@Repository("courseDao")
public class CourseDaoImpl extends BaseDaoImpl<Course> {

}
