package com.xunmi.comments.dao.impl;

import org.springframework.stereotype.Repository;

import com.xunmi.comments.model.CourseParent;

@Repository("courseParentDao")
public class CourseParentDaoImpl extends BaseDaoImpl<CourseParent> {

}
