package com.xunmi.comments.dao.impl;

import org.springframework.stereotype.Repository;

import com.xunmi.comments.model.Term;

@Repository("termDao")
public class TermDao extends BaseDaoImpl<Term> {

}
