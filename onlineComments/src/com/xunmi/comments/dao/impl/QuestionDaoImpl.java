package com.xunmi.comments.dao.impl;

import org.springframework.stereotype.Repository;

import com.xunmi.comments.model.Question;

@Repository("questionDao")
public class QuestionDaoImpl extends BaseDaoImpl<Question> {

}
