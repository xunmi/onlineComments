package com.xunmi.comments.dao.impl;

import org.springframework.stereotype.Repository;

import com.xunmi.comments.model.Page;

@Repository("pageDao")
public class PageDaoImpl extends BaseDaoImpl<Page> {

}
