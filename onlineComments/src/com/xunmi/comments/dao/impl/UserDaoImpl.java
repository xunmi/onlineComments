package com.xunmi.comments.dao.impl;

import org.springframework.stereotype.Repository;

import com.xunmi.comments.model.User;

@Repository("userDao")
public class UserDaoImpl extends BaseDaoImpl<User> {

}
