package com.xunmi.comments.dao.impl;

import org.springframework.stereotype.Repository;

import com.xunmi.comments.model.Answer;

@Repository("answerDao")
public class AnswerDaoImpl extends BaseDaoImpl<Answer> {

}
