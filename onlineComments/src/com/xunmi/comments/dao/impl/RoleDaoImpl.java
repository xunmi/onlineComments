package com.xunmi.comments.dao.impl;

import org.springframework.stereotype.Repository;

import com.xunmi.comments.model.security.Role;

@Repository("roleDao")
public class RoleDaoImpl extends BaseDaoImpl<Role> {

}
