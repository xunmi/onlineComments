package com.xunmi.comments.interceptor;

import com.xunmi.comments.model.User;
import com.xunmi.comments.aware.UserAware;
import com.xunmi.comments.actions.LoginAction;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

/**
 * ��¼������
 */
public class LoginInterceptor implements Interceptor {

	private static final long serialVersionUID = -8745073360224551868L;

	public void destroy() {

	}

	public void init() {

	}

	public String intercept(ActionInvocation arg0) throws Exception {
		@SuppressWarnings("rawtypes")
		Action action = (Action) arg0.getAction();
		if (action instanceof LoginAction) {
			return arg0.invoke();
		} else {
			User user = (User) arg0.getInvocationContext().getSession()
					.get("user");
			if (user == null) {
				// ȥ��½
				return "login";
			} else {
				// ����
				if (action instanceof UserAware) {
					// ע��user��action
					((UserAware) action).setUser(user);
				}
				return arg0.invoke();
			}
		}
	}

}
