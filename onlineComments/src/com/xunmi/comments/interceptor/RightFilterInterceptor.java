package com.xunmi.comments.interceptor;

import java.util.Map;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionProxy;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.xunmi.comments.aware.UserAware;
import com.xunmi.comments.model.User;
import com.xunmi.comments.model.security.Right;
import com.xunmi.comments.util.ValidateUtil;

/**
 * 登录拦截器
 */
public class RightFilterInterceptor implements Interceptor {

	private static final long serialVersionUID = -8745073360224551868L;

	public void destroy() {

	}

	public void init() {

	}

	public String intercept(ActionInvocation arg0) throws Exception {
		Action action = (Action) arg0.getAction();
		ActionProxy proxy = arg0.getProxy();
		String ns = proxy.getNamespace();
		String actionName = proxy.getActionName();
		System.out.println(ns);
		System.out.println(actionName);
		if (!ValidateUtil.isValid(ns) || "/".equals(ns)) {
			ns = "";
		}
		// 将超链接的参数部分滤掉?XXXX
		if (actionName.contains("?")) {
			actionName = actionName.substring(0, actionName.indexOf("?"));
		}
		String url = ns + "/" + actionName;
		System.out.println(url);

		// 从application中查询right
		Map<String, Right> map = (Map<String, Right>) arg0
				.getInvocationContext().getApplication().get("all_rights_map");
		// 通过url查询right对象
		Right r = map.get(url);
		// 公共资源？
		if (r == null || r.isCommon()) {
			System.out.println("公共资源");
			return arg0.invoke();
		} else {
			User user = (User) arg0.getInvocationContext().getSession()
					.get("user");
			// 登录？
			if (user == null) {
				System.out.println("未登录");
				return "login";
			} else {
				// 超级管理员？
				System.out.println("11111");
				if (action instanceof UserAware) {
					// 注入user给action
					((UserAware) action).setUser(user);
				}
				if (user.isSuperAdmin()) {
					System.out.println("超级管理员");
					return arg0.invoke();
				} else {
					System.out.println("222222");
					// 有权限？
					if (user.hasRight(r)) {
						return arg0.invoke();
					} else {
						return "error_no_right";
					}
				}
			}
		}
	}

}
