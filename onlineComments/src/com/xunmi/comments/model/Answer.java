package com.xunmi.comments.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * 作业提交表
 */
public class Answer implements Serializable {

	private static final long serialVersionUID = -7277950598843489994L;

	private Integer id;
	private String solution;// 学生提交的作业
	private String solutionComments;// 批注之后的作业
	private Date submitTime;// 作业提交时间

	private Integer t_id;
	
	public Integer getT_id() {
		return t_id;
	}

	public void setT_id(Integer t_id) {
		this.t_id = t_id;
	}

	// 建立Answer到User的多对一关系
	private User user;

	// 建立Answer到Question的一对一关系
	private Question question;

	// 建立Answer到Comment的一对多关系
	private Set<Comment> comments;

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public String getSolutionComments() {
		return solutionComments;
	}

	public void setSolutionComments(String solutionComments) {
		this.solutionComments = solutionComments;
	}

	public Date getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(Date submitTime) {
		this.submitTime = submitTime;
	}

	

}
