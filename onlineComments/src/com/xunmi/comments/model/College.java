package com.xunmi.comments.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class College implements Serializable {

	private static final long serialVersionUID = -6764819851603381974L;

	private Integer id;
	private String name;
	private String code;
	private String description;
	
	//建立College到Major的一对多关系
	private Set<Major> majors = new HashSet<Major>();

	//建立College到User（teacher）的一对多关系
	private Set<User> users = new HashSet<User>();
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Major> getMajors() {
		return majors;
	}

	public void setMajors(Set<Major> majors) {
		this.majors = majors;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}
	
	
}
