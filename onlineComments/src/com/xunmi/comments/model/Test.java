package com.xunmi.comments.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Test implements Serializable {

	private static final long serialVersionUID = -8461814820228470670L;
	
	private Integer id;
	private String title;
	private Date createTime = new Date();
	
	//最小页序
	private float minOrderno ;
	//最大页序
	private float maxOrderno ;

	public float getMinOrderno() {
		return minOrderno;
	}

	public void setMinOrderno(float minOrderno) {
		this.minOrderno = minOrderno;
	}

	public float getMaxOrderno() {
		return maxOrderno;
	}

	public void setMaxOrderno(float maxOrderno) {
		this.maxOrderno = maxOrderno;
	}

	//建立从Test到Course之间多对一关联关系
	private transient Course course;
	
	//建立从Test到Page的一对多关系
	private Set<Page> pages = new HashSet<Page>();
	
	//建立Test到User的多对多关联关系
	private Set<User> users = new HashSet<User>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
		minOrderno = id;
		maxOrderno = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Set<Page> getPages() {
		return pages;
	}

	public void setPages(Set<Page> pages) {
		this.pages = pages;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}
	

}
