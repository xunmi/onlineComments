package com.xunmi.comments.model;

import java.io.Serializable;
import java.util.Set;

public class Question implements Serializable {

	private static final long serialVersionUID = -504223854366047087L;

	private Integer id;
	// 问题类型（0-？）
	private Integer questionType;

	private String title;

	// 建立Question到Page的多对一关系
	private Page page;

	// 建立Question到Answer的一对一关系
	private Set<Answer> answers;

	// 建立Question到Comment的多对一关系
	private Set<Comment> comments;

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Set<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(Set<Answer> answers) {
		this.answers = answers;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getQuestionType() {
		return questionType;
	}

	public void setQuestionType(int questionType) {
		this.questionType = questionType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

}
