package com.xunmi.comments.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.xunmi.comments.model.security.Right;
import com.xunmi.comments.model.security.Role;

public class User implements Serializable {

	private static final long serialVersionUID = -8204954683803371218L;

	private Integer id;
	// 学号，登录用
	private String username;
	private String password;
	private char sex;
	// 用户名字
	private String nickname;
	private char type;

	// 注册时间
	private Date regdate = new Date();

	// 权限总和
	private long[] rightSum;

	// 是否是超级管理员
	private boolean superAdmin;

	public boolean isSuperAdmin() {
		return superAdmin;
	}

	public void setSuperAdmin(boolean superAdmin) {
		this.superAdmin = superAdmin;
	}

	public long[] getRightSum() {
		return rightSum;
	}

	public void setRightSum(long[] rightSum) {
		this.rightSum = rightSum;
	}

	// 建立Teacher到College的多对一关系
	private College college;

	// 建立User到Classroom的一对多关系
	private Classroom classroom;

	// 建立User到Test的多对多关系
	private Set<Test> tests = new HashSet<Test>();

	// 角色集合
	private Set<Role> roles = new HashSet<Role>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public char getSex() {
		return sex;
	}

	public void setSex(char sex) {
		this.sex = sex;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Date getRegdate() {
		return regdate;
	}

	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}

	public College getCollege() {
		return college;
	}

	public void setCollege(College college) {
		this.college = college;
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}

	public char getType() {
		return type;
	}

	public void setType(char type) {
		this.type = type;
	}

	public Set<Test> getTests() {
		return tests;
	}

	public void setTests(Set<Test> tests) {
		this.tests = tests;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	/**
	 * 计算用户权限总和
	 */
	public void calculateRightRightSum() {
		int pos = 0;
		long code = 0;
		for (Role role : roles) {
			// 判断是否是超级管理员
			if ("-1".equals(role.getRoleValue())) {
				this.superAdmin = true;
				// 释放资源
				roles = null;
				return;
			}
			for (Right r : role.getRights()) {
				pos = r.getRightPos();
				code = r.getRightCode();
				rightSum[pos] = rightSum[pos] | code;
			}
		}
		// 释放资源
		roles = null;
	}

	/**
	 * 判断用户是否有指定权限
	 */
	public boolean hasRight(Right r) {
		int pos = r.getRightPos();
		long code = r.getRightCode();
		return !((rightSum[pos] & code) == 0);
	}

}
