package com.xunmi.comments.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Page implements Serializable {

	private static final long serialVersionUID = 1086934325676546552L;
	
	private Integer id;
	private String name = "这是一个不可编辑的模板页";
	//页序
	private float orderno ;
	public float getOrderno() {
		return orderno;
	}
	public void setOrderno(float orderno) {
		this.orderno = orderno;
	}
	
	//建立Page到Test多对一的关系
	private Test test;
	
	//建立Page到Question的一对多的关系
	private Set<Question> questions = new HashSet<Question>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
		if(id != null){
			this.orderno = id ;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	public Set<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(Set<Question> questions) {
		this.questions = questions;
	}


	
}
