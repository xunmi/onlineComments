package com.xunmi.comments.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Classroom implements Serializable {

	private static final long serialVersionUID = -8324955333233580475L;

	private Integer id;
	private String name;
	private String code;
	private String description;
	private String createTime;

	// 建立Classroom到Major的多对一关系
	private Major major;

	// 建立Classroom到User的一对多关系
	private Set<User> users = new HashSet<User>();

	// 建立Classroom到Course的多对多关系
	private Set<Course> courses = new HashSet<Course>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Major getMajor() {
		return major;
	}

	public void setMajor(Major major) {
		this.major = major;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Set<Course> getCourses() {
		return courses;
	}

	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

}
