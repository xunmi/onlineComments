package com.xunmi.comments.model;

import java.io.Serializable;
import java.util.Date;

public class CourseParent implements Serializable{

	private static final long serialVersionUID = -8214835630779701570L;
	
	private Integer id;
	private String name;
	private String code;
	private String description;
	private Date createTime = new Date();
	
	//建立CourseParent到User的多对一关系
	private User user;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	
}
