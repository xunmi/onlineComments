package com.xunmi.comments.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Major implements Serializable {

	private static final long serialVersionUID = 4820720907164763787L;

	private Integer id;
	private String name;
	private String code;
	private String description;
	private Integer majorYears;// 专业学制年数

	// 建立Major到Classroom的一对多关系
	private Set<Classroom> classrooms = new HashSet<Classroom>();

	// 建立Major到College的多对一关系
	private College college;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Classroom> getClassrooms() {
		return classrooms;
	}

	public void setClassrooms(Set<Classroom> classrooms) {
		this.classrooms = classrooms;
	}

	public College getCollege() {
		return college;
	}

	public void setCollege(College college) {
		this.college = college;
	}

	public Integer getMajorYears() {
		return majorYears;
	}

	public void setMajorYears(Integer majorYears) {
		this.majorYears = majorYears;
	}

}
