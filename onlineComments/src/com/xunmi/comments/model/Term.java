package com.xunmi.comments.model;

import java.io.Serializable;
import java.util.Date;


public class Term implements Serializable{

	private static final long serialVersionUID = -8040704505171642598L;
	
	private Integer id;
	private String name;
	private Date createTime = new Date();
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}
