package com.xunmi.comments.model;

import java.io.Serializable;

public class Comment implements Serializable {

	private static final long serialVersionUID = 3452987742319828575L;

	private Integer id;
	private String commentId;// 作业中批注id，即为时间戳
	private String content;// 批注内容

	// 建立Comment到Answer的多对一关系
	private Answer answer;

	// 建立Comment到User的多对一关系
	private User user;

	// 建立Comment到Question的多对一关系
	private Question question;

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Answer getAnswer() {
		return answer;
	}

	public void setAnswer(Answer answer) {
		this.answer = answer;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCommentId() {
		return commentId;
	}

	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
