package com.xunmi.comments.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


public class Course implements Serializable{

	private static final long serialVersionUID = -3610030751774606697L;
	
	private Integer id;
	private String name;
	private String code;
	private String description;
	private String semester;
	private Date createTime = new Date();
	//被分配的教师id
	private Integer t_id;

	//建立Course到User的多对一关系
	private User user;
	
	//建立Course到Test的一对多关系
	private Set<Test> tests = new HashSet<Test>();
	
	//建立Course到CourseParent的多对一关系
	private CourseParent courseParent;
	
	//建立Course到Classroom的多对多关系
	private Set<Classroom> classrooms = new HashSet<Classroom>();
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSemester() {
		return semester;
	}
	public void setSemester(String semester) {
		this.semester = semester;
	}
	
	public Integer getT_id() {
		return t_id;
	}
	public void setT_id(Integer t_id) {
		this.t_id = t_id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Set<Test> getTests() {
		return tests;
	}
	public void setTests(Set<Test> tests) {
		this.tests = tests;
	}
	public CourseParent getCourseParent() {
		return courseParent;
	}
	public void setCourseParent(CourseParent courseParent) {
		this.courseParent = courseParent;
	}
	public Set<Classroom> getClassrooms() {
		return classrooms;
	}
	public void setClassrooms(Set<Classroom> classrooms) {
		this.classrooms = classrooms;
	}
	
	
	
}
