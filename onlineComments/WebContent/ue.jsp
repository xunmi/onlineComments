<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>在线批注系统登录</title>
	<link rel="stylesheet" type="text/css" href='<s:url value="/css/bootstrap.min.css" />'>
	<link rel="stylesheet" type="text/css" href='<s:url value="ueditor/themes/default/css/ueditor.css" />'>
	<script type="text/javascript" charset="utf-8" src="ueditor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="ueditor/ueditor.all.js"></script>

<style type="text/css">
body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #eee;
  font-family: "Microsoft Yahei";
}
.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="text"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
</style>

</head>
<body>
  <div class="container">
  
  	<script type="text/javascript">
  		UE.getEditor('content');
  	</script>
  
  	<form action="" method="post">
    <textarea id="content" name="content">测试ueditor</textarea>
    <br>
    <input type="submit" value="提交" />
    </form>
    
    <div class="container">
   		<h2><pre>
		#include &lt;stdio.h&gt;
		int main()
		{
		    int a=1,b=2;
		    c=a+b;
		    print("%f",c)
		    return 0;
		}
		</pre></h2>
    </div>
  </div>
  

</body>
</html>