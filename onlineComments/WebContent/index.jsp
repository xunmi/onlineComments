<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>在线批注系统登录</title>
	<link rel="stylesheet" type="text/css" href='<s:url value="/css/bootstrap.min.css" />'>

<style type="text/css">
body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #eee;
  font-family: "Microsoft Yahei";
}
.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="text"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
</style>

</head>
<body>
  <div class="container">
    <form class="form-signin" action="LoginAction_doLogin.action" method="post">
      <h2 class="form-signin-heading">请登录</h2>
      <input type="text" name="username" class="form-control" placeholder="用户名">
      <label><s:actionerror/></label>
      <input type="password" name="password" class="form-control" placeholder="密码">
      <input type="text" name="verify" class="form-control" placeholder="验证码">
      <div class="checkbox">
        <img src="/index.php/Admin/Login/verify" id="code"/> <a href="javascript:void(change_code(this));">看不清</a>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">登录</button>
    </form>
  </div>
  

</body>
</html>