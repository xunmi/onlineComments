<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<div class="col-sm-3 col-md-2 sidebar">
	<ul class="nav nav-sidebar">
       	<li><a href="RightAction_findAllRights.action">权限管理</a></li>
       	<li><a href="RoleAction_findAllRoles.action">角色管理</a></li>
       	<li><a href="UserAuthorizeAction_findAllUsers.action">用户授权</a></li>
       </ul>
</div>