<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/basisManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						专业管理
						<small>
							MAJOR MANAGE
						</small>
					</h3>
				</div>
				
				<ol class="breadcrumb">
				  <li class="active">专业管理</li>
				</ol>

				<h4><span class="label label-success">请选择一个学院查看其专业信息</span></h4>
				<hr>
				
				<s:if test="colleges.isEmpty() == true">目前没有任何学院！</s:if >
				<s:else>
				<table class="table table-striped table-hover">
					<tr>
						<th><input type="checkbox"></th>
						<th>代号</th>
						<th>名称</th>
						<th>描述</th>
						<th>所属专业</th>
					</tr>
					<s:iterator value="colleges">
					<s:set var="cId" value="id" />
					<tr>
						<td><input type="checkbox"></td>
						<td><s:property value="code" /></td>
						<td><s:property value="name" /></td>
						<td><s:property value="description" /></td>
						<td>
							<a title="查看专业" href="MajorAction_checkMajor.action?cid=<s:property value="#cId" />&cname=<s:property value="name" />" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-search"></span></a>
						</td>
					</tr>
					</s:iterator>
				</table>
				</s:else>

			</div>
		</div>
	</div>
</body>
</html>