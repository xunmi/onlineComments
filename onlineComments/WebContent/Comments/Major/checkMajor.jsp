<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/basisManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						专业管理
						<small>
							MAJOR MANAGE
						</small>
					</h3>
				</div>
				
				<ol class="breadcrumb">
				  <li><a href="MajorAction_majorManage.action">专业管理</a></li>
				  <li class="active"><s:property value="college.name" /></li>
				</ol>

				<h4><span class="label label-info"><s:property value="cname" />专业如下</span></h4>
				<hr>
				
				<s:if test="majors.isEmpty() == true">目前没有任何专业！</s:if >
				<s:else>
				<table class="table table-striped table-hover">
					<tr>
						<th><input type="checkbox"></th>
						<th>代号</th>
						<th>名称</th>
						<th>学年</th>
						<th>描述</th>
						<th>操作</th>
					</tr>
					<s:iterator value="majors">
					<s:set var="mId" value="id" />
					<tr>
						<td><input type="checkbox"></td>
						<td><s:property value="code" /></td>
						<td><s:property value="name" /></td>
						<td><s:property value="majorYears" /></td>
						<td><s:property value="description" /></td>
						<td>
							<a href="MajorAction_editMajor.action?mid=<s:property value="#mId" />&cid=<s:property value="cid" />" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
							<a href="MajorAction_deleteMajor.action?mid=<s:property value="#mId" />&cid=<s:property value="cid" />" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>
						</td>
					</tr>
					</s:iterator>
				</table>
				</s:else>
				<hr>
				<a href="MajorAction_addMajor.action?cid=<s:property value="cid" />" class="btn btn-primary">添加一个专业</a>

			</div>
		</div>
	</div>
</body>
</html>