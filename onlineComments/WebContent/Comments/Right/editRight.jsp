<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/rightManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						权限添加/更新
						<small>
							RIGHT ADD OR UPDATE
						</small>
					</h3>
				</div>
				
				<ol class="breadcrumb">
					<li><a href="RightAction_findAllRights.action">权限管理</a></li>
					<li class="active">权限添加/更新</li>
				</ol>
				
				<form class="form-horizontal" action="RightAction_saveOrUpdateRight" method="post">
					<s:hidden name="id" />
				   <div class="form-group">
				      <label for="firstname" class="col-sm-2 control-label">权限名称</label>
				      <div class="col-sm-9">
				         <input type="text" name="rightName" class="form-control" value='<s:property value="rightName" />' placeholder="请输入权限名称">
				      </div>
				   </div>
				   <div class="form-group">
				      <label for="lastname" class="col-sm-2 control-label">权限URL</label>
				      <div class="col-sm-9">
				         <input type="text" name="rightUrl" class="form-control" value='<s:property value="rightUrl" />' placeholder="请输入URL">
				      </div>
				   </div>
				   <div class="form-group">
				      <label for="lastname" class="col-sm-2 control-label">权限位</label>
				      <div class="col-sm-2">
				         <input type="text" name="rightPos" class="form-control" value='<s:property value="rightPos" />' placeholder="请输入URL">
				      </div>
				   </div>
				   <div class="form-group">
				      <label for="lastname" class="col-sm-2 control-label">权限码</label>
				      <div class="col-sm-2">
				         <input type="text" name="rightCode" class="form-control" value='<s:property value="rightCode" />' placeholder="请输入权限码">
				      </div>
				   </div>
				   <div class="form-group">
				      <label for="lastname" class="col-sm-2 control-label">公共资源</label>
				      <div class="col-sm-2">
				         <s:checkbox name="common"></s:checkbox>
				      </div>
				   </div>
				   <div class="form-group">
				   		<label for="lastname" class="col-sm-2 control-label">权限描述</label>
				      <div class="col-sm-9">
				         <textarea class="form-control" name="rightDesc" rows="3" placeholder="请输入描述"><s:property value="rightDesc" /></textarea>
				      </div>				    
				  </div>
				   <div class="form-group">
				      <div class="col-sm-offset-2 col-sm-10">
				         <button type="submit" class="btn btn-warning">确认添加/更新</button>
				      </div>
				   </div>
				</form>
				
			</div>
		</div>
	</div>
</body>
</html>