<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/rightManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						权限管理
						<small>
							RIGHT MANAGE
						</small>
					</h3>
				</div>
				
				<ol class="breadcrumb">
					<li class="active">权限管理</li>
				</ol>
				
				<a class="btn btn-success" href="RightAction_toAddRightPage.action">添加权限</a>
				<br><br>
				
				<s:if test="allRights.isEmpty() == true">目前没有任何权限!</s:if >
				<s:else>
					<s:form action="RightAction_batchUpdateRights" namespace="/" method="post">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<td>ID</td>
								<td>权限名称</td>
								<td>公共资源</td>
								<td>权限URL</td>
								<td>权限位</td>
								<td>权限码</td>
								<td>操作</td>
							</tr>
						</thead>
						<tbody>
							<s:iterator value="allRights" status="st">
								<s:set var="rightId" value="id" />
								<tr>
									<td>
										<s:property value="id" />
										<input type="hidden" name="allRights[<s:property value="#st.index" />].id" value="<s:property value="id" />" />
									</td>
									<td>
										<input type="text" name="allRights[<s:property value="#st.index" />].rightName" value="<s:property value="rightName" />" />
									</td>
									<td>
										<s:checkbox name="allRights[%{#st.index}].common" />
									</td>
									<td><s:property value="rightUrl" /></td>
									<td><s:property value="rightPos" /></td>
									<td><s:property value="rightCode" /></td>
									<td>
										<a title="修改" class="btn btn-info btn-xs" href="RightAction_editRight.action?rightId=<s:property value="id" />"><span class="glyphicon glyphicon-pencil"></span></a>
										<a title="删除" class="btn btn-danger btn-xs" href="RightAction_deleteRight.action?rightId=<s:property value="id" />"><span class="glyphicon glyphicon-trash"></span></a>
									</td>
								</tr>
							</s:iterator>
						</tbody>
					</table>
					<button class="btn btn-warning" type="submit">提交</button><br><br>
					</s:form>
				</s:else>

			</div>
		</div>
	</div>
</body>
</html>