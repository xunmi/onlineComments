<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/basisManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						编辑班级
						<small>
							CLASSROOM ADD
						</small>
					</h3>
				</div>
				
				<ol class="breadcrumb">
				  <li><a href="ClassroomAction_classroomManage.action">班级管理</a></li>
				  <li><a href="ClassroomAction_findClassroomByMid.action?mid=<s:property value="major.id" />&semester=<s:property value="semester" />"><s:property value="major.name" /></a></li>
				  <li class="active"><s:property value="name" /></li>
				</ol>

				<form class="form-horizontal" action="ClassroomAction_updateClassroom.action" method="post">
					<div class="form-group">
				      <label for="firstname" class="col-sm-2 control-label">开班学年</label>
				      <div class="col-sm-2">
				         	<select id="startTime" name="startTime" class="form-control">
				         		<option value='<s:property value="startTime" />'><s:property value="startTime" /></option>
				         	</select>
				      </div>
				   	</div>
				   <div class="form-group">
				      <label for="firstname" class="col-sm-2 control-label">代号</label>
				      <div class="col-sm-9">
				         <input type="text" name="code" class="form-control" id="firstname" 
				            placeholder="请输入代号" value='<s:property value="code" />'>
				      </div>
				   </div>
				   <div class="form-group">
				      <label for="lastname" class="col-sm-2 control-label">名称</label>
				      <div class="col-sm-9">
				         <input type="text" name="name" class="form-control" id="lastname" 
				            placeholder="请输入名称" value='<s:property value="name" />'>
				      </div>
				   </div>
				   <div class="form-group">
				   		<label for="lastname" class="col-sm-2 control-label">描述</label>
				      <div class="col-sm-9">
				         <textarea class="form-control" name="description" rows="3" placeholder="请输入描述"><s:property value="description" /></textarea>
				      </div>				    
				  </div>
				   <div class="form-group">
				      <div class="col-sm-offset-2 col-sm-10">
				      	<input type="hidden" name="id" value='<s:property value="id" />' />
				      	<input type="hidden" name="createTime" value='<s:property value="createTime" />' />
				      	<input type="hidden" name="major.id" value='<s:property value="major.id" />' />
				      	<input type="hidden" name="mid" value='<s:property value="mid" />' />
				         <button type="submit" class="btn btn-warning">确认修改</button>
				      </div>
				   </div>
				</form>

			</div>
		</div>
	</div>
</body>
</html>