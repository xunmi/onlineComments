<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/basisManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						班级管理
						<small>
							CLASSROOM MANAGE
						</small>
					</h3>
				</div>
				
				<ol class="breadcrumb">
					<li class="active">班级管理</li>
				</ol>
				
				<script type="text/javascript">
					
					$(document).ready(function(){
						var time = new Date();
						var nowYear = time.getFullYear();
						for(var i=nowYear-5;i<nowYear+5;i++){
							$("#semester").append("<option value='"+i+"-"+(i+1)+"'>"+i+"-"+(i+1)+"</option>");
						}
						$("#college").change(function(){
							//每次清楚之前的选择
							$("#major option").remove();
							
							var target = $(this).children('option:selected').val();
							var url = "AjaxJsonAction_findAllMajorByCid.action";
							var params = {
									"cid":target,
							};
							$.getJSON(url, params, function(data){
								var info = eval("(" + data +")");
								for(var i in info){
									$("#major").append("<option value='"+i+"'>"+info[i]+"</option>");
								}
							});
						});
					})
				</script>

				<form action="ClassroomAction_findClassroomByMid.action" method="post">
				<div class="row">
					<div class="col-md-3">
						<label>选择学院</label>
						<select id="college" name="cid" class="form-control">
							<option>请选择</option>
							<s:iterator value="colleges">
								<option value='<s:property value="id" />'><s:property value="name" /></option>
							</s:iterator>
						</select>
					</div>
					<div class="col-md-3">
						<label>选择专业</label>
						<select id="major" name="mid" class="form-control">
							<option>请选择</option>
						</select>
					</div>
					<div class="col-md-3">
						<label>选择学期</label>
						<select id="semester" name="semester" class="form-control">
							<option value="nochoose">请选择</option>
						</select>
					</div>
					<div class="col-md-1">
						<label></label>
						<input class="btn btn-primary" type="submit" value="查询" style="margin-top:4px;"/>
					</div>
				</div>
				</form>
				<hr>

			</div>
		</div>
	</div>
</body>
</html>