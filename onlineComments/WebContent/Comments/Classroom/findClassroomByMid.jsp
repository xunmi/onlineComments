<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<s:include value="/header.jsp" />
<div class="container-fluid">
	<div class="row">
		<s:include value="/basisManageSideNav.jsp" />
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<div class="page-header">
				<h3>
					班级管理 <small> CLASSROOM MANAGE </small>
				</h3>
			</div>

			<ol class="breadcrumb">
				<li><a href="ClassroomAction_classroomManage.action">班级管理</a></li>
				<li class="active"><s:property value="major.name" /></li>
			</ol>

			<script type="text/javascript">
				$(document)
						.ready(
								function() {
									$("#college")
											.change(
													function() {
														//每次清楚之前的选择
														$("#major option")
																.remove();

														var target = $(this)
																.children(
																		'option:selected')
																.val();
														var url = "AjaxJsonAction_findAllMajorByCid.action";
														var params = {
															"cid" : target,
														};
														$
																.getJSON(
																		url,
																		params,
																		function(
																				data) {
																			var info = eval("("
																					+ data
																					+ ")");
																			for ( var i in info) {
																				$(
																						"#major")
																						.append(
																								"<option value='"+i+"'>"
																										+ info[i]
																										+ "</option>");
																			}
																		});
													});
								})
			</script>

			<form action="ClassroomAction_findClassroomByMid.action"
				method="post">
				<div class="row">
					<div class="col-md-3">
						<label>选择学院</label> <select id="college" name="cid"
							class="form-control">
							<option>请选择</option>
							<s:iterator value="colleges">
								<option value='<s:property value="id" />'><s:property
										value="name" /></option>
							</s:iterator>
						</select>
					</div>
					<div class="col-md-3">
						<label>选择专业</label> <select id="major" name="mid"
							class="form-control">
							<option>请选择</option>
						</select>
					</div>
					<div class="col-md-3">
						<label>选择学期</label> <select id="semester" name="semester"
							class="form-control">
							<option value="nochoose">请选择</option>
							<option value="2012-2013">2012-2013</option>
							<option value="2013-2014">2013-2014</option>
							<option value="2014-2015">2014-2015</option>
							<option value="2015-2016">2015-2016</option>
							<option value="2016-2017">2016-2017</option>
						</select>
					</div>
					<div class="col-md-1">
						<label></label> <input class="btn btn-primary" type="submit"
							value="查询" style="margin-top: 4px;" />
					</div>
				</div>
			</form>
			<hr>

			<s:if test="classrooms.isEmpty() == true">目前没有任何班级！</s:if>
			<s:else>
				<table class="table table-striped table-hover">
					<tr>
						<th><input type="checkbox"></th>
						<th>代号</th>
						<th>名称</th>
						<th>描述</th>
						<th>操作</th>
					</tr>
					<s:iterator value="classrooms">
						<s:set var="cId" value="id" />
						<tr>
							<td><input type="checkbox"></td>
							<td><s:property value="code" /></td>
							<td><s:property value="name" /></td>
							<td><s:property value="description" /></td>
							<td><a title="修改"
								href="ClassroomAction_editClassroom.action?cid=<s:property value="#cId" />&mid=<s:property value="mid" />&semester=<s:property value="semester" />"
								class="btn btn-success btn-xs"><span
									class="glyphicon glyphicon-pencil"></span></a> <a title="删除"
								href="ClassroomAction_deleteClassroom.action?cid=<s:property value="#cId" />&mid=<s:property value="mid" />"
								class="btn btn-danger btn-xs"><span
									class="glyphicon glyphicon-trash"></span></a></td>
						</tr>
					</s:iterator>
				</table>
			</s:else>
			<hr>
			<a
				href="ClassroomAction_addClassroom.action?mid=<s:property value="mid" />&semester=<s:property value="semester" />"
				class="btn btn-warning">添加一个班级</a>

		</div>
	</div>
</div>
</body>
</html>