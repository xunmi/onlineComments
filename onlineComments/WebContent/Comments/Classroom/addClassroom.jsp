<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/basisManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						添加班级
						<small>
							CLASSROOM ADD
						</small>
					</h3>
				</div>
				<s:debug></s:debug>
				
				<script type="text/javascript">
					$(document).ready(function(){
						var time = new Date();
						var nowYear = time.getFullYear();
						for(var i=nowYear-5;i<nowYear+5;i++){
							$("#startTime").append("<option value='"+i+"'>"+i+"</option>");
						}
					});
				</script>
				
				<ol class="breadcrumb">
				  <li><a href="ClassroomAction_classroomManage.action">班级管理</a></li>
				  <li><a href="ClassroomAction_findClassroomByMid.action?mid=<s:property value="major.id" />&semester=<s:property value="semester" />"><s:property value="major.name" /></a></li>
				  <li class="active">添加班级</li>
				</ol>

				<form class="form-horizontal" action="ClassroomAction_doAddClassroom.action" method="post">
					<div class="form-group">
				      <label for="firstname" class="col-sm-2 control-label">开班学年</label>
				      <div class="col-sm-2">
				         	<select id="startTime" name="startTime" class="form-control">
				         		<option>请选择</option>
				         	</select>
				      </div>
				   	</div>
				   <div class="form-group">
				      <label for="firstname" class="col-sm-2 control-label">代号</label>
				      <div class="col-sm-9">
				         <input type="text" name="code" class="form-control" id="firstname" 
				            placeholder="请输入代号">
				      </div>
				   </div>
				   <div class="form-group">
				      <label for="lastname" class="col-sm-2 control-label">名称</label>
				      <div class="col-sm-9">
				         <input type="text" name="name" class="form-control" id="lastname" 
				            placeholder="请输入名称">
				      </div>
				   </div>
				   <div class="form-group">
				   		<label for="lastname" class="col-sm-2 control-label">描述</label>
				      <div class="col-sm-9">
				         <textarea class="form-control" name="description" rows="3" placeholder="请输入描述"></textarea>
				      </div>				    
				  </div>
				   <div class="form-group">
				      <div class="col-sm-offset-2 col-sm-10">
				      	<input type="hidden" name="mid" value="<s:property value="mid" />" />
				         <button type="submit" class="btn btn-warning">确认添加</button>
				      </div>
				   </div>
				</form>
				<h2><s:actionerror></s:actionerror></h2>

			</div>
		</div>
	</div>
</body>
</html>