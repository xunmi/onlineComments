<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/basisManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						学院管理
						<small>
							COLLEGE MANAGE
						</small>
					</h3>
				</div>
				
				<ol class="breadcrumb">
				  <li class="active">学院管理</li>
				</ol>

				<h4><span class="label label-success">学院列表</span></h4>
				<hr>
				
				<s:if test="colleges.isEmpty() == true">目前没有任何学院！</s:if >
				<s:else>
				<table class="table table-striped table-hover">
					<tr>
						<th><input type="checkbox"></th>
						<th>代号</th>
						<th>名称</th>
						<th>描述</th>
						<th>操作</th>
					</tr>
					<s:iterator value="colleges">
					<s:set var="cId" value="id" />
					<tr>
						<td><input type="checkbox"></td>
						<td><s:property value="code" /></td>
						<td><s:property value="name" /></td>
						<td><s:property value="description" /></td>
						<td>
							<a title="修改" href="CollegeAction_editCollege.action?cid=<s:property value="#cId" />" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
							<a title="删除" href="CollegeAction_deleteCollege.action?cid=<s:property value="#cId" />" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>
						</td>
					</tr>
					</s:iterator>
				</table>
				</s:else>
				<hr>
				<a href="CollegeAction_addCollege.action" class="btn btn-warning">添加一个学院</a>

			</div>
		</div>
	</div>
</body>
</html>