<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/basisManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						添加学院
						<small>
							COLLEGE ADD
						</small>
					</h3>
				</div>
				
				<ol class="breadcrumb">
				  <li><a href="CollegeAction_collegeManage.action">学院管理</a></li>
				  <li class="active">添加学院</li>
				</ol>

				<form class="form-horizontal" action="CollegeAction_doAddCollege.action" method="post">
				   <div class="form-group">
				      <label for="firstname" class="col-sm-1 control-label">代号</label>
				      <div class="col-sm-10">
				         <input type="text" name="code" class="form-control" id="firstname" 
				            placeholder="请输入代号">
				      </div>
				   </div>
				   <div class="form-group">
				      <label for="lastname" class="col-sm-1 control-label">名称</label>
				      <div class="col-sm-10">
				         <input type="text" name="name" class="form-control" id="lastname" 
				            placeholder="请输入名称">
				      </div>
				   </div>
				   <div class="form-group">
				   		<label for="lastname" class="col-sm-1 control-label">描述</label>
				      <div class="col-sm-10">
				         <textarea class="form-control" name="description" rows="3" placeholder="请输入描述"></textarea>
				      </div>				    
				  </div>
				   <div class="form-group">
				      <div class="col-sm-offset-1 col-sm-10">
				         <button type="submit" class="btn btn-warning">确认添加</button>
				      </div>
				   </div>
				</form>
				<h2><s:actionerror></s:actionerror></h2>

			</div>
		</div>
	</div>
</body>
</html>