<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>我的课程</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
<script src="<s:url value='/Js/jquery-1.7.2.min.js' />"></script>
<script src="<s:url value='/Js/bootstrap.min.js' />"></script>
<script src="<s:url value='/Js/public.js' />"></script>
</head>
<body>
	<s:include value="header_three.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb diyol">
				  <li><a href="#">
				  	<s:if test="#session['user'] != null">
						<s:property value="#session['user'].nickname" />
					</s:if>
				  </a></li>
				  <li class="active">我的课程</li>
				</ol>
				<s:if test="myCourses.isEmpty() == true">目前您没有课程!</s:if >
				<s:else>
				<div class="row">
					<table id="sample-table-1" class="table table-striped table-bordered table-hover">
		                <thead>
		                    <tr>
		                        <th>ID</th>
		                        <th>课程编号</th>
		                        <th>课程名字</th>
		                        <th>学期</th>
		                        <th>授课教师</th>
		                        <th>操作</th>
		                    </tr>
		                </thead>
		
		                <tbody>
		                	<s:iterator value="myCourses" status="i">
		                	<s:set var="cId" value="id" />
		                    <tr>
		                        <!-- <td><s:property value="id" /></td> -->
		                        <td><s:property value="#i.index+1" /></td>
		                        <td><s:property value="code" /></td>
		                        <td><s:property value="name" /></td>
		                        <td><s:property value="semester" /></td>
		                        <td><s:property value="user.nickname" /></td>
		                        <td>
		                            <div class="btn-group">
									    <a class="btn btn-success btn-sm" href="TestAction_studentTest?cid=<s:property value="#cId" />">
	                                    	<span class="glyphicon glyphicon-th-list"></span>
		                                </a>
		                            </div>
		                        </td>
		                    </tr>
		                    </s:iterator>
		                </tbody>
		            </table>
				</div>
				</s:else>
			</div>
		</div>
	</div>
</body>
</html>