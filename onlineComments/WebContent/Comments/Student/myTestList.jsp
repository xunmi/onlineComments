<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>我的课程</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
<script src="<s:url value='/Js/jquery-1.7.2.min.js' />"></script>
<script src="<s:url value='/Js/public.js' />"></script>
<style type="text/css">
	.ace{
		z-index: 12;
		width: 18px;
		height: 18px;
		cursor: pointer;
	}
	.center{
		vertical-align: middle;
		text-align: center;
	}
</style>
</head>
<body>
	<s:include value="header_three.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb diyol">
				  <li><a href="#">
				  	<s:if test="#session['user'] != null">
						<s:property value="#session['user'].nickname" />
					</s:if>
				  </a></li>
				  <li><a href="CourseAction_studentCourses"><s:property value="course.name" /></a></li>
				  <li class="active">实验列表</li>
				</ol>
		        <h1><span class="label label-success"><s:property value="course.name" /></span></h1><br>
				<s:if test="myTests.isEmpty() == true">目前该课程没有任何实验报告!</s:if >
				<s:else>
				<div class="row">
					<table id="sample-table-1" class="table table-striped table-bordered table-hover">
		                <thead>
		                    <tr>
		                        <th>ID</th>
		                        <th>实验名称</th>
		                        <th>创建时间</th>
		                        <th>操作</th>
		                    </tr>
		                </thead>
		
		                <tbody>
		                	<s:iterator value="myTests" status="i">
		                	<s:set var="tId" value="id" />
		                    <tr>
		                        <!-- <td><s:property value="id" /></td> -->
		                        <td><s:property value="#i.index+1" /></td>
		                        <td><s:property value="title" /></td>
		                        <td><s:date name="createTime" format="yy/MM/dd HH:mm" /></td>
		                        <td>
		                            <div class="btn-group">
		                                <a class="btn btn-info btn-sm" href="PageAction_toStudentPage?cid=<s:property value='cid' />&tid=<s:property value='id' />">
		                                    <span class="glyphicon glyphicon-pencil"></span>
		                                </a>
		                            </div>
		                        </td>
		                    </tr>
		                    </s:iterator>
		                </tbody>
		            </table>
				</div>
				</s:else>
			</div>
		</div>
	</div>
</body>
</html>