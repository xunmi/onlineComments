<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>我的课程</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
<script src="<s:url value='/Js/jquery-1.7.2.min.js' />"></script>
<script src="<s:url value='/Js/bootstrap.min.js' />"></script>
<script src="<s:url value='/Js/public.js' />"></script>
<script type="text/javascript">
function collegeToTeacher(){
	var url = "AjaxLinkageAction_collegeToTeacher.action";
	data = {"cid":$("#ajax").val()};
	$.getJSON(url, data,function(data){
		var obj = eval( "(" + data + ")" );
		$('#ajax_two').empty();
		$.each(obj,function(i,list){
			var tr = $("<option value='"+list.id+"'>"+list.id+list.nickname+"</option>");
			$('#ajax_two').append(tr);
		});
	});	
}
</script>
</head>
<body>
	<s:include value="header_one.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
				<div class="page-header">
					<h3>
						课程分配
						<small>
							COURSE DISTRIBUTION
						</small>
					</h3>
				</div>
			
				<ol class="breadcrumb">
				  <li class="active">课程分配</li>
				</ol>
				
				
				<div class="row">
					<div class="col-md-2">
						<a href="LinkageAction_toAllotCoursePage" class="btn btn-info">
				          <span class="glyphicon glyphicon-plus"></span> 添加课程分配
				        </a>
		        	</div>
		        	<div class="col-md-10">
			        	<div class="row">
			        		<form class="form-horizontal" role="form" action="CourseAction_doInquireCourse" namespace="/" method="post">
				        	<div class="col-md-3 col-sm-offset-1">
				        		<div class="form-group">
							      <div class="col-sm-2">
							         <select class="form-control term" onChange="collegeToTeacher()" id="ajax" name="clid">
									  	<option value="0">---请选择学院---</option>
									  	<s:iterator value="colleges">
									  	<option value="<s:property value="id" />"><s:property value="name" /></option>
									  	</s:iterator>
									 </select>
							      </div>
							    </div>
				        	</div>
				        	<div class="col-md-3">
				        		<div class="form-group">
							      <div class="col-sm-2">
							         <select class="form-control term" id="ajax_two" name="tid">
									  	<option value="0">---请选择教师---</option>
									 </select>
							      </div>
							    </div>
				        	</div>
				        	<div class="col-md-3">
				        		<div class="form-group">
							      <div class="col-sm-2">
							         <select class="form-control term" id="ajax" name="cid">
									  	<option value="0">---请选择课程---</option>
									  	<s:iterator value="courseParents">
									  	<option value="<s:property value="id" />"><s:property value="name" /></option>
									  	</s:iterator>
									 </select>
							      </div>
							    </div>
				        	</div>
				        	<div class="col-md-2">
							    <button type="submit" class="btn btn-success">
								  <span class="glyphicon glyphicon-search"></span> 搜索
								</button>
				        	</div>
				        	</form>
				        </div>
			        </div>
		        </div>
				<s:if test="myCourses.isEmpty() == true">目前您没有已分配的课程!</s:if >
				<s:else>
					<table id="sample-table-1" class="table table-striped table-hover">
		                <thead>
		                    <tr>
		                        <th class="center">
		                            <label>
		                                <input type="checkbox" level="1"/>
		                            </label>
		                        </th>
		                        <th>ID</th>
		                        <th>课程编号</th>
		                        <th>课程名字</th>
		                        <th>学期</th>
		                        <th>授课老师</th>
		                        <th>分配时间</th>
		                        <th>操作</th>
		                    </tr>
		                </thead>
		
		                <tbody>
		                	<s:iterator value="myCourses" status="i" var="c">
		                	<s:set var="cId" value="id" />
		                    <tr>
		                        <td class="center">
		                            <label>
		                                <input type="checkbox" level="2"/>
		                            </label>
		                        </td>
		                        <!-- <td><s:property value="id" /></td> -->
		                        <td><s:property value="#i.index+1" /></td>
		                        <td><s:property value="code" /></td>
		                        <td><s:property value="name" /></td>
		                        <td><s:property value="semester" /></td>
		                        <td><s:property value="#c.user.username + #c.user.nickname" /></td>
		                        <td><s:date name="createTime" format="yy/MM/dd HH:mm" /></td>
		                        <td>
	                                <a title="删除" class="btn btn-danger btn-sm" href="CourseAction_doDeleteCourse?cid=<s:property value="#cId" />">
	                                    <span class="glyphicon glyphicon-trash"></span>
	                                </a>
		                        </td>
		                    </tr>
		                    </s:iterator>
		                </tbody>
		            </table>
				</s:else>
			</div>
		</div>
	</div>
</body>
</html>