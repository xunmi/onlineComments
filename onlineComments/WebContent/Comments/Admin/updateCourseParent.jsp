<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>更新课程</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
</head>
<body>
	<s:include value="header_one.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
				<div class="page-header">
					<h3>
						课程更新
						<small>
							COURSE UPDATE
						</small>
					</h3>
				</div>
			
				<ol class="breadcrumb">
				  <li><a href="CourseParentAction_myCourseParents"><s:property value="name" /></a></li>
				  <li class="active">课程更新</li>
				</ol>
				<div class="row">
					<form class="form-horizontal" role="form" action="CourseParentAction_doUpdateCourseParent" namespace="/" method="post">
					   <s:hidden name="id" />
					   <div class="form-group">
					      <label for="firstname" class="col-sm-2 control-label">课程名字</label>
					      <div class="col-sm-9">
					         <input type="text" class="form-control" id="firstname" name="name"
					            placeholder="请输入课程的名字" value="<s:property value='name' />">
					      </div>
					   </div>
					   <div class="form-group">
					      <label for="secondname" class="col-sm-2 control-label">课程编号</label>
					      <div class="col-sm-9">
					         <input type="text" class="form-control" id="secondname" name="code"
					            placeholder="请输入课程的编号" value="<s:property value='code' />">
					      </div>
					   </div>
					   <div class="form-group">
					      <label for="thirdname" class="col-sm-2 control-label">课程描述</label>
					      <div class="col-sm-9">
					         <textarea class="form-control" rows="3" id="thirdname" name="description" placeholder="请输入课程的描述"><s:property value='description' /></textarea>
					      </div>
					   </div>
					   <div class="form-group">
					      <div class="col-sm-offset-2 col-sm-10">
					         <button type="submit" class="btn btn-warning btn-lg">更新课程</button>
					      </div>
					   </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>