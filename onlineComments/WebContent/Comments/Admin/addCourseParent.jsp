<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="pragma" content="no-cache"> 
<meta http-equiv="cache-control" content="no-cache"> 
<meta http-equiv="expires" content="0"> 
<title>新建课程</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
</head>
<body>
	<s:include value="header_one.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				
				<div class="page-header">
					<h3>
						课程添加
						<small>
							COURSE ADD
						</small>
					</h3>
				</div>
				
				<ol class="breadcrumb">
				  <li><a href="CourseParentAction_myCourseParents">当前课程</a></li>
				  <li class="active">新建课程</li>
				</ol>
				<div class="row">
					<form class="form-horizontal" role="form" action="CourseParentAction_newCourseParent" namespace="/" method="post">
					   <div class="form-group">
					      <label for="secondname" class="col-sm-2 control-label">课程编号</label>
					      <div class="col-sm-9">
					         <input type="text" class="form-control" id="secondname" name="code"
					            placeholder="请输入课程的编号">
					      </div>
					   </div>
					   <div class="form-group">
					      <label for="firstname" class="col-sm-2 control-label">课程名字</label>
					      <div class="col-sm-9">
					         <input type="text" class="form-control" id="firstname" name="name"
					            placeholder="请输入课程的名字">
					      </div>
					   </div>
					   <div class="form-group">
					      <label for="thirdname" class="col-sm-2 control-label">课程描述</label>
					      <div class="col-sm-9">
					         <textarea class="form-control" rows="3" id="thirdname" name="description" placeholder="请输入课程的描述"></textarea>
					      </div>
					   </div>
					   <s:actionerror/>
					   <div class="form-group">
					      <div class="col-sm-offset-2 col-sm-10">
					         <button type="submit" class="btn btn-warning btn-lg">添加课程</button>
					      </div>
					   </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>