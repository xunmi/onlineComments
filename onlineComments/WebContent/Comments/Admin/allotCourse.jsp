<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>分配课程</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
<script src="<s:url value='/Js/jquery.min.js' />"></script>
<script type="text/javascript">
	function collegeToTeacher(){
		var url = "AjaxLinkageAction_collegeToTeacher.action";
		data = {"cid":$("#ajax_one").val()};
		$.getJSON(url, data,function(data){
			var obj = eval( "(" + data + ")" );
			$('#ajax_t').empty();
			//$('#ajax_two').append("<option>---请选择教师---</option>");
			$.each(obj,function(i,list){
				var tr = $("<option value='"+list.id+"'>"+list.id+list.nickname+"</option>");
				$('#ajax_t').append(tr);
			});
		});	
	}
	
	function collegeToMajor(){
		var url = "AjaxLinkageAction_collegeToMajor.action";
		data = {"cid":$("#ajax_two").val()};
		$.getJSON(url, data,function(data){
			var obj = eval( "(" + data + ")" );
			$('#ajax_m').empty();
			$('#ajax_m').append("<option>---请选择系---</option>");
			$.each(obj,function(i,list){
				var tr = $("<option value='"+list.id+"'>"+list.name+"</option>");
				$('#ajax_m').append(tr);
			});
		});	
	}
	
	function majorToClassroom(){
		var url = "AjaxLinkageAction_majorToClassroom.action";
		data = {"cid":$("#ajax_m").val()};
		$.getJSON(url, data,function(data){
			var obj = eval( "(" + data + ")" );
			$('#ajax_cr').empty();
			//$('#ajax_two').append("<option>---请选择班级---</option>");
			$.each(obj,function(i,list){
				var tr = $("<option value='"+list.id+"'>"+list.name+"</option>");
				$('#ajax_cr').append(tr);
			});
		});	
	}

</script>
</head>
<body>
	<s:include value="header_one.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb diyol">
				  <li><a href="#">
				  	<s:if test="#session['user'] != null">
						<s:property value="#session['user'].nickname" />
					</s:if>
				  </a></li>
				  <li><a href="CourseAction_toMyAllotedCoursePage">我的课程</a></li>
				  <li class="active">课程分配</li>
				</ol>
				<div class="row">
					<form class="form-horizontal" role="form" action="CourseAction_doAlloteCourse" namespace="/" method="post">
						<div class="form-group">
					      <label class="col-sm-2 control-label">分配教师</label>
					      <div class="col-sm-2">
					         <select class="form-control term" onChange="collegeToTeacher()" id="ajax_one">
							  	<option>---请选择学院---</option>
							  	<s:iterator value="colleges">
							  	<option value="<s:property value="id" />"><s:property value="name" /></option>
							  	</s:iterator>
							 </select>
					      </div>
					      <div class="col-sm-2">
					         <select class="form-control term" id="ajax_t" name="tid" >
							  	<option>---请选择教师---</option>
							  	
							 </select>
					      </div>
					   </div>
					   <div class="form-group">
					      <label class="col-sm-2 control-label">分配班级</label>
					      <div class="col-sm-2">
					         <select class="form-control term" onChange="collegeToMajor()" id="ajax_two">
							  	<option>---请选择学院---</option>
							  	<s:iterator value="colleges">
							  	<option value="<s:property value="id" />"><s:property value="name" /></option>
							  	</s:iterator>
							 </select>
					      </div>
					      <div class="col-sm-2">
					         <select class="form-control term" onChange="majorToClassroom()" id="ajax_m">
							  	<option>---请选择系---</option>
							  	
							 </select>
					      </div>
					      <div class="col-sm-2">
					         <select class="form-control term" id="ajax_cr" name="crid" >
							  	<option>---请选择班级---</option>
							  	
							 </select>
					      </div>
					   </div>
					   <div class="form-group">
					   	  <label class="col-sm-2 control-label">分配课程</label>
					   	  <div class="col-sm-2">
					         <select class="form-control term" name="cpid">
							  	<option>---请选择课程---</option>
							  	<s:iterator value="courseparents">
							  	<option value="<s:property value="id" />"><s:property value="name" /></option>
							  	</s:iterator>
							 </select>
					      </div>
					   </div>
					   <div class="form-group">
					   	  <label class="col-sm-2 control-label">指定学期</label>
					   	  <div class="col-sm-2">
					         <select class="form-control term" name="semester">
							  	<option>---请选择学期---</option>
							  	<s:iterator value="terms">
							  	<option><s:property value="name" /></option>
							  	</s:iterator>
							 </select>
					      </div>
					   </div>
					   <div class="form-group">
					      <div class="col-sm-offset-2 col-sm-10">
					         <button type="submit" class="btn btn-warning btn-lg">确定</button>
					      </div>
					   </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>