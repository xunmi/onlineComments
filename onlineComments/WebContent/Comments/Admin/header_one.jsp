<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
	<nav class="navbar navbar-fixed-top navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a href="#" class="navbar-brand">在线批注系统</a>
			</div>

			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="CourseParentAction_myCourseParents">课程管理</a></li>
					<li><a href="UserAction_loadUsers.action">用户管理</a></li>
					<li><a href="RightAction_findAllRights.action">权限控制</a></li>
					<li><a href="BasisAction_collegeManage.action">基础配置</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<s:if test="#session['user'] != null">
					<li><a href="javascript:void(0)">欢迎：<s:property value="#session['user'].username" /></a></li>
					</s:if>
				</ul>
			</div>
		</div>
	</nav>