<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>我的课程</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
<script src="<s:url value='/Js/jquery-1.7.2.min.js' />"></script>
<script src="<s:url value='/Js/bootstrap.min.js' />"></script>
<script src="<s:url value='/Js/public.js' />"></script>
</head>
<body>
	<s:include value="header_one.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
				<div class="page-header">
					<h3>
						课程发布
						<small>
							COURSE RELEASE
						</small>
					</h3>
				</div>
			
				<ol class="breadcrumb">
				  <li class="active">当前课程</li>
				</ol>
				
				
				<a href="CourseParentAction_toAddCourseParentPage" class="btn btn-info">
		          <span class="glyphicon glyphicon-plus"></span> 添加课程
		        </a><br><br>
				<s:if test="myCourseParents.isEmpty() == true">目前您没有课程!</s:if >
				<s:else>
					<table id="sample-table-1" class="table table-striped table-hover">
		                <thead>
		                    <tr>
		                        <th class="center">
		                            <label>
		                                <input type="checkbox" level="1"/>
		                            </label>
		                        </th>
		                        <th>ID</th>
		                        <th>课程编号</th>
		                        <th>课程名字</th>
		                        <th>创建时间</th>
		                        <th>操作</th>
		                    </tr>
		                </thead>
		
		                <tbody>
		                	<s:iterator value="myCourseParents" status="i">
		                	<s:set var="cpId" value="id" />
		                    <tr>
		                        <td class="center">
		                            <label>
		                                <input type="checkbox" level="2"/>
		                            </label>
		                        </td>
		                        <!-- <td><s:property value="id" /></td> -->
		                        <td><s:property value="#i.index+1" /></td>
		                        <td><s:property value="code" /></td>
		                        <td><s:property value="name" /></td>
		                        <td><s:date name="createTime" format="yy/MM/dd HH:mm" /></td>
		                        <td>
	                                <a title="修改" class="btn btn-info btn-sm" href="CourseParentAction_toUpdateCourseParentPage?cpid=<s:property value="#cpId" />">
	                                    <span class="glyphicon glyphicon-pencil"></span>
	                                </a>
	                                <a title="删除" class="btn btn-danger btn-sm" href="CourseParentAction_doDeleteCourseParent?cpid=<s:property value="#cpId" />">
	                                    <span class="glyphicon glyphicon-trash"></span>
	                                </a>
		                        </td>
		                    </tr>
		                    </s:iterator>
		                </tbody>
		            </table>
				</s:else>
			</div>
		</div>
	</div>
</body>
</html>