<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/userManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						教师管理
						<small>
							TEACHER MANAGE
						</small>
					</h3>
				</div>
				<h4>批量添加教师</h4>
				<hr>
				
				<form class="form-inline" action="FileAction_batchUploadTeachers.action" method="post" enctype="multipart/form-data">
				   <div class="form-group">
				      <s:file name="uploadFile" label="文件位置" size="80"/>
				   </div>
				   <button type="submit" class="btn btn-primary">提交</button>
				</form>
				
				<br>
				
				<h3 style="color:red"><b>注意:</b></h3>
				<label for="" style="font-size:20px;">1,请按照指定.csv文件的模板添加用户，<a href="FileAction_downloadUserTemplate.action?fileName=UserTemplate.xls" style="color:#337AC4;text-decoration:underline;">模板下载地址</a></label><br>
				<label for="" style="font-size:20px;">2,模板填写示例</label>
				<hr>
				<h4>教师分配</h4>
				<hr>
				<s:if test="undistributedTeachers.isEmpty() == true">目前还没有任何教师</s:if>
				<s:else>
				<table class="table table-striped table-hover">
					<tr>
						<th><input type="checkbox"></th>
						<th>用户名</th>
						<th>性别</th>
						<th>昵称</th>
						<th>操作</th>
					</tr>
					<s:iterator value="undistributedTeachers">
					<tr>
						<td><input type="checkbox"></td>
						<td><s:property value="username" /></td>
						<td><s:property value="sex" /></td>
						<td><s:property value="nickname" /></td>
						<td>
							<a href="UserAction_distributeTeacher?uid=<s:property value="id" />" class="btn btn-success btn-xs">分配学院</span></a>
							<a href="#" class="btn btn-danger btn-xs disabled"><span class="glyphicon glyphicon-trash"></span></a>
						</td>
					</tr>
					</s:iterator>
				</table>	
				</s:else>
				
				<hr>
				<h4>教师查询</h4>
				<hr>
				<s:if test="users.isEmpty() == true">目前还没有任何教师</s:if>
				<s:else>
				<table class="table table-striped table-hover">
					<tr>
						<th><input type="checkbox"></th>
						<th>用户名</th>
						<th>性别</th>
						<th>昵称</th>
						<th>操作</th>
					</tr>
					<s:iterator value="users">
					<tr>
						<td><input type="checkbox"></td>
						<td><s:property value="username" /></td>
						<td><s:property value="sex" /></td>
						<td><s:property value="nickname" /></td>
						<td>
							<a href="#" class="btn btn-success btn-xs disabled"><span class="glyphicon glyphicon-pencil"></span></a>
							<a href="#" class="btn btn-danger btn-xs disabled"><span class="glyphicon glyphicon-trash"></span></a>
						</td>
					</tr>
					</s:iterator>
				</table>	
				</s:else>

			</div>
		</div>
	</div>
</body>
</html>