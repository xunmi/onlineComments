<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/userManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						分配教师
						<small>
							DISTRIBUTE TEACHER
						</small>
					</h3>
				</div>
				
				<ol class="breadcrumb">
				  <li><a href="UserAction_loadTeachers.action">教师管理</a></li>
				  <li class="active">分配教师</li>
				</ol>

				<form class="form-horizontal" action="UserAction_doDistributeTeacher.action" method="post">
				   <div class="form-group">
				      <label for="firstname" class="col-sm-2 control-label">当前教师</label>
				      <div class="col-sm-9">
				         <label for="firstname" class="col-sm-2 control-label"><s:property value="username" /></label>
				      </div>
				   </div>
				   <div class="form-group">
				   		<label for="lastname" class="col-sm-2 control-label">选择学院</label>
				   		<div class="col-sm-2">
					      	<select name="cid" class="form-control">
					      		<s:iterator value="colleges">
					      			<option value="<s:property value="id" />"><s:property value="name" /></option>
					      		</s:iterator>
					      	</select>
				      	</div>			    
				  </div>
				   <div class="form-group">
				      <div class="col-sm-offset-2 col-sm-10">
				      <input type="hidden" name="uid" value='<s:property value="id" />' />
				         <button type="submit" class="btn btn-warning">确认分配</button>
				      </div>
				   </div>
				</form>

			</div>
		</div>
	</div>
</body>
</html>