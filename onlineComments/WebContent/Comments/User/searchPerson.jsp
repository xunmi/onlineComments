<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/userManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						学生查询
						<small>
							VIEW THE STUDENT
						</small>
					</h3>
				</div>
				
				<ol class="breadcrumb">
				  <li><a href="UserAction_loadUsers.action">学生管理</a></li>
				  <li class="active"><s:property value="classroom.name" /></li>
				</ol>
				
				<script type="text/javascript">
					$(document).ready(function(){
						
						//初始化select
						var time = new Date();
						var nowYear = time.getFullYear();
						for(var i=nowYear-5;i<nowYear+5;i++){
							$("#semester").append("<option value='"+i+"-"+(i+1)+"'>"+i+"-"+(i+1)+"</option>");
						}
						
						//点击学院，联动专业
						$("#college").change(function(){
							//每次清楚之前的选择
							$("#major option").remove();
							$("#classroom option").remove();
							
							var target = $(this).children('option:selected').val();
							var url = "AjaxJsonAction_findAllMajorByCid.action";
							var params = {
									"cid":target,
							};
							$.getJSON(url, params, function(data){
								var info = eval("(" + data +")");
								$("#major").append("<option>请选择</option>");
								$("#classroom").append("<option>请选择</option>");
								if(data == "{}"){
									$("#major").append("<option>无班级</option>");
								}else{
									for(var i in info){
										$("#major").append("<option value='"+i+"'>"+info[i]+"</option>");
									}
								}
							});
						});
						
						//点击专业，依据学期和专业联动班级
						$("#major").change(function(){
							//每次清楚之前的选择
							$("#classroom option").remove();
							
							var targetMid = $(this).children('option:selected').val();
							var targetSemester = $("#semester").children('option:selected').val();
							var url = "AjaxJsonAction_findAllClassroomByMidAndSemester.action";
							var params = {
									"mid":targetMid,
									"semester":targetSemester,
							};
							$.getJSON(url, params, function(data){
								var info = eval("(" + data +")");
								$("#classroom").append("<option>请选择</option>");
								if(data == "{}"){
									$("#classroom").append("<option>无班级</option>");
								}else{
									for(var i in info){
										$("#classroom").append("<option value='"+i+"'>"+info[i]+"</option>");
									}
								}
							});
						});
					});
				</script>

				<form action="UserAction_searchPerson.action" method="post">
				<div class="row">
					<div class="col-md-2">
						<label>用户类型</label>
						<select name="type" class="form-control">
							<option>请选择</option>
							<option value="2">学生</option>
						</select>
					</div>
					<div class="col-md-2">
						<label>学期</label>
						<select id="semester" class="form-control">
							<option>请选择</option>
						</select>
					</div>
					<div class="col-md-2">
						<label>学院</label>
						<select id="college" class="form-control">
							<option>请选择</option>
							<s:iterator value="colleges">
								<option value='<s:property value="id" />'><s:property value="name" /></option>
							</s:iterator>
						</select>
					</div>
					<div class="col-md-2">
						<label>专业</label>
						<select id="major" class="form-control">
							<option>请选择</option>	
						</select>
					</div>
					<div class="col-md-2">
						<label>班级</label>
						<select name="classId" id="classroom" class="form-control">
							<option>请选择</option>
						</select>
					</div>
					<div class="col-md-1">
						<label></label>
						<input class="btn btn-primary" type="submit" value="查询" style="margin-top:4px;"/>
					</div>
				</div>
				</form>
				<hr>

				<s:if test="users.isEmpty() == true">目前没有任何用户！</s:if>
				<s:else>
				<table class="table table-striped table-hover">
					<tr>
						<th><input type="checkbox"></th>
						<th>用户名</th>
						<th>密码</th>
						<th>性别</th>
						<th>别名</th>
						<th>激活日期</th>
						<th>操作</th>
					</tr>
					<s:iterator value="users">
					<tr>
						<td><input type="checkbox"></td>
						<td><s:property value="username" /></td>
						<td><s:property value="password" /></td>
						<td><s:property value="sex" /></td>
						<td><s:property value="nickname" /></td>
						<td><s:property value="regdate" /></td>
						<td>
							<a href="UserAction_editUser.action?classId=<s:property value="classId" />&uid=<s:property value="id" />" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
							<a href="UserAction_deleteUser.action?uid=<s:property value="id" />" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>
						</td>
					</tr>
					</s:iterator>
				</table>
				</s:else>
				<hr>
				<s:if test="type == '1'">
					<a href="UserAction_addUser.action?classId=<s:property value="classId" />&type=<s:property value="type" />" class="btn btn-warning">添加一个教师</a>
				</s:if>
				<s:elseif test="type == '2'">
					<a href="UserAction_addUser.action?classId=<s:property value="classId" />&type=<s:property value="type" />" class="btn btn-warning">添加一个学生</a>
				</s:elseif>
				<hr>
				<h4>批量添加学生</h4>
				<hr>
				
				<form class="form-inline" action="FileAction_batchUploadStudents.action?classId=<s:property value="classId" />&type=<s:property value="type" />" method="post" enctype="multipart/form-data">
				   <div class="form-group">
				      <s:file name="uploadFile" label="文件位置" size="80"/>
				   </div>
				   <button type="submit" class="btn btn-primary">提交</button>
				</form>
				
				<br>
				
				<h3 style="color:red"><b>注意:</b></h3>
				<label for="" style="font-size:20px;">1,请按照指定Excel文件的模板添加用户，<a href="FileAction_downloadUserTemplate.action?fileName=UserTemplate.xls" style="color:#337AC4;text-decoration:underline;">模板下载地址</a></label><br>
				<label for="" style="font-size:20px;">2,模板填写示例</label>
				

			</div>
		</div>
	</div>
</body>
</html>