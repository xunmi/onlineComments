<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/userManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						学生管理
						<small>
							STUDENT MANAGE
						</small>
					</h3>
				</div>
				
				<ol class="breadcrumb">
					<li class="active">学生管理</li>
				</ol>
				
				<script type="text/javascript">
					$(document).ready(function(){
						
						//初始化select
						var time = new Date();
						var nowYear = time.getFullYear();
						for(var i=nowYear-5;i<nowYear+5;i++){
							$("#semester").append("<option value='"+i+"-"+(i+1)+"'>"+i+"-"+(i+1)+"</option>");
						}
						
						//点击学院，联动专业
						$("#college").change(function(){
							//每次清楚之前的选择
							$("#major option").remove();
							$("#classroom option").remove();
							
							var target = $(this).children('option:selected').val();
							var url = "AjaxJsonAction_findAllMajorByCid.action";
							var params = {
									"cid":target,
							};
							$.getJSON(url, params, function(data){
								var info = eval("(" + data +")");
								$("#major").append("<option>请选择</option>");
								$("#classroom").append("<option>请选择</option>");
								if(data == "{}"){
									$("#major").append("<option>无班级</option>");
								}else{
									for(var i in info){
										$("#major").append("<option value='"+i+"'>"+info[i]+"</option>");
									}
								}
							});
						});
						
						//点击专业，依据学期和专业联动班级
						$("#major").change(function(){
							//每次清楚之前的选择
							$("#classroom option").remove();
							
							var targetMid = $(this).children('option:selected').val();
							var targetSemester = $("#semester").children('option:selected').val();
							var url = "AjaxJsonAction_findAllClassroomByMidAndSemester.action";
							var params = {
									"mid":targetMid,
									"semester":targetSemester,
							};
							$.getJSON(url, params, function(data){
								var info = eval("(" + data +")");
								$("#classroom").append("<option>请选择</option>");
								if(data == "{}"){
									$("#classroom").append("<option>无班级</option>");
								}else{
									for(var i in info){
										$("#classroom").append("<option value='"+i+"'>"+info[i]+"</option>");
									}
								}
							});
						});
					});
				</script>

				<form action="UserAction_searchPerson.action" method="post">
				<div class="row">
					<div class="col-md-2">
						<label>用户类型</label>
						<select name="type" class="form-control">
							<option>请选择</option>
							<option value="2">学生</option>
						</select>
					</div>
					<div class="col-md-2">
						<label>学期</label>
						<select id="semester" class="form-control">
							<option>请选择</option>
						</select>
					</div>
					<div class="col-md-2">
						<label>学院</label>
						<select id="college" class="form-control">
							<option>请选择</option>
							<s:iterator value="colleges">
								<option value='<s:property value="id" />'><s:property value="name" /></option>
							</s:iterator>
						</select>
					</div>
					<div class="col-md-2">
						<label>专业</label>
						<select id="major" class="form-control">
							<option>请选择</option>	
						</select>
					</div>
					<div class="col-md-2">
						<label>班级</label>
						<select name="classId" id="classroom" class="form-control">
							<option>请选择</option>
						</select>
					</div>
					<div class="col-md-1">
						<label></label>
						<input class="btn btn-primary" type="submit" value="查询" style="margin-top:4px;"/>
					</div>
				</div>
				</form>
				<hr>
	

			</div>
		</div>
	</div>
</body>
</html>