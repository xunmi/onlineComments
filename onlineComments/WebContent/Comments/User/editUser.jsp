<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/userManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						学生更新
						<small>
							STUDENT UPDATE
						</small>
					</h3>
				</div>
				
				<script type="text/javascript">
					$(document).ready(function(){
						var time = new Date();
						var nowYear = time.getFullYear();
						for(var i=nowYear-5;i<nowYear+5;i++){
							$("#startTime").append("<option value='"+i+"'>"+i+"</option>");
						}
					});
				</script>
				
				<ol class="breadcrumb">
				  <li><a href="UserAction_loadUsers.action">学生管理</a></li>
				  <li><a href="UserAction_searchPerson.action?classId=<s:property value="classId" />&type=<s:property value="type" />"><s:property value="classroom.name" /></a></li>
				  <li class="active">更新学生信息</li>
				</ol>

				<form class="form-horizontal" action="UserAction_updateUser.action" method="post">
				   <div class="form-group">
				      <label for="firstname" class="col-sm-2 control-label">用户名</label>
				      <div class="col-sm-9">
				         <input type="text" name="username" class="form-control" id="firstname" 
				            placeholder="请输入用户名" value='<s:property value="username" />'>
				      </div>
				   </div>
				   <div class="form-group">
				   		<label for="lastname" class="col-sm-2 control-label">昵称</label>
				      <div class="col-sm-9">
				         <input type="text" name="nickname" class="form-control" id="lastname" 
				            placeholder="请输入昵称" value='<s:property value="nickname" />'>
				      </div>				    
				  </div>
				   <div class="form-group">
				   		<label for="lastname" class="col-sm-2 control-label">性别</label>
				   		<div class="col-sm-2">
					      	<select name="sex" class="form-control">
						      	<s:if test="sex == '男'">
						      		<option value="男">男</option>
						      	</s:if>
						      	<s:elseif test="sex == '女'">
						      		<option value="女">女</option>
						      	</s:elseif>
					      	</select>
				      	</div>			    
				  </div>
				   <div class="form-group">
				      <div class="col-sm-offset-2 col-sm-10">
				      	<input type="hidden" name="password" value="<s:property value="password" />" />
				      	<input type="hidden" name="type" value="<s:property value="type" />" />
				      	<input type="hidden" name="classroom.id" value="<s:property value="classroom.id" />" />
				      	<input type="hidden" name="id" value="<s:property value="uid" />" />
				         <button type="submit" class="btn btn-warning">确认添加</button>
				      </div>
				   </div>
				</form>
				<h2><s:actionerror></s:actionerror></h2>

			</div>
		</div>
	</div>
</body>
</html>