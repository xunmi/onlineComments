<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/rightManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						编辑用户角色
						<small>
							EDIT USER ROLES
						</small>
					</h3>
				</div>
				
				<ol class="breadcrumb">
					<li><a href="UserAuthorizeAction_findAllUsers.action">用户授权</a></li>
					<li class="active">编辑用户角色</li>
				</ol>
				
				<script type="text/javascript">
					$(function(){
						$('#one1').click(function(){
							var size = $('#left>option:selected').size();
							if(size != 0){
								$('#left > option:selected').appendTo($('#right'));
							}
							else{
								$('#left>option:first-child').appendTo($('#right'));
							}
						});	
						$('#all1').click(function(){
							$('#left > option').appendTo($('#right'));
						});	
						$('#one2').click(function(){
							var size = $('#right>option:selected').size();
							if(size != 0){
								$('#right > option:selected').appendTo($('#left'));
							}
							else{
								$('#right>option:first-child').appendTo($('#left'));
							}
						});	
						$('#all2').click(function(){
							$('#right > option').appendTo($('#left'));
						});	
					});
					
					function submitForm1(){
						$('#left > option').attr('selected','selected');
						return true ;
					}
				</script>
				
				<s:form cssClass="form-horizontal" action="UserAuthorizeAction_updateAuthorize" namespace="/" method="post">
					<s:hidden name="id" />
					
					<div class="form-group">
				      <label class="col-sm-4 control-label">ID:</label>
				      <div class="col-sm-8">
				      		<label class="control-label"><s:property value="username" /></label>
				        	<input type="hidden" name="username" value='<s:property value="username" />' />
				      </div>
				  	</div>
				  	<div class="form-group">
				      <label class="col-sm-4 control-label">昵称:</label>
				      <div class="col-sm-8">
				      		<label class="control-label"><s:property value="nickname" /></label>
				         	<input type="hidden" name="nickname" value='<s:property value="nickname" />' />
				      </div>
				   	</div>
				   	<br>
					
					<table>
						<tr>
							<td width="65%" align="right">
								<s:select id="left" 
									name="ownRoleIds" 
									list="roles" 
									listKey="id" 
									listValue="roleName"
									multiple="true"
									size="15"
									cssStyle="width:150px">
								</s:select>
							</td>
							<td width="10%" valign="middle" align="center">
								<input type="button" id="one1" value="&gt;" class="btn btn-primary"></input><br><br>
								<input type="button" id="one2" value="&lt;" class="btn btn-primary"></input><br><br>
								<input type="button" id="all1" value="&gt;&gt;" class="btn btn-primary"></input><br><br>
								<input type="button" id="all2" value="&lt;&lt;" class="btn btn-primary"></input><br><br>
							</td>
							<td width="45%" align="left">
								<s:select id="right" 
									list="noOwnRoles" 
									name="noOwnRoleIds"
									listKey="id" 
									listValue="roleName"
									multiple="true"
									size="15"
									cssStyle="width:150px">
								</s:select>
							</td>
						</tr>
					</table>
					<br><br>
					
					<div class="form-group">
				      <div class="col-sm-offset-3 col-sm-8">
				      	<s:submit value="确定编辑" cssClass="btn btn-warning" onclick="javascript:return submitForm1()"/>
				      </div>
				   	</div>
					
					
					
					</s:form>
				
			</div>
		</div>
	</div>
</body>
</html>