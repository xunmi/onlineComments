<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/rightManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						用户授权
						<small>
							USER AUTHORISE
						</small>
					</h3>
				</div>
				
				<ol class="breadcrumb">
					<li class="active">用户授权</li>
				</ol>
				
				<s:if test="allUsers.isEmpty() == true">目前没有任何用户!</s:if >
				<s:else>
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<td>序号</td>
								<td>ID</td>
								<td>性别</td>
								<td>昵称</td>
								<td>修改授权</td>
								<td>清除授权</td>
							</tr>
						</thead>
						<tbody>
							<s:iterator value="allUsers" status="st">
								<s:set var="userId" value="id" />
								<tr>
									<td><s:property value="#st.count" /></td>
									<td><s:property value="username" /></td>
									<td><s:property value="sex" /></td>
									<td><s:property value="nickname" /></td>
									<td><s:a action="UserAuthorizeAction_editAuthorize?userId=%{#userId}" cssClass="aList">修改授权</s:a></td>
									<td><s:a action="UserAuthorizeAction_clearAuthorize?userId=%{#userId}" cssClass="aList">清除授权</s:a></td>
								</tr>
							</s:iterator>
						</tbody>
					</table>
				</s:else>

			</div>
		</div>
	</div>
</body>
</html>