<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>

<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/userManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						添加用户
						<small>
							ADD USERS
						</small>
					</h3>
				</div>
				
				<form class="form-inline" action="FileAction_doUploadFile.action" method="post" enctype="multipart/form-data">
				   <div class="form-group">
				      <s:file name="uploadFile" label="文件位置" size="80"/>
				   </div>
				   <button type="submit" class="btn btn-primary">提交</button>
				</form>
				
				<br>
				
				<h3 style="color:red"><b>注意:</b></h3>
				<label for="" style="font-size:20px;">1,请按照指定.csv文件的模板添加用户，<a href="FileAction_fileDownload.action?fileName=test.xls" style="color:#337AC4;text-decoration:underline;">模板下载地址</a></label><br>
				<label for="" style="font-size:20px;">2,模板填写示例</label>

			</div>
		</div>
	</div>
</body>
</html>