<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>test</title>
	<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
	<style>
		body{
			margin: 0px;
			padding: 0px;
		}
		.bg{
			position: fixed; 
			top: 0; 
			left: 0; 
			bottom: 0; 
			right: 0; 
			z-index: -1;
			opacity: 1;
		}
		.bg img { 
			height: 100%; 
			width: 100%; 
			border: 0; 
		} 
		[class*="col-md-4"]{
			padding-top: 10px;
			padding-bottom: 10px;
			min-height: 100px;
			color: #C1D5EC;
			text-align: center;
			border-radius: 10px;
			background-color: rgba(255, 255, 255, 0.4); 
			/*opacity: 1;*/
	    }
	   .glyphicon{
			font-size: 30px;
	    }
	    .col-sm-1{
	    	padding-left: 10px;
	    	text-align: center;
	    }
	    .col-sm-3{
	    	padding-top: 10px;
	    	padding-left: 0px;
	    	/*background: black;*/
	    	text-align: left;
	    }
	    .row{
			padding-top: 200px;
	    }
	</style>
</head>
<body>
	<div class="bg"><img src="Images/login_bg.jpg"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<h2>Welcome!</h2><br>
				<s:actionerror></s:actionerror>
				<form role="form" class="form-horizontal" action="LoginAction_doLogin" namespace="/" method="post">
					<div class="form-group">
						<lable class="col-sm-1">
							<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
						</lable>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="username" placeholder="username">
						</div>
					</div>
					<div class="form-group">
						<lable class="col-sm-1">
							<span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
						</lable>
						<div class="col-sm-10">
							<input type="password" class="form-control" name="password" placeholder="password">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4 type">
							<input type="radio" name="usertype" value="1">管理员</input>
						</div>
						<div class="col-sm-4 type">
							<input type="radio" name="usertype" value="2">教师</input>
						</div>
						<div class="col-sm-4 type">
							<input type="radio" name="usertype" value="3">学生</input>
						</div>
					</div>
					<!-- 验证码 -->
					<div class="form-group">
						<lable class="col-sm-1">
							<span class="glyphicon glyphicon-picture" aria-hidden="true"></span>
						</lable>
						<div class="col-sm-4 verify">
							<input type="text" name="verify" class="form-control">
						</div>
						<div class="col-sm-5">
							<img src="" id="code">
							<a href="">看不清</a>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4 col-md-offset-4">
							<input type="submit" class="btn btn-primary" id="button" value="log in">
						</div>
					</div>
					

				</form>
			</div>
	    </div>
	</div>
</body>
</html>