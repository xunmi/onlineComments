<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/rightManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						角色添加/更新
						<small>
							ROLE ADD OR UPDATE
						</small>
					</h3>
				</div>
				
				<ol class="breadcrumb">
					<li><a href="RoleAction_findAllRoles.action">角色管理</a></li>
					<li class="active">角色添加/更新</li>
				</ol>
				
				<script type="text/javascript">
					$(function(){
						$('#one1').click(function(){
							var size = $('#left>option:selected').size();
							if(size != 0){
								$('#left > option:selected').appendTo($('#right'));
							}
							else{
								$('#left>option:first-child').appendTo($('#right'));
							}
						});	
						$('#all1').click(function(){
							$('#left > option').appendTo($('#right'));
						});	
						$('#one2').click(function(){
							var size = $('#right>option:selected').size();
							if(size != 0){
								$('#right > option:selected').appendTo($('#left'));
							}
							else{
								$('#right>option:first-child').appendTo($('#left'));
							}
						});	
						$('#all2').click(function(){
							$('#right > option').appendTo($('#left'));
						});	
					});
					
					function submitForm1(){
						$('#left > option').attr('selected','selected');
						return true ;
					}
				</script>
				
				<s:form cssClass="form-horizontal" id="form1" action="RoleAction_saveOrUpdateRole" method="post">
					<s:hidden name="id" />
					
					<div class="form-group">
				      <label class="col-sm-2 control-label">角色名称</label>
				      <div class="col-sm-9">
				         <input type="text" name="roleName" class="form-control" value='<s:property value="roleName" />' placeholder="请输入角色名称">
				      </div>
				   </div>
				   <div class="form-group">
				      <label class="col-sm-2 control-label">角色值</label>
				      <div class="col-sm-2">
				         <input type="text" name="roleValue" class="form-control" value='<s:property value="roleValue" />' placeholder="请输入角色值">
				      </div>
				   </div>
				   <div class="form-group">
				   		<label class="col-sm-2 control-label">角色描述</label>
				      <div class="col-sm-9">
				         <textarea class="form-control" name="roleDesc" rows="3" placeholder="请输入描述"><s:property value="roleDesc" /></textarea>
				      </div>				    
				  </div>
					
					<table>
						<tr>
							<td width="65%" align="right">
								<s:select id="left" 
									name="ownRightIds" 
									list="rights" 
									listKey="id" 
									listValue="rightName"
									multiple="true"
									size="15"
									cssStyle="width:150px">
								</s:select>
							</td>
							<td width="10%" valign="middle" align="center">
								<input class="btn btn-primary" type="button" id="one1" value="&gt;" class="btn"></input><br><br>
								<input class="btn btn-primary" type="button" id="one2" value="&lt;" class="btn"></input><br><br>
								<input class="btn btn-primary" type="button" id="all1" value="&gt;&gt;" class="btn"></input><br><br>
								<input class="btn btn-primary" type="button" id="all2" value="&lt;&lt;" class="btn"></input><br><br>
							</td>
							<td width="45%" align="left">
								<s:select id="right" 
									list="noOwnRights" 
									name="noOwnRightIds"
									listKey="id" 
									listValue="rightName"
									multiple="true"
									size="15"
									cssStyle="width:150px">
								</s:select>
							</td>
						</tr>
					</table>
					<br><br>
					
					<div class="form-group">
				      <div class="col-sm-offset-2 col-sm-10">
						<s:submit value="确定更改" cssClass="btn btn-warning" onclick='return submitForm1()'/>
				      </div>				    
				  	</div>					
					
					</s:form>
				
			</div>
		</div>
	</div>
</body>
</html>