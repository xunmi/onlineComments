<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:include value="/header.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="/rightManageSideNav.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="page-header">
					<h3>
						角色管理
						<small>
							ROLE MANAGE
						</small>
					</h3>
				</div>
				
				<ol class="breadcrumb">
					<li class="active">角色管理</li>
				</ol>
				
				<a class="btn btn-success" href="RoleAction_toAddRolePage.action">添加角色</a>
				<br><br>
				
				<s:if test="allRoles.isEmpty() == true">目前没有任何角色!</s:if >
				<s:else>
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<td>序号</td>
								<td>ID</td>
								<td>角色名称</td>
								<td>修改</td>
								<td>删除</td>
							</tr>
						</thead>
						<tbody>
							<s:iterator value="allRoles" status="st">
								<s:set var="roleId" value="id" />
								<tr>
									<td><s:property value="#st.count" /></td>
									<td><s:property value="id" /></td>
									<td><s:property value="roleName" /></td>
									<td><s:a action="RoleAction_editRole?roleId=%{#roleId}" cssClass="btn btn-warning btn-xs">修改</s:a></td>
									<td><s:a action="RoleAction_deleteRole?roleId=%{#roleId}" cssClass="btn btn-danger btn-xs">删除</s:a></td>
								</tr>
							</s:iterator>
						</tbody>
					</table>
				</s:else>

			</div>
		</div>
	</div>
</body>
</html>