<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>更新课程</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
</head>
<body>
	<s:include value="header_two.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course_work.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb diyol">
				  <li><a href="#">
				  	<s:if test="#session['user'] != null">
						<s:property value="#session['user'].nickname" />
					</s:if>
				  </a></li>
				  <li><a href="CourseAction_myCourses"><s:property value="name" /></a></li>
				  <li class="active">更新课程</li>
				</ol>
				<div class="row">
					<form class="form-horizontal" role="form" action="CourseAction_doUpdateCourse" namespace="/" method="post">
					   <s:hidden name="id" />
					   <div class="form-group">
					      <label for="firstname" class="col-sm-2 control-label">课程名字</label>
					      <div class="col-sm-9">
					         <input type="text" class="form-control" id="firstname" name="name"
					            placeholder="请输入课程的名字" value="<s:property value='name' />">
					      </div>
					   </div>
					   <div class="form-group">
					      <label for="secondname" class="col-sm-2 control-label">课程编号</label>
					      <div class="col-sm-9">
					         <input type="text" class="form-control" id="secondname" name="code"
					            placeholder="请输入课程的编号" value="<s:property value='code' />">
					      </div>
					   </div>
					   <div class="form-group">
					      <label for="thirdname" class="col-sm-2 control-label">课程描述</label>
					      <div class="col-sm-9">
					         <textarea class="form-control" rows="3" id="thirdname" name="description" placeholder="请输入课程的描述"><s:property value='description' /></textarea>
					      </div>
					   </div>
					   <div class="form-group">
					      <label for="lastname" class="col-sm-2 control-label">所属学期</label>
					      <div class="col-sm-6">
					         <select class="form-control term" name="semester">
							  	<option>---请选择学期---</option>
							  	<s:if test=" semester == '2015上学期' ">
							  		<option selected="seclected">2015上学期</option>
							  		<option>2015下学期</option>
								  	<option>2016上学期</option>
								  	<option>2016下学期</option>
								</s:if>
							  	<s:elseif test=" semester == '2015下学期' ">
							  		<option>2015上学期</option>
							  		<option selected="seclected">2015下学期</option>
								  	<option>2016上学期</option>
								  	<option>2016下学期</option>
							  	</s:elseif>
							  	<s:elseif test=" semester == '2016上学期' ">
							  		<option>2015上学期</option>
							  		<option>2015下学期</option>
								  	<option selected="seclected">2016上学期</option>
								  	<option>2016下学期</option>
							  	</s:elseif>
							  	<s:elseif test=" semester == '2016下学期' ">
							  		<option>2015上学期</option>
							  		<option>2015下学期</option>
								  	<option>2016上学期</option>
								  	<option selected="seclected">2016下学期</option>
							  	</s:elseif>
							  		
							 </select>
					      </div>
					   </div>
					   <div class="form-group">
					      <div class="col-sm-offset-2 col-sm-10">
					         <button type="submit" class="btn btn-warning btn-lg">更新课程</button>
					      </div>
					   </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>