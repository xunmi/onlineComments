<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新建课程</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">

<script type="text/javascript">
	window.UEDITOR_HOME_URL = 'UEditor/';
	window.onload = function() {
		UE.getEditor('title',{
			initialFrameHeight: 500
		});
	}
</script>
<script type="text/javascript" charset="utf-8" src="UEditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="UEditor/ueditor.all.min.js"> </script>
</head>
<body>
	<s:include value="header_two.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course_work.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
				<div class="page-header">
					<h3>
						问题设计
						<small>
							QUESTION DESIGN
						</small>
					</h3>
				</div>
			
				<ol class="breadcrumb">
				  <li><a href="CourseAction_myCourses"><s:property value="page.test.course.name" /></a></li>
				  <li><a href="TestAction_myTests?cid=<s:property value="cid" />"><s:property value="page.test.title" /></a></li>
				  <li><a href="TestAction_toDesignTest?tid=<s:property value='tid' />&cid=<s:property value='cid' />"><s:property value="page.name" /></a></li>
				  <li><a href="PageAction_toDesignPage?pid=<s:property value="pid" />&tid=<s:property value="tid" />&cid=<s:property value="cid" />">设计页面</a></li>
				  <li class="active">设计问题</li>
				</ol>
				
					<form role="form" action="QuestionAction_saveOrUpdateQuestion" namespace="/" method="post">
					   <s:hidden name="id" />
					   <s:hidden name="questionType" />
					   <s:hidden name="pid" />
					   <s:hidden name="cid" />
					   <s:hidden name="tid" />
					   <div class="form-group">
					      <label>问题内容</label>
					      <div>
					         <textarea id="title" name="title"><s:property value="title" /></textarea>
					      </div>
					   </div>
					   <!-- 
					   <div class="form-group">
					      <label class="col-sm-2 control-label">问题类型</label>
					      <div class="col-sm-9">
					         <input type="text" class="form-control" name="questionType" value='<s:property value="questionType" />'/>
					      </div>
					    -->
					   <div class="form-group">
					      <button type="submit" class="btn btn-success">确定发布</button>
					   </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>