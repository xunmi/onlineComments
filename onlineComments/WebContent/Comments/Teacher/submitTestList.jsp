<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>我的课程</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
<script src="<s:url value='/Js/jquery-1.7.2.min.js' />"></script>
<script src="<s:url value='/Js/public.js' />"></script>
<style type="text/css">
	.ace{
		z-index: 12;
		width: 18px;
		height: 18px;
		cursor: pointer;
	}
	.center{
		vertical-align: middle;
		text-align: center;
	}
</style>
</head>
<body>
	<s:include value="header_two.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course_work.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
				<div class="page-header">
					<h3>
						已提交作业
						<small>
							HOMEWORK HAS SUBMITED
						</small>
					</h3>
				</div>
			
				<ol class="breadcrumb">
				  <li><a href="CourseAction_myCourses"><s:property value="course.name" /></a></li>
				  <li class="active">已提交的作业</li>
				</ol>
				
				<s:if test="myUsers.isEmpty() == true">目前该实验报告没有任何人提交!</s:if >
				<s:else>
					<table id="sample-table-1" class="table table-striped table-hover">
		                <thead>
		                    <tr>
		                        <th class="center">
		                            <label>
		                                <input type="checkbox" level="1"/>
		                            </label>
		                        </th>
		                        <th>ID</th>
		                        <th>作业名称</th>
		                        <th>学生</th>
		                        <th>提交时间</th>
		                        <th>操作</th>
		                    </tr>
		                </thead>
		
		                <tbody>
		                	<s:iterator value="myUsers" status="i">
		                	<s:set var="uId" value="id" />
		                    <tr>
		                        <td class="center">
		                            <label>
		                                <input type="checkbox" level="2"/>
		                            </label>
		                        </td>
		                        <!-- <td><s:property value="id" /></td> -->
		                        <td><s:property value="#i.index+1" /></td>
		                        <td><s:property value="title" /></td>
		                        <td><s:property value="nickname" /></td>
		                        <td><s:property value="submitTime.get(#i.index)" /></td>
		                        <!-- <td><s:date name="createTime" format="yy/MM/dd HH:mm" /></td> -->
		                        <td>
	                                <a class="btn btn-info btn-xs" href="PageAction_toEndorseStudentPage?cid=<s:property value='cid' />&tid=<s:property value='tid' />&uid=<s:property value='#uId' />" title="批阅实验">
	                                    <span class="glyphicon glyphicon-check"></span>
	                                </a>
		                        </td>
		                    </tr>
		                    </s:iterator>
		                </tbody>
		            </table>
				</s:else>
			</div>
		</div>
	</div>
</body>
</html>