<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>我的课程</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
<script src="<s:url value='/Js/jquery-1.7.2.min.js' />"></script>
<script src="<s:url value='/Js/public.js' />"></script>
</head>
<body>
	<s:include value="header_two.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course_work.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
				<div class="page-header">
					<h3>
						作业列表
						<small>
							HOMEWORK LISTS
						</small>
					</h3>
				</div>
			
				<ol class="breadcrumb">
				  <li><a href="CourseAction_myCourses"><s:property value="course.name" /></a></li>
				  <li class="active">我发布的作业</li>
				</ol>
				
				<s:if test="myTests.isEmpty() == true">目前该课程没有任何实验报告!</s:if >
				<s:else>
					<table id="sample-table-1" class="table table-striped table-hover">
		                <thead>
		                    <tr>
		                        <th class="center">
		                            <label>
		                                <input type="checkbox" level="1"/>
		                            </label>
		                        </th>
		                        <th>ID</th>
		                        <th>作业名称</th>
		                        <th>创建时间</th>
		                        <th>操作</th>
		                    </tr>
		                </thead>
		
		                <tbody>
		                	<s:iterator value="myTests" status="i">
		                	<s:set var="tId" value="id" />
		                    <tr>
		                        <td class="center">
		                            <label>
		                                <input type="checkbox" level="2"/>
		                            </label>
		                        </td>
		                        <!-- <td><s:property value="id" /></td> -->
		                        <td><s:property value="#i.index+1+(currPage-1)*showNumber" /></td>
		                        <td><s:property value="title" /></td>
		                        <td><s:date name="createTime" format="yy/MM/dd HH:mm" /></td>
		                        <td>
		                            <div class="btn-group">
		                                <a class="btn btn-success btn-xs" href="TestAction_toSubmitTestPage?cid=<s:property value='cid' />&tid=<s:property value='id' />" title="查看已提交作业">
		                                    <span class="glyphicon glyphicon-share-alt"></span>
		                                </a>
		                            </div>
		                        </td>
		                    </tr>
		                    </s:iterator>
		                </tbody>
		            </table>
		          	  当前第<s:property value="currPage" />页，共<s:property value="totalPage" />页&nbsp;&nbsp;
		            <s:if test="currPage > 1">
                    	<a href="TestAction_myTests()?cid=<s:property value='cid' />&currPage=<s:property value="currPage-1" />&totalNumber=<s:property value="totalNumber" />">上一页</a>
                    </s:if>
                    <s:if test="currPage < totalPage">
                    	<a href="TestAction_myTests()?cid=<s:property value='cid' />&currPage=<s:property value="currPage+1" />&totalNumber=<s:property value="totalNumber" />">下一页</a>
					</s:if>
				</s:else>
			</div>
		</div>
	</div>
</body>
</html>