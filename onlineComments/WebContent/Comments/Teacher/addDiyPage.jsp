<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>添加页面</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
</head>
<body>
	<s:include value="header_two.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course_work.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
				<div class="page-header">
					<h3>
						自定义页面
						<small>
							PAGE DESIGN
						</small>
					</h3>
				</div>
			
				<ol class="breadcrumb">
				  <li><a href="CourseAction_myCourses"><s:property value="test.course.name" /></a></li>
				  <li><a href="TestAction_myTests?cid=<s:property value="cid" />"><s:property value="test.title" /></a></li>
				  <li><a href="TestAction_toDesignTest?tid=<s:property value='tid' />&cid=<s:property value='cid' />" id="pageNav">设计实验</a></li>
				  <li class="active">添加自定义页面</li>
				</ol>
					<form class="form-horizontal" role="form" action="PageAction_doAddPage" namespace="/" method="post">
					   <!-- <s:hidden name="id" /> --> 
					   <s:hidden name="tid" />
					   <s:hidden name="cid" />
					   <div class="form-group">
					      <label for="firstname" class="col-sm-2 control-label">页面名称</label>
					      <div class="col-sm-9">
					         <input type="text" class="form-control" id="firstname" name="name"
					            placeholder="请输入页面名称，例如：页面1">
					      </div>
					   </div>
					   <div class="form-group">
					      <div class="col-sm-offset-2 col-sm-10">
					         <button type="submit" class="btn btn-success">确认添加</button>
					      </div>
					   </div>
					</form>
			</div>
		</div>
	</div>
</body>
</html>