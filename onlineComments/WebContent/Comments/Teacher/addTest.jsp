<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>添加实验</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
</head>
<body>
	<s:include value="header_two.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course_work.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
				<div class="page-header">
					<h3>
						添加作业
						<small>
							HOMEWORK ADD
						</small>
					</h3>
				</div>
			
				<ol class="breadcrumb">
				  <li><a href="CourseAction_myCourses"><s:property value="course.name" /></a></li>
				  <li class="active">添加作业</li>
				</ol>
				
				<form class="form-horizontal" role="form" action="TestAction_newTest" namespace="/" method="post">
				   <input type="hidden" name="cid" value='<s:property value="cid" />' />
				   <div class="form-group">
				      <label for="firstname" class="col-sm-2 control-label">作业名称</label>
				      <div class="col-sm-9">
				         <input type="text" class="form-control" id="firstname" name="title"
				            placeholder="请输入作业名称">
				      </div>
				   </div>
				   <div class="form-group">
				      <div class="col-sm-offset-2 col-sm-10">
				         <button type="submit" class="btn btn-success">添加实验</button>
				      </div>
				   </div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>