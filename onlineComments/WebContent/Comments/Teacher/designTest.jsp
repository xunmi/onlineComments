<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>设计实验</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
<script src="<s:url value='/Js/jquery.min.js' />"></script>
<script src="<s:url value='/Js/bootstrap.min.js' />"></script>
<script src="<s:url value='/Js/public.js' />"></script>
<script type="text/javascript">
$(document).ready(function () {
	$('#ajax').click(function(){
		//alert("111");
		var url = "AjaxUpdateNameAction_updateTestName.action";
		var params = {
			"testTitle" : $("#title").val(),
			"tid" : $("#id").val()
		};
		$.getJSON(url, params, function(data){
			var msg = eval("(" + data +")");
			$('#tt').val(msg.result);
			$('#testNav').text(msg.result);
		});
		$('#myModal').modal('hide')
		});
		
	});
</script>
</head>
<body style="overflow-x:hidden">
	<s:include value="header_two.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course_work.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
				<div class="page-header">
					<h3>
						作业设计
						<small>
							HOMEWORK DESIGN
						</small>
					</h3>
				</div>
			
				<ol class="breadcrumb">
				  <li><a href="CourseAction_myCourses"><s:property value="course.name" /></a></li>
				  <li><a href="TestAction_myTests?cid=<s:property value="cid" />" id="testNav"><s:property value="title" /></a></li>
				  <li class="active">设计作业</li>
				</ol>
				
				<div class="row autoside">
					   <s:hidden name="id" id="id"/>
					   <div class="form-group">
						   <lable for="name"><strong>作业名称</strong></lable>&nbsp;&nbsp;<a title="修改名称" class="btn btn-warning btn-xs" data-toggle="modal" 
   data-target="#myModal"><span class="glyphicon glyphicon-pencil"></span></a><br><br>
						   <input type="text" class="form-control" id="tt" name="title"
						      placeholder="请输入作业的名称" value="<s:property value='title' />" readonly="true" onfocus=this.blur() />
   						   <!-- 模态框（Modal） -->
							<div class="modal fade" id="myModal" tabindex="-1" role="dialog" 
							   aria-labelledby="myModalLabel" aria-hidden="true">
							   <div class="modal-dialog">
							      <div class="modal-content">
							         <div class="modal-header">
							            <button type="button" class="close" 
							               data-dismiss="modal" aria-hidden="true">
							                  &times;
							            </button>
							            <h4 class="modal-title" id="myModalLabel">
							             	更改作业名称
							            </h4>
							         </div>
							         <div class="modal-body">
							         	<div class="row">
							         		<div class="col-xs-5 col-sm-2">
						   				<lable for="title"><h4><span class="label label-info">作业名称</span></h4></lable>
						   					</div>
						   					<div class="col-xs-7 col-sm-9">
						   				<input type="text" class="form-control" id="title" name="title"
										    placeholder="请输入作业名称" value="<s:property value='title' />">
											</div>
										</div>
							         </div>
							         <div class="modal-footer">
							            <button type="button" class="btn btn-primary" 
							               data-dismiss="modal">关闭
							            </button>
							            <button type="button" class="btn btn-primary" id="ajax">
							               	提交更改
							            </button>
							         </div>
							      </div><!-- /.modal-content -->
							</div><!-- /.modal -->
							</div>
					   </div>
					   <div class="form-group">
					   	   <lable for="name"><strong>作业页面</strong></lable>&nbsp;&nbsp;<a title="添加模板页面" namespace="/" href="PageAction_toAddPage?tid=<s:property value='tid' />&cid=<s:property value='cid' />" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span>&nbsp;添加模板页面</a>&nbsp;&nbsp;<a title="添加自定义页面" namespace="/" href="PageAction_toAddPage?tid=<s:property value='tid' />&cid=<s:property value='cid' />" class="btn btn-warning"><span class="glyphicon glyphicon-plus"></span>&nbsp;添加自定义页面</a><br><br> 
					   	   <s:if test="pages.isEmpty() == true">目前作业没有任何页面!</s:if >
						   <s:else>
							  <table id="sample-table-1" class="table table-striped table-hover">
				                <thead>
				                    <tr>
				                        <th class="center">
				                            <label>
				                                <input type="checkbox" level="1"/>
				                            </label>
				                        </th>
				                        <th>ID</th>
				                        <th>页面名字</th>
				                        <th>操作</th>
				                    </tr>
				                </thead>
				
				                <tbody>
				                	<!-- 迭代页面集合  -->
				                	<s:iterator var="p" value="pages" status="i">
				                    <tr>
				                        <td class="center">
				                            <label>
				                                <input type="checkbox" level="2"/>
				                            </label>
				                        </td>
				                        <td><s:property value="#i.index+1" /></td>
				                        <td><s:property value="#p.name" /></td>
				                        <td>
			                                <a title="设计页面问题" class="btn btn-info btn-xs" href="PageAction_toDesignPage?cid=<s:property value="cid" />&tid=<s:property value="tid" />&pid=<s:property value="#p.id" />">
			                                    <span class="glyphicon glyphicon-edit"></span>
			                                </a>
			                                <a title="删除当前页面" class="btn btn-danger btn-xs" href="PageAction_doDeletePage?cid=<s:property value="cid" />&tid=<s:property value="tid" />&pid=<s:property value="#p.id" />">
			                                    <span class="glyphicon glyphicon-trash"></span>
			                                </a>
				                        </td>
				                    </tr>
				                    </s:iterator>
				                </tbody>
				            </table>
						</s:else>
				   </div>
					   <!-- <div class="form-group">
					   	   <lable for="name"><h2><span class="label label-info">实验目的</span></h2></lable><br>
					   	   <textarea class="form-control" rows="6"></textarea>
					   </div>
					   <div class="form-group">
					   	   <lable for="name"><h2><span class="label label-info">实验内容</span></h2></lable><br>
					   	   <textarea class="form-control" rows="12"></textarea>
					   </div>
					    -->
				</div>
			</div>
		</div>
	</div>
</body>
</html>