<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>设计页面</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
<script src="<s:url value='/Js/jquery.min.js' />"></script>
<script src="<s:url value='/Js/bootstrap.min.js' />"></script>
<script src="<s:url value='/Js/public.js' />"></script>
<script type="text/javascript">
$(document).ready(function () {
	$('#ajax').click(function(){
		//alert("111");
		var url = "AjaxUpdateNameAction_updatePageName.action";
		var params = {
			"pageName" : $("#name").val(),
			"pid" : $("#id").val()
		};
		$.getJSON(url, params, function(data){
			var msg = eval("(" + data +")");
			$('#pp').val(msg.result);
			$('#pageNav').text(msg.result);
		});
		$('#myModal').modal('hide')
		});
		
	});
</script>

</head>
<body style="overflow-x:hidden">
	<s:include value="header_two.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course_work.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
				<div class="page-header">
					<h3>
						页面设计
						<small>
							PAGE DESIGN
						</small>
					</h3>
				</div>
			
				<ol class="breadcrumb">
				  <li><a href="CourseAction_myCourses"><s:property value="test.course.name" /></a></li>
				  <li><a href="TestAction_myTests?cid=<s:property value="cid" />"><s:property value="test.title" /></a></li>
				  <li><a href="TestAction_toDesignTest?tid=<s:property value='tid' />&cid=<s:property value='cid' />" id="pageNav"><s:property value="name" /></a></li>
				  <li class="active">设计页面</li>
				</ol>
				
				<div class="row autoside">
				   <s:hidden name="id" />
				   <s:hidden name="cid" />
				   <div class="form-group">
					   <lable for="name"><strong>页面名称</strong></lable>&nbsp;&nbsp;<a title="修改页面名称" class="btn btn-warning btn-xs" data-toggle="modal" 
  data-target="#myModal"><span class="glyphicon glyphicon-pencil"></span></a><br><br>
					   <input type="text" class="form-control" id="pp" name="name"
					      placeholder="请输入页面的名称" value="<s:property value='name' />" readonly="true" onfocus=this.blur() />
				   	   <!-- 模态框（Modal） -->
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" 
						   aria-labelledby="myModalLabel" aria-hidden="true">
						   <div class="modal-dialog">
						      <div class="modal-content">
						         <div class="modal-header">
						            <button type="button" class="close" 
						               data-dismiss="modal" aria-hidden="true">
						                  &times;
						            </button>
						            <h4 class="modal-title" id="myModalLabel">
						             	更改页面名称
						            </h4>
						         </div>
						         <div class="modal-body">
						         	<div class="row">
						         		<div class="col-xs-5 col-sm-2">
					   				<lable for="title"><h4><span class="label label-info">页面名称</span></h4></lable>
					   					</div>
					   					<div class="col-xs-7 col-sm-9">
					   				<input type="text" class="form-control" id="name" name="name"
									    placeholder="请输入实验名称" value="<s:property value='name' />">
										</div>
									</div>
						         </div>
						         <div class="modal-footer">
						            <button type="button" class="btn btn-primary" 
						               data-dismiss="modal">关闭
						            </button>
						            <button type="button" class="btn btn-primary" id="ajax">
						               	提交更改
						            </button>
						         </div>
						      </div><!-- /.modal-content -->
						</div><!-- /.modal -->
						</div>
				   </div>
				   <div class="form-group">
				   	   <lable for="name"><strong>页面问题</strong></lable>&nbsp;&nbsp;<a title="添加问题" namespace="/" href="QuestionAction_toSelectQuestionType?pid=<s:property value='pid' />&tid=<s:property value='tid' />&cid=<s:property value='cid' />" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span>&nbsp;添加问题</a><br><br>
				   	   <s:if test="questions.isEmpty() == true">目前该页面没有任何问题!</s:if >
					   <s:else>
						<table id="sample-table-1" class="table table-striped table-hover">
			                <thead>
			                    <tr>
			                        <th class="center">
			                            <label>
			                                <input type="checkbox" level="1"/>
			                            </label>
			                        </th>
			                        <th>ID</th>
			                        <th>题目</th>
			                        <th>题型</th>
			                        <th>操作</th>
			                    </tr>
			                </thead>
			
			                <tbody>
			                	<!-- 迭代问题集合 -->
			                	<s:iterator var="q" value="questions" status="i">
			                    <tr>
			                        <td class="center">
			                            <label>
			                                <input type="checkbox" level="2"/>
			                            </label>
			                        </td>
			                        <!-- <td><s:property value="#q.id" /></td> -->
			                        <td><s:property value="#i.index+1" /></td>
			                        <s:if test="#q.title.length() > 54"><td><s:property value="#q.title.substring(0,50)+'...' " /></td></s:if>
			                        <s:else><td><s:property value="#q.title" /></td></s:else>
			                        <s:if test="#q.questionType == 0">
			                        <td>问答题</td>
			                        </s:if>
			                        <s:else>
			                        <td>未定义题型</td>
			                        </s:else>
			                        <td>
		                                <a title="修改" class="btn btn-info btn-xs" href="QuestionAction_toUpdateQuestion?cid=<s:property value='cid' />&tid=<s:property value='tid' />&pid=<s:property value='pid' />&qid=<s:property value='id' />">
		                                    <span class="glyphicon glyphicon-pencil"></span>
		                                </a>
		                                <a title="删除" class="btn btn-danger btn-xs" href="QuestionAction_doDeleteQuestion?cid=<s:property value='cid' />&tid=<s:property value='tid' />&pid=<s:property value='pid' />&qid=<s:property value='id' />">
		                                    <span class="glyphicon glyphicon-trash"></span>
		                                </a>
			                        </td>
			                    </tr>
			                    </s:iterator>
			                </tbody>
			            </table>
					   </s:else>
			         </div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>