<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>选择问题类型</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
</head>
<body>
	<s:include value="header_two.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course_work.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
				<div class="page-header">
					<h3>
						题型选择
						<small>
							QUESTION TYPE CHOOSE
						</small>
					</h3>
				</div>
			
				<ol class="breadcrumb">
				  <li><a href="CourseAction_myCourses"><s:property value="page.test.course.name" /></a></li>
				  <li><a href="TestAction_myTests?cid=<s:property value="cid" />"><s:property value="page.test.title" /></a></li>
				  <li><a href="TestAction_toDesignTest?tid=<s:property value='tid' />&cid=<s:property value='cid' />"><s:property value="page.name" /></a></li>
				  <li><a href="PageAction_toDesignPage?pid=<s:property value="pid" />&tid=<s:property value="tid" />&cid=<s:property value="cid" />">设计页面</a></li>
				  <li class="active">选择问题类型</li>
				</ol>
				
				<div class="row">
					<form class="form-horizontal" role="form" action="QuestionAction_toDesignQuestion" namespace="/" method="post">
					   <s:hidden name="pid" />
					   <s:hidden name="tid" />
					   <s:hidden name="cid" />
					   <div class="form-group">
					      <label for="firstname" class="col-sm-2 control-label">请选择问题类型</label>
					      <div class="col-sm-3">
					         <select class="form-control" name="questionType">
						         <option>---请选择问题类型---</option>
						         <option value="0">问答题</option>
						      </select>
					      </div>
					   </div>
					   <div class="form-group">
					      <div class="col-sm-offset-2 col-sm-10">
					         <button type="submit" class="btn btn-success">确定</button>
					      </div>
					   </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>