<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
<script src="<s:url value='/Js/jquery.min.js' />"></script>
<script src="<s:url value='/Js/bootstrap.min.js' />"></script>
<script src="<s:url value='/Js/public.js' />"></script>
<script type="text/javascript" charset="utf-8" src="UEditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="UEditor/ueditor.all.min.js"> </script>

	<style type="text/css">
	#addButton{
		position: absolute;
		display: none;
	}
	#comment-panel{
		display: none;
		position: absolute;
		width: 420px;
		z-index: 9999;
	}
	</style>

	<script type="text/javascript">

	$(document).ready(function(){

		//弹出框初始化
		$("[data-toggle='popover']").popover();

		//【选中文字】，显示添加按钮
		$('.answer').mouseup(function(e){
			//获取选中的文字
			var selectedText;

			if(window.getSelection){
				selectedText = window.getSelection().toString();
			}
			else if(document.selection && document.selection.createRange){
				selectedText = document.selection.createRange().text;
			}

			if(selectedText){
				var x = e.originalEvent.x || e.originalEvent.layerX || 0;
				var y = e.originalEvent.y || e.originalEvent.layerY || 0;
				$('#addButton').css({
					'left':x + 250,
					'top':y - 50,
					'display':'block'
				}).fadeIn();
			}
			else{
				$('#addButton').hide();
			}
		});

		//点击【添加】按钮，显示添加面板
		$('#addButton').click(function(e){
			//----初始化面板--------
			//使value等于0的值选中
			$('#comment-select option[value = "0"]').attr('selected', true);
			//清空textarea内容
			$('#comment-content').val('');
			//隐藏【添加按钮】
			$('#addButton').hide();

			//-----实现<button>添加
			var selectedText;
			var range;
			if(window.getSelection){
				range = window.getSelection().getRangeAt(0);
				selectedText = window.getSelection().toString();
			}
			else if(document.selection && document.selection.createRange){
				range = document.selection.createRange();
				selectedText = document.selection.createRange().text;
			}

			//替换选中的文本，变为<button>选中文本格式</button>
			if(range){
				var time = new Date().getTime();
				var mark = document.createElement("button");
				mark.setAttribute("id", time);
				//将id传给【取消】按钮
				$('.cancel').attr("temporary", time);

				mark.setAttribute("style", "border:0");
				mark.setAttribute("class", "btn-info");
				mark.setAttribute("type", "button");
				mark.setAttribute("data-toggle", "popover");
				mark.setAttribute("data-trigger", "focus");
				mark.setAttribute("data-content", "");
				range.surroundContents(mark);
			}

			//-----打开面板--------
			var x = 0;
			var y = e.pageY;
			var width = $(window).width();
			if(width - e.pageX < 420){
				x = width - 430;
			}else{
				x = e.pageX;
			}
			$('#comment-panel').css({
				'left':x + 10,
				'top':y - 20,
				'display':'block'
			}).fadeIn();
		});

		//单击[select]触发事件
		$('.comment-select-item').click(function(){
			$('#comment-content').val($(this).val());
		});

		//点击【批注添加】按钮
		$('#add').click(function(){
			//获得批注的信息
			var commentContent = $('#comment-content').val();
			
			//-------将数据存储到数据库----------
			//获取当前隐藏的答案id
			if(commentContent){
				//获取临时button的id,存放在【取消】按钮中
				var buttonId = $('.cancel').attr("temporary");
				$("#" + buttonId).attr("data-content", commentContent);
			}
			else{
				alert("批注内容不能为空！");
				return;
			}
			var str = "";
			for(var i=0;i<$('.answer').size();i++){
				var answer_id = $('.answer').eq(i).children("input[type='hidden']").val();
				var solutionComments = $('.answer').eq(i).html();
				str = str+answer_id+"<answer_id></answer_id>"+solutionComments+"<solutionComments></solutionComments>";
			}
			var url = "AjaxCommentsAction_addComment.action";
			var data = {"info":str};
			$.post(url, data,function(data){});
			
			
			
			//------处理本地显示--------
			//获取临时button的id,存放在【取消】按钮中
			var buttonId = $('.cancel').attr("temporary");
			$("#" + buttonId).attr("data-content", commentContent);

			//将错误放在错误列表中
			$('.list-group').append("<li class='list-group-item'>" + commentContent + "&nbsp;&nbsp;&nbsp;<button class='btn btn-success btn-xs change' temporary='" + buttonId + "'><span class='glyphicon glyphicon-pencil'></span></button>&nbsp;<button class='btn btn-danger btn-xs delete' temporary='" + buttonId + "'><span class='glyphicon glyphicon-trash'></span></button></li>");

			//------初始化事件---------
			//弹出框初始化
			$("[data-toggle='popover']").popover();
			//点击【修改】按钮
			$('.change').on("click", function(e){
				var buttonId = $(this).attr("temporary");
				var commentContent = $("#" + buttonId).attr("data-content");

				//传值到控制面板
				$('#comment-content').val(commentContent);

				$('#comment-panel').css({
					'left':e.clientX + 30,
					'top':e.clientY - 120,
					'display':'block'
				}).fadeIn();
			});

			//删除temporary属性
			$('.cancel').removeAttr("temporary");

			$('#comment-panel').hide();

		});

		//点击【取消】按钮
		$('.cancel').click(function(){
			
			//删除button
			$("#" + $('.cancel').attr("temporary")).each(function(){
				var text = $(this).html();
				$(this).replaceWith(text);
			});
			//删除temporary属性
			$('.cancel').removeAttr("temporary");

			//隐藏面板
			$('#comment-panel').hide();
		});

		//显示所有的错误
		$('.show-all-errors').click(function(){
			$("[data-toggle='popover']").popover('show');
		});

		//隐藏全部错误
		$('.hide-all-errors').click(function(){
			$("[data-toggle='popover']").popover('hide');
		});



	});

	</script>


</head>
<body>
	<s:include value="header_two.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				
				<div class="page-header">
					<h3>
						批改作业
						<small>
							HOMEWORK CHECKING
						</small>
					</h3>
				</div>
			
				<ol class="breadcrumb">
				  <li><a href="CourseAction_studentCourses"><s:property value="model.test.course.name" /></a></li>
				  <li><a href="TestAction_studentTest?cid=<s:property value="cid" />"><s:property value="model.test.title" /></a></li>
				  <li class="active">批改作业</li>
				</ol>
				
				<h2><span class=""><s:property value="test.title" /></span></h2>
				<br><br>
				<s:if test="model.isEmpty() == true">目前该页面没有任何题目</s:if >
				<s:else>
				<div class="st">
					<s:form action="PageAction_doTeacherPage" method="post">
					<s:hidden name="currPid" value="%{model.id}" />
					<s:hidden name="cid" value="%{cid}" />
					<s:hidden name="tid" value="%{tid}" />
					<s:hidden name="uid" value="%{uid}" />
					<s:iterator value="model.questions" status="i" var="q">
						<div class="row question">
							<s:property value="title" escape="false"/>
						</div>
						<hr>
						<label class="label label-info">答案：</label>
						<s:iterator value="myAnswers" var="a">
						<s:if test="#a.question.id == #q.id">
						<div class="answer">
							<s:set value="id" var="qq"></s:set>
							<s:property value="#session.get(#qq)" escape="false"/> 
							<input type="hidden" value='<s:property value="#qq"/>' />
						</div>
						</s:if>
						</s:iterator>
					</s:iterator>
					<h4 style="text-align:center"><span class="label label-success"><s:property value="name" /></span></h4>
					<div class="row">
					<div class="col-sm-4">	
						<div class="btn-group btn-group-lg">
							<!-- 构造上一页按钮 -->
							<s:if test="model.orderno != #session.current_test.minOrderno">
							<button type="submit" name="submit_pre" value="上一页" class="btn btn-default">上一页</button>
							</s:if>
							<!-- 构造下一页按钮 -->
							<s:if test="model.orderno != #session.current_test.maxOrderno">
							<button type="submit" name="submit_next" value="下一页" class="btn btn-default">下一页</button>
							</s:if>
						</div>
					</div>
					<div class="col-sm-8 right">
						<div class="btn-group btn-group-lg">
							<!-- 构造提交按钮 -->
							<s:if test="model.orderno == #session.current_test.maxOrderno">
							<button type="submit" name="submit_done" value="提交" class="btn btn-default">提交</button>
							</s:if>
							<button type="submit" name="submit_exit" value="退出" class="btn btn-default">退出</button>
						</div>
					</div>
					</div>
					</s:form>
				</div>
				</s:else>
				
				<br>
				<div class="row">
				<!-- 显示全部错误 -->
				<div>
					<button class="btn btn-primary show-all-errors">显示全部错误</button>
					<button class="btn btn-primary hide-all-errors">隐藏全部错误</button>
					</div>
					
					<hr>
					<!-- 下部错误列表 -->
					<div class="col-md-3">
						<ul class="list-group"></ul>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
<!-- 批注添加按钮 -->
<div id="addButton">
	<button class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span></button>
</div>

<!-- 批注添加隐藏界面 -->
<div id="comment-panel">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">添加批注</h3>
		</div>
		<div class="panel-body">
		   <div class="form-group">
		      <label for="name">名称</label>
		      <select id="comment-select" class="form-control">
		      	<option value="0">请选择</option>
		      	<option class="comment-select-item">批注1</option>
		      	<option class="comment-select-item">批注2</option>
		      	<option class="comment-select-item">批注3</option>
		      	<option class="comment-select-item">批注4</option>
		      </select>
		   </div>
		   <div class="form-group">
		      <label for="name">添加新批注：</label>
		      <textarea id="comment-content" class="form-control" rows="5"></textarea>
		   </div>
		   <button class="btn btn-default cancel">取消</button>
		   <button id="add" class="btn btn-warning">添加</button>
		</div>
	</div>
</div>
	
	
</body>
</html>