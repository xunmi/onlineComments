<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
<script src="<s:url value='/Js/jquery-1.7.2.min.js' />"></script>
<script src="<s:url value='/Js/public.js' />"></script>
<script type="text/javascript">
	window.UEDITOR_HOME_URL = 'UEditor/';
</script>
<script type="text/javascript" charset="utf-8" src="UEditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="UEditor/ueditor.all.min.js"> </script>
</head>
<body>
	<s:include value="header_three.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course_work.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<ol class="breadcrumb diyol">
				  <li><a href="#">
				  	<s:if test="#session['user'] != null">
						<s:property value="#session['user'].nickname" />
					</s:if>
				  </a></li>
				  <li><a href="CourseAction_studentCourses"><s:property value="model.test.course.name" /></a></li>
				  <li><a href="TestAction_studentTest?cid=<s:property value="cid" />"><s:property value="model.test.title" /></a></li>
				  <li class="active">实验列表</li>
				</ol>
				<h1><span class="label label-success"><s:property value="test.title" /></span></h1><br>
				<s:if test="model.isEmpty() == true">目前该页面没有任何题目</s:if >
				<s:else>
				<div class="row st">
					<s:form action="PageAction_doStudentPage" method="post">
					<s:hidden name="currPid" value="%{model.id}" />
					<s:hidden name="cid" value="%{cid}" />
					<s:hidden name="tid" value="%{tid}" />
					<s:iterator value="model.questions" status="i">
						<s:property value="title" escape="false"/>
						<textarea id="<s:property value='#i.index' />" name="a<s:property value='id' />"><s:property value='setText("a"+id)' /></textarea>
						<script type="text/javascript">window.UEDITOR_CONFIG.initialFrameHeight = 320;UE.getEditor("<s:property value='#i.index' />");</script>
					</s:iterator>
					<h4 style="text-align:center"><span class="label label-success"><s:property value="name" /></span></h4>
					<div class="col-sm-4">	
						<div class="btn-group btn-group-lg">
							<!-- 构造上一页按钮 -->
							<s:if test="model.orderno != #session.current_test.minOrderno">
							<button type="submit" name="submit_pre" value="上一页" class="btn btn-default">上一页</button>
							</s:if>
							<!-- 构造下一页按钮 -->
							<s:if test="model.orderno != #session.current_test.maxOrderno">
							<button type="submit" name="submit_next" value="下一页" class="btn btn-default">下一页</button>
							</s:if>
						</div>
					</div>
					<div class="col-sm-8 right">
						<div class="btn-group btn-group-lg">
							<!-- 构造提交按钮 -->
							<s:if test="model.orderno == #session.current_test.maxOrderno">
							<button type="submit" name="submit_done" value="提交" class="btn btn-default">提交</button>
							</s:if>
							<button type="submit" name="submit_exit" value="退出" class="btn btn-default">退出</button>
						</div>
					</div>
					</s:form>
				</div>
				</s:else>
			</div>
		</div>
	</div>
</body>
</html>