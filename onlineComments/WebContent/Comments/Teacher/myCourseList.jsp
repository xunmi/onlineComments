<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>我的课程</title>
<link href="<s:url value='/Css/bootstrap.min.css' />" rel="stylesheet">
<link href="<s:url value='/Css/public.css' />" rel="stylesheet">
<script src="<s:url value='/Js/jquery-1.7.2.min.js' />"></script>
<script src="<s:url value='/Js/bootstrap.min.js' />"></script>
<script src="<s:url value='/Js/public.js' />"></script>
</head>
<body>
	<s:include value="header_two.jsp" />
	<div class="container-fluid">
		<div class="row">
			<s:include value="leftside_course_work.jsp" />
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
				<div class="page-header">
					<h3>
						作业发布
						<small>
							HOMEWORK RELEASE
						</small>
					</h3>
				</div>
			
				<ol class="breadcrumb">
				  <li class="active">当前学期课程</li>
				</ol>
				<s:if test="myCourses.isEmpty() == true">目前您没有课程!</s:if >
				<s:else>
					<table id="sample-table-1" class="table table-striped table-hover">
		                <thead>
		                    <tr>
		                        <th class="center">
		                            <label>
		                                <input type="checkbox" level="1"/>
		                            </label>
		                        </th>
		                        <th>ID</th>
		                        <th>课程编号</th>
		                        <th>课程名字</th>
		                        <th>所属学期</th>
		                        <th>创建时间</th>
		                        <th>操作</th>
		                    </tr>
		                </thead>
		
		                <tbody>
		                	<s:iterator value="myCourses" status="i">
		                	<s:set var="cId" value="id" />
		                    <tr>
		                        <td class="center">
		                            <label>
		                                <input type="checkbox" level="2"/>
		                            </label>
		                        </td>
		                        <!-- <td><s:property value="id" /></td> -->
		                        <td><s:property value="#i.index+1" /></td>
		                        <td><s:property value="code" /></td>
		                        <td><s:property value="name" /></td>
		                        <td><s:property value="semester" /></td>
		                        <td><s:date name="createTime" format="yy/MM/dd HH:mm" /></td>
		                        <td>
		                            <div class="btn-group">
									    <a class="btn btn-success btn-xs" href="TestAction_myTests?cid=<s:property value="#cId" />" title="查看详情">
	                                    	<span class="glyphicon glyphicon-zoom-in"></span>
		                                </a>
		                            </div>
		                        </td>
		                    </tr>
		                    </s:iterator>
		                </tbody>
		            </table>
				</s:else>
			</div>
		</div>
	</div>
</body>
</html>