<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<div class="col-sm-3 col-md-2 sidebar">
	<ul class="nav nav-sidebar">
       	<li><a href="CollegeAction_collegeManage.action">学院管理</a></li>
       	<li><a href="MajorAction_majorManage.action">专业管理</a></li>
       	<li><a href="ClassroomAction_classroomManage.action">班级管理</a></li>
       </ul>
</div>