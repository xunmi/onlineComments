<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
	<title>在线批注系统</title>
	<link rel="stylesheet" type="text/css" href='<s:url value="/Css/bootstrap.min.css" />'>
	<script type="text/javascript" src="<s:url value="/Js/jquery-1.8.3.js" />"></script>
	<script type="text/javascript" src="<s:url value="/Js/public.js" />"></script>
	<style type="text/css">
	body{
	font-family: "Microsoft Yahei";
	}
	.navbar-nav{
		margin-left: 20px;
	}
	.sidebar{
		position: fixed;
		top: 51px;
		bottom: 0px;
		left: 0px;
		z-index: 1000;
		display: block;
		padding: 20px;
		overflow-x: hidden;
		overflow-y: auto;
		background-color: #F5F5F5;
		border-right: 1px solid #EEE;
	}
	.nav-sidebar{
		margin-right: -21px;
		margin-bottom: 20px;
		margin-left: -20px;
	}
	.main{
		padding-top: 51px;
		padding-right: 40px;
		padding-left: 40px;
	}
	</style>
</head>
<body>
	<nav class="navbar navbar-fixed-top navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a href="#" class="navbar-brand">在线批注系统</a>
			</div>

			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="CourseParentAction_myCourseParents">课程管理</a></li>
					<li><a href="UserAction_loadUsers.action">用户管理</a></li>
					<li><a href="RightAction_findAllRights.action">权限控制</a></li>
					<li><a href="BasisAction_collegeManage.action">基础配置</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<s:if test="#session['user'] != null">
					<li><a href="javascript:void(0)">欢迎：<s:property value="#session['user'].username" /></a></li>
					</s:if>
				</ul>
			</div>
		</div>
	</nav>